(function($){
    if($('[data-validate]').length > 0)
    {
        $('[data-validate]').validate({
            errorElement : 'div',
            errorClass: 'red-text',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }
})(jQuery)
