<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use SebastianBergmann\ObjectEnumerator\Fixtures\ExceptionThrower;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\Debug\Exception\FatalThrowableError;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if(config('app.env') == 'production')
        {
            if($exception instanceof ExceptionThrower || $exception instanceof \ErrorException)
            {

                session()->flash('message', [
                    'type' => 'warning',
                    'content' => 'Oufff ! Une erreur inconnue s\'est produite ! Nous nous mettrons à travailler dessus bientôt.
                Merci pour votre indulgence et patience.'
                ]);
                return redirect(route('home'));
            }
        }
        return parent::render($request, $exception);
    }
}
