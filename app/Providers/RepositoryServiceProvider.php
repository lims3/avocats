<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Contracts\RegimesRepository::class, \App\Repositories\RegimesRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\GroupsRepository::class, \App\Repositories\GroupsRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\GroupRepository::class, \App\Repositories\GroupRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\SubgroupRepository::class, \App\Repositories\SubgroupRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\RegionRepository::class, \App\Repositories\RegionRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\SchoolRepository::class, \App\Repositories\SchoolRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\OfficeManagerRepository::class, \App\Repositories\OfficeManagerRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\PersonnelRepository::class, \App\Repositories\PersonnelRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\StudentRepository::class, \App\Repositories\StudentRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\InfoRepository::class, \App\Repositories\InfoRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\CountryRepository::class, \App\Repositories\CountryRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\CapabilityRepository::class, \App\Repositories\CapabilityRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\FeatureRepository::class, \App\Repositories\FeatureRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\LevelRepository::class, \App\Repositories\LevelRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\SectionRepository::class, \App\Repositories\SectionRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\OptionRepository::class, \App\Repositories\OptionRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\ParcourRepository::class, \App\Repositories\ParcourRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\OutcomeRepository::class, \App\Repositories\OutcomeRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\BillingRepository::class, \App\Repositories\BillingRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\CostRepository::class, \App\Repositories\CostRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\PresenceRepository::class, \App\Repositories\PresenceRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\CommunicationRepository::class, \App\Repositories\CommunicationRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\BroadcastRepository::class, \App\Repositories\BroadcastRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\ConfigRepository::class, \App\Repositories\ConfigRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\CategoryStudentRepository::class, \App\Repositories\CategoryStudentRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\DefaultInstitutionAdminRepository::class, \App\Repositories\DefaultInstitutionAdminRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\SessionRepository::class, \App\Repositories\SessionRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\EmportRepository::class, \App\Repositories\EmportRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\ActivityRepository::class, \App\Repositories\ActivityRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\NavigationRepository::class, \App\Repositories\NavigationRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\StudentImportRepository::class, \App\Repositories\StudentImportRepositoryEloquent::class);
        //:end-bindings:
    }
}
