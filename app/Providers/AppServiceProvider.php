<?php

namespace App\Providers;

use App\Models\Feature;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Carbon::setLocale(config('app.locale'));
        Schema::defaultStringLength(191);

        // Inject la variable menu dans sidebar
        View::composer('inc.sidebar', function(\Illuminate\Contracts\View\View $view){
            $menus = (auth()->user()->isSAdmin()) ?
                Feature::root()->get() :
                auth()->user()->capability->features(true);
            return $view->with('menus', $menus);
        });

        if(env('APP_ENV') == 'production')
        {
            \URL::forceScheme('https');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RepositoryServiceProvider::class);
        require_once app_path('Helpers/SchoolapHelpers.php');
    }
}
