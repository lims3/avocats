<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class StakeholdersFromUsersCriteria.
 *
 * @package namespace App\Criteria;
 */
class StakeholdersFromUsersCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereHas('adminOf', function($q){
            return $q->whereHas('institution', function($q){
                return $q;
            });
        });
    }
}
