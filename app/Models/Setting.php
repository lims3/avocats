<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //

    const NOTIFICATION_GAT_AWAY= 'NOTIFICATION_GAT_AWAY';

    const NOTIFICATION_POSSIBILITY=[
    	'NOTIFICATION_BY_SMS',
	    'NOTIFICATION_BY_MAIL',
	    'NOTIFICATION_IN_BOX'
    ];


    public $fillable=[
    	'TypeSetting','Value','user_id'
    ];
}
