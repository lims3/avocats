<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    //

    public $fillable=[
    	'title','cover','documents'
    ];
}
