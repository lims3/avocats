<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Picture;
use App\User;
class Profile extends Model
{
    //

    public $fillable=[
    	'name','lastname','firstname','adresse','sexe','province','phone','profile_id','about','numero_ordre','barreau','date_prestation','type_avocat','adresse_cabinet','nom_cabinet'
    ];


    public function picture()
    {
    	return $this->hasOne(Picture::class)->withDefault('');
    }


    public function user()
    {
    	return $this->belongsTo(User::class,'id');
    }

    
}
