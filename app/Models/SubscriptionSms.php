<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionSms extends Model
{
    //

    public $fillable=[
    	'TotalSms','SmsUsed','user_id'
    ];
}
