<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Speciality;

class Autority extends Model
{
    //

    public $fillable=[
    	'name'
    ];


    public function users()
    {
    	return $this->BelongsTo(User::class);
    }

    public function usersFounded()
    {
    	return $this->hasMany(User::class);
    }


    public function speciality()
    {
    	return $this->hasMany(Speciality::class,'autority_id');
    }


    
}
