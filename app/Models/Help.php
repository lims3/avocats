<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Help extends Model
{
    //


    public $fillable=[
    	'user_id','speciality_id'
    ];


    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
