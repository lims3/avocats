<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Consulting;

class Client extends Model
{
    //

    public $fillable=[
    	'Name','Lastname','Firstname','Number','Email'
    ];


    public function consulting()
    {
    	return $this->belongsTo(Consulting::class,'consulting_id');
    }
}
