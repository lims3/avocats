<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Questions;

use App\User;

// use App\Models\

class Answer extends Model
{
    //

    public $fillable=[
    	'lawyer_id','answer','question_id'
    ];


    public function lawyer()
    {
    	return $this->belongsTo(User::class,'lawyer_id');
    }


    public function question()
    {
    	return $this->belongsTo(Questions::class);
    }

    public function answers()
    {
    	// return $this->hasMany(Answer::class,'question_id');
    	return [];
    }
}
