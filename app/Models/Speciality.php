<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Questions;
use App\Models\Autority;

class Speciality extends Model
{
    //


    public function questions()
    {
    	return $this->hasMany(Questions::class,'specialities_id');
    }


    public function autority()
    {
    	return $this->belongsTo(Autority::class);
    }

    public function autorities()
    {
    	return $this->hasMany(Autority::class);
    }
}
