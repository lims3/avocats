<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //

    public $fillable=[
    	'title','Content','Slug','user_id'
    ];

    const ARTICLE_STATE=[
    	'DRAFT',
    	'PUT_ON_LINE'
    ];
}
