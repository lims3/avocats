<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    //


    public $fillable=[
    	'Name','profile_id','thumbnail'
    ];
}
