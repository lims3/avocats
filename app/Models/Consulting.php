<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Client;
use App\Conversation;
use App\user;
class Consulting extends Model
{
    //

    public $fillable=[
    	'Reason','Explanation','user_id','client_id'
    ];

    public function client()
    {
    	return $this->hasOne(Client::class);
    }

    public function messages()
    {
    	return $this->hasMany(Conversation::class);
    }

    public function lawyer()
    {
    	return $this->belongsTo(User::class,'consulting_id');
    }
}
