<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


use App\Models\Speciality;

use App\Models\Answer;

use App\Models\Autority;

class Questions extends Model
{
    //

    public $fillable=[
    	'specialities_id','question','statut'

    ];


    public function speciality()
    {
    	return $this->belongsTo(Speciality::class);
    }

    public function shema()
    {
        // dd(Speciality::find($this->specialities_id)->first()->autority()->first());



        // Speciality::find($this->specialities_id)->autority()->first()->name
        // dd(Speciality::find($this->specialities_id)->autority()->name);

        // dd($this->autority_by_speciality(Speciality::find($this->specialities_id)->autority_id)->name);
        // dd(Autority::find(Speciality::find($this->specialities_id)->autority_id));
        // echo Speciality::find($this->specialities_id)->autority_id;

        // dd();
        return [
            'speciality'=> Speciality::find($this->specialities_id)->name,
            'autority'=> Autority::find(Speciality::find($this->specialities_id)->autority_id)
        ];
    }


    public function answers()
    {
    	return $this->hasMany(Answer::class,'question_id');
    	// return [];
    }

    public function autority_by_speciality($id)
    {
        return Autority::find($id);
    }

    public static function answered()
    {
    	return $this->statut = null ?true:false;
    }

    public function autority($id)
    {
        // dd('call man');
        // dd($this->speciality()->get());
        return Speciality::find((int) $id)->autority()->first()->name;
    }

     public function spc($id)
     {
        return Speciality::find($id);
     }
}


