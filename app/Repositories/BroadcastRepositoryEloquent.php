<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\broadcastRepository;
use App\Models\Broadcast;
use App\Validators\BroadcastValidator;

/**
 * Class BroadcastRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class BroadcastRepositoryEloquent extends BaseRepository implements BroadcastRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Broadcast::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return BroadcastValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
