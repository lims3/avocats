<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\capabilityRepository;
use App\Models\Capability;
use App\Validators\CapabilityValidator;

/**
 * Class CapabilityRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CapabilityRepositoryEloquent extends BaseRepository implements CapabilityRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Capability::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return CapabilityValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
