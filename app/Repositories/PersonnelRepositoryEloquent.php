<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\PersonnelRepository;
use App\Models\Personnel;
use App\Validators\PersonnelValidator;

/**
 * Class PersonnelRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PersonnelRepositoryEloquent extends BaseRepository implements PersonnelRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Personnel::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
