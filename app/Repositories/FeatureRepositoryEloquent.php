<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\featureRepository;
use App\Models\Feature;
use App\Validators\FeatureValidator;

/**
 * Class FeatureRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class FeatureRepositoryEloquent extends BaseRepository implements FeatureRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Feature::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
