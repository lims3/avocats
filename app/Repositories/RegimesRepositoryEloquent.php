<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\RegimesRepository;
use App\Models\Regime;
use App\Validators\RegimesValidator;

/**
 * Class RegimesRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RegimesRepositoryEloquent extends BaseRepository implements RegimesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Regime::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return RegimesValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
