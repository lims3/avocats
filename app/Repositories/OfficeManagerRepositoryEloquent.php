<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\OfficeManagerRepository;
use App\Models\OfficeManager;
use App\Validators\OfficeManagerValidator;

/**
 * Class OfficeManagerRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OfficeManagerRepositoryEloquent extends BaseRepository implements OfficeManagerRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OfficeManager::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return OfficeManagerValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
