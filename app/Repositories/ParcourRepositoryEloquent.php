<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\parcourRepository;
use App\Models\Parcour;
use App\Validators\ParcourValidator;

/**
 * Class ParcourRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ParcourRepositoryEloquent extends BaseRepository implements ParcourRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Parcour::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ParcourValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
