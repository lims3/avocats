<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\StudentImportRepository;
use App\Models\StudentImport;
use App\Validators\StudentImportValidator;

/**
 * Class StudentImportRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class StudentImportRepositoryEloquent extends BaseRepository implements StudentImportRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return StudentImport::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return StudentImportValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
