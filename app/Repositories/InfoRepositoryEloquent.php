<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\InfoRepository;
use App\Models\Info;
use App\Validators\InfoValidator;

/**
 * Class InfoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class InfoRepositoryEloquent extends BaseRepository implements InfoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Info::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
