<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\presenceRepository;
use App\Models\Presence;
use App\Validators\PresenceValidator;

/**
 * Class PresenceRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PresenceRepositoryEloquent extends BaseRepository implements PresenceRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Presence::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return PresenceValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
