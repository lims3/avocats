<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\subgroupRepository;
use App\Models\Subgroup;
use App\Validators\SubgroupValidator;

/**
 * Class SubgroupRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class SubgroupRepositoryEloquent extends BaseRepository implements SubgroupRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Subgroup::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return SubgroupValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
