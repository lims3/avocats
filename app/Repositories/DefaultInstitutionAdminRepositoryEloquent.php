<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\DefaultInstitutionAdminRepository;
use App\Models\DefaultInstitutionAdmin;
use App\Validators\DefaultInstitutionAdminValidator;

/**
 * Class DefaultInstitutionAdminRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class DefaultInstitutionAdminRepositoryEloquent extends BaseRepository implements DefaultInstitutionAdminRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return DefaultInstitutionAdmin::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return DefaultInstitutionAdminValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
