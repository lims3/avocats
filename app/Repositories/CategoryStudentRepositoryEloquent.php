<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\CategoryStudentRepository;
use App\Models\CategoryStudent;
use App\Validators\CategoryStudentValidator;

/**
 * Class CategoryStudentRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CategoryStudentRepositoryEloquent extends BaseRepository implements CategoryStudentRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CategoryStudent::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return CategoryStudentValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
