<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\outcomeRepository;
use App\Models\Outcome;
use App\Validators\OutcomeValidator;

/**
 * Class OutcomeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OutcomeRepositoryEloquent extends BaseRepository implements OutcomeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Outcome::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return OutcomeValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
