<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\GroupsRepository;
use App\Models\Groups;
use App\Validators\GroupsValidator;

/**
 * Class GroupsRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class GroupsRepositoryEloquent extends BaseRepository implements GroupsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Groups::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
