<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\communicationRepository;
use App\Models\Communication;
use App\Validators\CommunicationValidator;

/**
 * Class CommunicationRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CommunicationRepositoryEloquent extends BaseRepository implements CommunicationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Communication::class;
    }


    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return CommunicationValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
