<?php

namespace App\Http\Middleware;

use Closure;

class clientCheckout
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->get('client'))
        {
            return redirect(url('/follow'));
        }
        return $next($request);
    }
}
