<?php

namespace App\Http\Middleware;

use Closure;

use App\User;

class PaymentCheckoutForAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(auth()->check()){
            if(auth()->user()->state == User::PAYMENT_STATUT){
                return redirect(url('/').'/avocat/paymentCheckout');
            }else{
                redirect(url('/home'));
            }
        }
        return $next($request);
    }
}
