<?php

namespace App\Http\Middleware;

use Closure;

class AccountCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(auth()->user()->state);
        if(auth()->user()->state == 'INVALID' )
        {
             abort(403,'Veuillez confirmer votre compte en fournissant les informations réquisent');

        }

        return $next($request);
    }
}
