<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('AccountCheck')->except(['checkout','active']);
        // $this->middleware('checkout');
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('lawyer.statiques.chart');
    }

    public function checkout()
    {
        return view('lawyer.checkout');
    }

    public function active(Request $request)
    {
        $this->validate($request,[
            'ordre'=>'required|max:60',
            'barreau'=>'required|max:60',
            'date'=>'required|max:60',
            'cabinet'=>'required|max:60',
            'dashboard'=>'required|max:300',
            'name'=>'required|max:300'
        ]);

        $profile=auth()->user()->profile()->first()->update([
            "numero_ordre" => $request->ordre,
            "barreau" => $request->barreau,
            "date_prestation" => $request->date,
            "type_avocat" => $request->dashboard,
            "adresse_cabinet" => $request->cabinet,
            "nom_cabinet" => $request->name
        ]);
        
        session(['message'=>'Information ajouté avec succès']);


        return back();
    }


    

}
