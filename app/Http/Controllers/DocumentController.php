<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Document;
use Intervention\Image\Facades\Image;


class DocumentController extends Controller
{

    public function __construct()
    {
          $this->middleware('Admin')->except('index');
          $this->middleware('auth')->except('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $documents=Document::all();
        

        return view('documents',compact('documents'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //


        return view('admin.documents');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request,[
            'title'=>'required|max:255',
            'document'=>'required'
        ]);

        if($request->file('cover'))
        {
            $cover = $request->file('cover');

            $coverOriginalName = pathinfo($cover->getClientOriginalName());

           
            $p=public_path('documents/cover/').$coverOriginalName['basename'];

            if($request->file('cover')
                       ->move(public_path('documents/cover'),$p
                )){ 

                $document=$request->file('document');
                $documentOriginalName = pathinfo($document->getClientOriginalName());
                $pd=public_path('documents/cover/').$coverOriginalName['basename'];
                $request->file('document')
                       ->move(public_path('documents/'),$pd);

                $image=  Image::make($p);
                $image->resize(512,380);
                unlink($p);
                $image->save();

                Document::create([
                    'title'     =>$request->title,
                    'cover'     =>$request->file('cover')->getClientOriginalName(),
                    'documents'  =>$request->file('document')->getClientOriginalName()
                ]);
                session(['message'=>'Document Ajouté avec succès']);
                return back();

            }       
                        
        }
        // dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
