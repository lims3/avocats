<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Consulting;

use App\Models\Autority;

// use App\Models\Client;

class NotificationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('autority');
         $this->middleware('AccountCheck')->except('autority');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $onStandby=auth()->user()->fileOnStandbyOfTreatment()->get();
        // dd($onStandby);
        // dd($onStandby);

        // dd($onStandby);
        // foreach ($onStandby as  $value) {
            # code...

        //     dd($value->Reason);
        // }

        return view('lawyer.consulting.dossier',compact('onStandby'));

        // return view('lawyer.notification.consulting',compact('onStandby'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    
    public function autority()
    {
        return Autority::all();
    }
}
