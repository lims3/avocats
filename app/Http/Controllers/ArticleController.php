<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Article;

use Intervention\Image\Facades\Image;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Carbon;

class ArticleController extends Controller
{

    public function __construct()
    {
        // $this->middleware('checkout');
        $this->middleware('auth')->except('show');
        $this->middleware('AccountCheck')->except('show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles=auth()->user()->articles()->get();

        $draft=0;
        $online=0;

        foreach ($articles as  $article) {
            if($article->state == 'DRAFT')
            {
                $draft++;
            }else
            {
                $online++;
            }
        }

        // $date=Carbon::parse('2018-07-01 09:15:47');
        // dd($date);
        return view('lawyer.article.liste_articles',compact('articles','draft','online'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lawyer.article.news_articles');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        // dd($request->all()); 

        $this->validate($request,[
            'title'=>'required|max:500',
            'descr'=>'required',
            'tosendlike'=>'required|integer|min:1|max:2'
        ]);

        // try{

                if($request->file('file'))
                {
                    
                    $file = $request->file('file');

                    $fileOriginalName = pathinfo($file->getClientOriginalName());
                    $fileSize = $file->getSize();
                   
                    $p=public_path('img/upload/').$fileOriginalName['basename'];

                    if($request->file('file')
                               ->move(public_path('img/upload'),$p
                        )){
                        
                       

                        $request->file('file')->getClientOriginalName();
                        $article            = new Article();
                        $article->title     = $request->title;
                        $article->content   = $request->descr;
                        $article->slug      = str_slug($request->title,'-');
                        $article->created_at=Carbon::now();

                        switch ($request->tosendlike) {
                            case 1:
                                  $article->state= Article::ARTICLE_STATE[0];
                            break;
                            
                            case 2:
                                  $article->state= Article::ARTICLE_STATE[1];
                            break;
                        }

                        $article->cover          = $request->file('file')->getClientOriginalName();
              
                        $article->user_id        = auth()->user()->id;
                        $article->save();

                        $image=  Image::make($p);
                        $image->resize(512,380);
                        unlink($p);
                        $image->save();
                        // $picture = new Picture();
                        // $picture->Name = Hash::make($fileOriginalName['filename']).'.'.$fileOriginalName['extension'];
                        // $picture->profile_id = auth()->user()->profile->id;
                        // $picture->thumbnail = $newFileName;
                        // $picture->save();



                         session()->put('message','L\'article a été ajouté avec succès');
                         return redirect()->back();

                      }else{

                         session()->put('message','L\'article n\'a pas pu être ajouté pour de raison inentendue et nous vous garantissons que nous férons de notre mieux pour régler le problème, franche collaboration!');
                         return back();

                      }
                
                }else{

                    $article            = new Article();
                    $article->title     = $request->title;
                    $article->content   = $request->descr;
                    $article->slug      = str_slug($request->title,'-');
                    $article->created_at=Carbon::now();

                     switch ($request->tosendlike) {
                            case 1:
                                  $article->state=Article::ARTICLE_STATE[0];
                            break;
                            
                            case 2:
                                  $article->state=Article::ARTICLE_STATE[1];
                            break;
                        }

                    $article->user_id   =auth()->user()->id;
                    $article->save();

                    session()->put('message','L\'article a été ajouté avec succès');
                    return back();
                

               }

        // }catch(\Exception $e){

        // }
    
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        //

        // dd($id);
        $article=Article::where('id',$id)->get()[0];

        $recents=Article::all();

        return view('blog',compact('article','recents'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        if(!Article::find($id))
            return redirect(route('article.index'));

        $article=Article::find($id);
        // dd($article->id);
        return view('lawyer.article.edit_articles',compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

       
    }

    public function redirect_by_action(Request $request)
    {
        $this->validate($request,[
            'item'=>'required|integer',
            'action'=>'required|integer|max:2|min:1'
        ]);

        if(!Article::find($request->item))
                return back();
        switch ($request->action) {
            case 1:
                    Article::find($request->item)->delete();
                    session()->put('message','Suppression réusit avec succès');
                    return back();
                break;
                
            case 2:
                    return redirect(route('article.edit',['id'=>$request->item]));
                break;
        }

        // dd($request->all());
    }


    public function update_article(Request $request)
    {
        // dd($request->all()); 

        $this->validate($request,[
            'item'=>'required|integer',
            'title'=>'required|max:255',
            'tosendlike'=>'required|integer|min:1|max:2',
            'descr'=>'required'
        ]);

        if(!Article::find($request->item))
            return back();

        if($request->file('file'))
        {   
          

             if($request->file('file')
                               ->move(public_path('img/upload'),$request->file('file')
                               ->getClientOriginalName()
                        )){
                  // dd('modification');
                Article::where('id',(int)$request->item)
                    ->update([
                        'title'     =>$request->title,
                        'content'   =>$request->descr,
                        'state'     => $request->tosendlike == 1?Article::ARTICLE_STATE[0]:
                                       Article::ARTICLE_STATE[1],
                        'cover'     => $request->file('file')->getClientOriginalName()


                    ]);
                session()->put('message','L\'article a été modifié avec succès');
                dd('modification');
                return back();

            }else{
                dd('une erreur a été rencontrée');
                session()->put('message','L\'article n\'a pas pu être modifié pour des raisons inentendues et nous vous garantissons que nous férons de notre mieux pour régler le problème. Franche collaboration!');
                return back();
            }
        }

           
            $article= Article::where('id',(int)$request->item)->first();
            $article->title=$request->title;
            $article->content=$request->descr;
            $article->state=(int)$request->tosendlike == 1 ?Article::ARTICLE_STATE[0]:
                                                            Article::ARTICLE_STATE[1];
            $article->save();
   
            
            session()->put('message','L\'article a été modifié avec succès');

            return back();
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        return 'suppression';
        // dd($request->id);
    }

    public function delete($id)
    {
        if(!isset($id)){
            return back();
        }

        Article::find($id)->first()->delete();

        return back();

    }
}

