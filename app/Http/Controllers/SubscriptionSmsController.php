<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\SubscriptionSms;

class SubscriptionSmsController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
         $this->middleware('AccountCheck');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        // return view('lawyer.sms.subscribe_sms');
        return view('lawyer.sms.souscription');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        


        // dd($request->all());

        try{

                $this->validate($request,[
                    'offre'=>'required|numeric'
                ]);

            $message='';

            if(empty(auth()->user()->sms()->get()->all()))
            {
                     SubscriptionSms::create([
                        'TotalSms'=>$request['offre'],
                        'SmsUsed'=>0,
                        'user_id'=>auth()->user()->id
                     ]);
                $message="Merci pour votre souscription à ce service";

            }else
            {
                 if(auth()->user()->sms()->get()->last()->TotalSms > 0)
                {
              
                     $message='Vous avez une souscription en cours de validité...';
                }else
                {
                     SubscriptionSms::create([
                        'TotalSms'=>$request['offre'],
                        'SmsUsed'=>0,
                        'user_id'=>auth()->user()->id
                     ]);

                    $message= 'Réabonnement éffectué avec sucès';
                }
            }
            
            session()->put('message',$message);

        }catch(\Exception $e)
        {

        }

        return redirect()->back();
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
