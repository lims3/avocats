<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Speciality;

use App\Models\Questions;

use App\Models\Answer;

use Carbon\Carbon;

// use App\Models\Speciality;   

class QuestionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->only('index')->except(['create','store']);
         $this->middleware('AccountCheck')->only('index')->except(['create','store']);;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // dd(Carbon::now());
        $retrevied_speciality=[];
        $speciality=auth()->user()->autority()->first()->speciality()->get();
        foreach ($speciality as $key => $value) {
           $retrevied_speciality[]=$value->id;
        }

        $valid_speciality=implode(',',$retrevied_speciality);
        // $questions=DB::table('Questions')
        //                 ->whereRaw(" specialities_id IN($valid_speciality) ")->get();
        $questions=Questions::whereIn('specialities_id',$retrevied_speciality)->get();
        $answered=count(DB::table('questions')
                        ->whereRaw(" specialities_id IN($valid_speciality) and statut is not null ")->get());
        $pedding=$statut=count(DB::table('questions')
                        ->whereRaw(" specialities_id IN($valid_speciality) and statut is  null ")->get());
        // dd($retrevied_speciality);

        $index=1;
        $tota=0;
        // dd($pedding);
        // $question=Questions::where('id',2)->first()->get();
        // dd(count())
        // $total=Questions::where('id',2)->first();
        $total=count($questions);
        
         // dd($questions);
        return view('lawyer.questions.questions_monitoring',compact('questions','index','total','pedding','answered'));
       
    }

    /**
     * Récupere le nombre de toutes les questions en attente des réponses.
     *
     * @return Int
     */

    public static function getCountQuestion()
    {
        $retrevied_speciality=[];
        $speciality=auth()->user()->autority()->first()->speciality()->get();
        foreach ($speciality as $key => $value) {
           $retrevied_speciality[]=$value->id;
        }

        $valid_speciality=implode(',',$retrevied_speciality);

        $pedding=$statut=count(DB::table('questions')
                        ->whereRaw(" specialities_id IN($valid_speciality) and statut is  null ")->get());

        return $pedding;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

         $specialities=Speciality::all();




        return view('questions',compact('specialities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        // dd($request->all());


        $this->validate($request,[
            'speciality'=>'required|integer',
            'question' => 'required|string|max:500'
        ]);

        if (!Speciality::find($request->speciality)) {
            return back();
        }

        Questions::create([
            'question'=>$request->question,
            'specialities_id'=>$request->speciality
        ]);

        session()->put('message','Questions envoyées avec succès');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        if(!Questions::find($id)){
            return back();
        }

        $answers=Questions::find($id)->answers()->simplePaginate(10);
     

        return view('answers',compact('answers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function answers($id)
    {
        if (!Questions::find($id)) 
                return back();

        $id=$id;

        return view('lawyer.questions.answers',compact('id'));
     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function response(Request $request)
    {

    

        $this->validate($request,[
            'id'=>'required|integer',
            'message' => 'required|string|max:500'
        ]);

        if (!Questions::find($request->id)) 
                return back();


        Answer::create([
            'lawyer_id'=>auth()->user()->id,
            'question_id'=>$request->id,
            'answer'=>$request->message
           

        ]);

        Questions::find($request->id)->update([ 'statut'=>'ACTIVE']);

        session()->put('message','reponse ajoutée avec succès');


        return back();


    }   


    public function faq()
    {

        $questions=Questions::orderBy('created_at','asc')->paginate(6);

      
        return view('faq',compact('questions'));
    }
}
