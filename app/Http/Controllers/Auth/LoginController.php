<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'email';
    }

    public function showLoginForm()
    {
        return view('login');
    }

    public function authenticated($request, $user)
    {
        // if($user->isAdminOfSchoolOffice() &&
        //     empty($user->institution()->getOptions()))
        // {
        //     session()->flash('message', [
        //         'content' => 'Bienvenu dans Schoolap ! Pour que votre établissement soit utilisable, Veuillez définir les différentes options qu\'il organise',
        //         'type' => 'info'
        //     ]);
        //     return redirect(route('config', ['type'=>'option']));
        // }

        // session()->flash('message', [
        //     'content' => 'Bienvenu dans Schoolap !',
        //     'type' => 'info'
        // ]); 

        // return redirect()->intended($this->redirectPath());
    }
}
