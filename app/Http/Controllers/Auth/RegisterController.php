<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Models\Profile;
use App\Models\Speciality;

use App\Models\Picture;

use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // dd($data,'enregistrement');
        return Validator::make($data, [
            'name' => 'required|string|max:255|min:4',
            'lastname'=>'required|string|max:255|min:4',
            'firstname'=>'required|string|max:255|min:4',
            'adresse'=>'required|max:255',
            'sexe' =>'required|integer|max:2|min:1',    
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'province'=>'required|integer|max:4|min:1',
            'phone'=>'required|max:13|regex:/^[+243]{3}[0-9]{9}$/',
            'speciality'=>'required|integer|exists:specialities,id'
            
        ],[
            'phone.regex'=>'Le numero de telephone que vous avez renseigné n\'est pas valide',
            'phone.max'=>'Le numero de telephone que vous avez renseigné n\'est pas valide'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // dd($data['speciality']);      
        $registerd_user=User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'state'=>'INVALID',
            'autority_id'=>$data['speciality']
        ]);

         $profile=Profile::create([
            'name'=>$data['name'],
            'lastname'=>$data['lastname'],
            'firstname'=>$data['firstname'],
            'adresse'=>$data['adresse'],
            'sexe'=>$data['sexe'],
            'province'=>$data['province'],
            'phone'=>$data['phone'],
            'profile_id'=>$registerd_user->id
        
        ]);

        switch ($data['sexe']) {
            case 1:
                Picture::create([
                    'Name'=>'m-default-profile.png',
                    'profile_id'=> $profile->id
                ]);
                break;
            
            case 2:
                Picture::create([
                    'Name'=>'f-default-profile.png',
                    'profile_id'=> $profile->id
                ]);
            break;
        }

         return $registerd_user;

    }


}
