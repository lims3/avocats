<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Article;
use App\Models\Documents;

class RootController extends Controller
{
    //

    public function __contruct()
    {
    	$this->middleware('Admin');
    	$this->middleware('auth');
    }


    // public function index()
    // {
    // 	dd('avocat');
    // }


    public function checklist()
    {
    	$inactived=User::where('state','INVALID')->simplePaginate(10);
    	
    	return view('admin.checklist',compact('inactived'));
    }


    public function allowed($id)
    {
    	if(!User::find($id)){
    		return back();
    	}

    	User::where('id',$id)->first()->update([
    		'state'=>'VALID'
    	]);

    	session(['message'=>'Compte activé avec succès']);

    	return back();

    }


    public function articles()
    {
    	$articles=Article::paginate(10);
    	// dd($articles);

    	return view('admin.articles',compact('articles'));
    }
}
