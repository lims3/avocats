<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Autority;

class AutorityController extends Controller
{

    public function __construct()
    {
          $this->middleware('auth');
          $this->middleware('AccountCheck');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $autorities=auth()->user()->autorities()->get();

        // dd($autorities);
        return view('lawyer.autority.list_of_autority',compact('autorities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lawyer.autority.add_autority');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        $this->validate($request,[
            'name'=>'required|string|max:255|',
            'level'=>'required|integer|min:10|max:100'
        ]);

        try{

            $autority=new Autority();
            $autority->name=$request->name;
            $autority->level=$request->level;
            $autority->user_id=auth()->user()->id;
            $autority->save();
            session()->put('message','Compétence ajoutée avec succès');
            return redirect()->back();
        }catch(\Exception $e){
            session()->put('message','Problème survenu lors de l\'ajout de la compétence');
            return redirect()->back();

       }
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
