<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Profile;

use App\Models\Picture;

use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{
    public function __construct()
    {
          $this->middleware('auth');
          $this->middleware('AccountCheck');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // dd();
        $lawyerProfile=auth()->user()->profile;
        // dd($lawyerProfile->picture()->orderBy('id','desc')->first()->Name);
        $picture=Picture::where('profile_id',$lawyerProfile->id)->first()?'/'.Picture::where('profile_id',$lawyerProfile->id)->orderBy('id','desc')->first()->Name:'/avatar.jpg';
     
        return view('lawyer.profile.lawyer_profile',compact('lawyerProfile','picture'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request,[
            // 'file'=>'required',
             'name' => 'required|string|max:255|min:4',
             'lastname'=>'required|string|max:255|min:4',
             'firstname'=>'required|string|max:255|min:4',
             'adresse'=>'required|max:255',
             'sexe' =>'required|integer|max:2|min:1',
             'region'=>'required|integer|max:4|min:1',
             'phone'=>'required|max:13|regex:/^[243]{3}[0-9]{9}$/',
             'description'=>'max:250'
        ],[
            // 'file.required'=>'Veuillez joindre un fichier à fin d\'ajouter votre photo'
        ]);

            // dd($request->all());
            if($request->file('file'))
            {
                $file = $request->file('file');

                $fileOriginalName = pathinfo($file->getClientOriginalName());
                $fileSize = $file->getSize();
                // dd(substr(PHP_OS,0,3));
                // dd($request->all());    
                 $p=public_path('img/upload/').$fileOriginalName['basename'];
                // dd($p);
                if($file->move(public_path('img/upload/'),$fileOriginalName['basename'])){
                   
                    // dd($p); 
                    $image=  Image::make($p);
                    $image->resize(160,160);
                    unlink($p);
                    $image->save();
                    $picture = new Picture();
                    $picture->Name = $fileOriginalName['filename'].'.'.$fileOriginalName['extension'];
                    $picture->profile_id = auth()->user()->profile->id;
                    $picture->save();

                    Profile::find(auth()->user()->id)->update([
                         "about"=>$request->description,
                         "firstname" => $request->firstname,
                         "name" => $request->name,
                         "lastname" => $request->lastname,
                         "adresse" => $request->adresse,
                         "sexe" => $request->sexe,
                         "province" => $request->region,
                         "phone" => $request->phone

                    ]);
                    
                    session(['message'=>'fichier ajouté avec succès']);
                    return back()->withMessage('Modification appliquée avec succès');
      
                }
             Profile::find(auth()->user()->id)->update(['about'=>$request->description,
                     "firstname" => $request->firstname,
                    "name" => $request->name,
                    "lastname" => $request->lastname,
                    "adresse" => $request->adresse,
                    "sexe" => $request->sexe,
                    "province" => $request->region,
                    "phone" => $request->phone

                ]);
         }else{

                    Profile::find(auth()->user()->id)->update([
                        "about"=>$request->description,
                        "firstname" => $request->firstname,
                        "name" => $request->name,
                        "lastname" => $request->lastname,
                        "adresse" => $request->adresse,
                        "sexe" => $request->sexe,
                        "province" => $request->region,
                        "phone" => $request->phone

                    ]);
                    
                    session(['message'=>'fichier ajouté avec succès']);
                    return back()->withMessage('Modification appliquée avec succès');
         }
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
