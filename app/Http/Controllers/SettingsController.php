<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Setting;

use App\User;

class SettingsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
         $this->middleware('AccountCheck');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }


    public function profession(Request $request)
    {
        return view('config.profession');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        // return view('lawyer.settings.notification');
        return view('lawyer.settings.config_notification');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'notification'=>'required'
        ]);

        $message='';
     
        // try
        // {
            switch ($request->notification) {
            case 0:
                

                if(!(auth()->user()->sms->last()->TotalSms > 0))
                {
                    session()->put(['message'=>'Veuillez recharger les sms à fin de béneficier de ce service']);
                    return back();
                }

                if(!(count(auth()->user()->setting()->where('TypeSetting',
                            Setting::NOTIFICATION_GAT_AWAY
                        )->first())))
                {
                               
                        Setting::create([
                                'TypeSetting'=>Setting::NOTIFICATION_GAT_AWAY,
                                'Value'     =>Setting::NOTIFICATION_POSSIBILITY[0],
                                'user_id'   =>auth()->user()->id
                        ]);
                        dd('message');

                        session()->put(['message'=>'Paramettre configuré avec succès']);

                        return back();
                }
                        
                        if(count(auth()->user()->setting()->where('TypeSetting',
                            Setting::NOTIFICATION_GAT_AWAY)) &&
                                !(auth()->user()->setting()->where('TypeSetting',
                                Setting::NOTIFICATION_GAT_AWAY
                            )->first()->Value == Setting::NOTIFICATION_POSSIBILITY[0])
                            )
                        {
                            auth()->user()->setting()->where('TypeSetting',
                                    Setting::NOTIFICATION_GAT_AWAY
                            )->first()->update(['Value'=>Setting::NOTIFICATION_POSSIBILITY[0]]);

                            session()->put(['message'=>'Configuration modifiée avec succès']);

                            return back();
                        }

                        session()->put(['message'=>'Ce paramettre a été déjà définit']);

                        return back();
                

                break;
            case 1:
                Setting::create([
                        'TypeSetting'=>Setting::NOTIFICATION_GAT_AWAY,
                        'Value'     =>Setting::NOTIFICATION_POSSIBILITY[1],
                        'user_id'   =>auth()->user()->id
                ]);

                session()->put(['message'=>'Paramettre configuré avec succès']);

                return back();
                break;
            case 2:
                Setting::create([
                        'TypeSetting'=>Setting::NOTIFICATION_GAT_AWAY,
                        'Value'     =>Setting::NOTIFICATION_POSSIBILITY[2],
                        'user_id'   =>auth()->user()->id
                ]);

                session()->put(['message'=>'Paramettre configuré avec succès']);

                return back();

                break;
            default:
               session()->put(['message'=>'Aucun paramettre trouvé']);

                return back();
                break;
        }
            

        // }catch(\Exception $e)
        // {

        // }
        // dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
