<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Models\Profile;
use App\Models\Consulting;
use App\Models\Client;
use App\Models\Setting;
use App\Conversation;
use App\Models\Speciality;
use App\Models\Help;
// use App\User;

class ConsultingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->only(['show','apply','index']);
        $this->middleware('clientCheckout')->only('infollow');
        $this->middleware('AccountCheck')->only(['show','apply','index']);;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $speciality=speciality::whereIn('id',[13,17,4])->get();

       

        return view('lawyer.consulting.help',compact('speciality'));
    }

    public function apply(Request $request)
    {
        $this->validate($request,[
            'message'=>'max:255',
            'id'=>'required|numeric'
        ]);

        if($request->wantJson())
        {
            dd('requête utilisant ajax');
        }

        if(!count(Consulting::find($request->id)))
            return redirect()->route('notification.index');
        
        // dd(trim($request->file('file')->getClientOriginalName(),' '));

        if($request->hasFile('file') && !$request->message)
         {
            Conversation::create([
                'message'       => trim($request->file('file')->getClientOriginalName(),' '),
                'message_type'  => Conversation::MESSAGE_TYPE[1],
                'consulting_id' => $request->id,
                'user_id'       => auth()->user()->id
            ]);

            $request->file->move(public_path('img/upload'),$request->file('file')-> 
                                 getClientOriginalName());
            return back();
         }

         if($request->hasFile('file') && $request->message)
         {
            Conversation::create([
                'message'       => trim($request->file('file')->getClientOriginalName(),' '),
                'message_type'  => Conversation::MESSAGE_TYPE[1],
                'consulting_id' => $request->id,
                'user_id'       => auth()->user()->id
            ]);

            Conversation::create([
                'message'       => $request->message,
                'message_type'  => Conversation::MESSAGE_TYPE[0],
                'consulting_id' => $request->id,
                'user_id'       => auth()->user()->id
            ]);

            $request->file->move(public_path('img/upload'),$request->file('file')-> 
                                 getClientOriginalName());
            return back();

         }

         if(!$request->hasFile('file') && $request->message)
         {
            Conversation::create([
                'message'       => $request->message,
                'message_type'  => Conversation::MESSAGE_TYPE[0],
                'consulting_id' => $request->id,
                'user_id'       => auth()->user()->id
            ]);

            return back();
         }

         if(!$request->hasFile('file') && !$request->message)
         {
            return $this->validate($request,[
                'message'=>'required|max:255'
            ]);
         }  

    }


    public function treating($id)
    {


        session()->put('message','Désolé car cette fonctionnalité n\'est pas encore disponible');

        return back();

        if($id)
        {
            if(!count(Consulting::findorFail($id)))
            return redirect()->route('notification.index');

            $messages=Consulting::find($id)->messages()->where('user_id',auth()->user()->id)
                                                        ->SimplePaginate(4);
            $id=$id;
         
            return view('lawyer.conversation.lawyer_conversation',compact('messages','id'));  
        }else{
            return view('lawyer.conversation.lawyer_conversation',compact('messages','id'));  
        }     
    }

    public function search(Request $request)
    {
            
            // dd($request->all());
            if($request->help)
            {   
               
                // $users=User::where('specialities_id',$request->speciality);
                 $specialities=Speciality::where('id',$request->speciality)->first();
                        // dd($specialities);
                        
                    if(!$specialities)
                    {
                        session()->put('search','Aucun avocat trouvé dans ce domaine');
                        return back();
                    }
                    $specialities=$specialities->autority()->first();

                    

                $retrivied_data=[];

                $req=(int) $request->speciality;

                 foreach ($specialities->usersFounded()->get() as $key => $user) {

                    foreach ($user->profile()->get() as $key => $profile) {
            
                            if($profile->province == (int) $request->region)
                            {
                                $retrivied_data[] = $profile; 
                            }   
                    }
                }
                // dd($retrivied_data);
     

               $retrivied_helps=[];
               $user=[];


               foreach ($retrivied_data as $key => $value) {
                    
                    if($value->user()->first()->help())
                    {
                        // dd($value);
                    
                     
                       foreach ($value->user()->get() as $key => $values) 
                       {
                                
                            // dd($values);
                            foreach ($values->help()->get() as $key => $help) {
                        
                                
                                if($help->speciality_id == (int) $req )
                                {
                                    // dd($help);
                                    $user[]=$help;
                                }
                            }
                              

                        }     
                    
                    }
               }

               // dd($user);
               $usr=[];

               foreach ($user as $key => $userss) {
                    
                    $usr[]=$userss->user()->get();
               }
                
                // dd($usr);
                if(count($usr) > 0)
                {
                    // dd($usr);
                    // dd('ici');
                    // $unselected=$this->unselected($retrieved);
                    return view('lawyer.consulting.search_legal',compact('usr','unselected'));
                   
                }else{
                    // dd('ici...');
                    session()->put('search','Aucun avocat trouvé dans ce domaine');
                    return back();
                }
            }else{

                // Recherche sans l'aide légale
                // dd($request->speciality,$request->region);

                // $users=User::where('specialities_id',$request->speciality);
                $specialities=Speciality::where('id',(int) $request->speciality)->first();
                // dd($specialities,$request->speciality); 
                
            if(!$specialities)
            {
                  // dd('ici');
                session()->put('search','Aucun avocat trouvé dans ce domaine');
                return back();
            }
            $specialities=$specialities->autority()->first();

            
            $retrivied_data=[];

            // $users=[];

            // foreach ($specialities as $key => $speciality) {
            //     $users[]=$speciality->autority()->first()->users()->first();
            // }

            // dd($specialities->usersFounded()->get());

            foreach ($specialities->usersFounded()->get() as $key => $user) {

                    foreach ($user->profile()->get() as $key => $profile) {
            
                            if($profile->province == (int) $request->region)
                            {
                                $retrivied_data[] = $profile; 
                            }   
                    }
            }
           
           // dd($retrivied_data);

           if(!$retrivied_data){
                // dd('ici');
                session()->put('search','Aucun avocat trouvé dans ce domaine');   
                return back();

           }

          

            $matching_for_any=[];
        
            foreach ($retrivied_data as $key => $data_to) {
             
                foreach ($data_to->user()->get() as $key => $user) {
            
                         // foreach ($user->articles()->orderBy('user_id')->get() 
                         //                as $key => $article) {
                   
            //                     if($user->id == $article->user_id )
            //                     {
                            
            //                        if(array_key_exists($user->id, $matching_for_any))
            //                        {
                     
            //                             $matching_for_any[$user->id]['articles'][]=$article;
                                        $matching_for_any[$user->id]['user']=$user;
            //                             $matching_for_any[$user->id]['compteur']= 
            //                                     $increment= count($matching_for_any[$user->id]['articles']) ;
            //                        }else{
            //                             $matching_for_any[$user->id]['articles'][]=$article;
            //                             $matching_for_any[$user->id]['user']=$user;
            //                             $matching_for_any[$user->id]['compteur']=
            //                             count($matching_for_any[$user->id]['articles']);
            //                        }
            //                     }
            //              }
                        
                        }
                    }
            
           
                
                $sortedByDesc     = collect($matching_for_any);
                // $sortedByDesc     = collect($matching_for_any)->sortByDesc('compteur');
                $users_matcheds   = $sortedByDesc->values()->all();
                $retrieved_users  = [];
            
                foreach ($users_matcheds as  $values)
                {
               
                        $retrieved_users[]=$values['user'];
                        
                }

                           
                   
                      
                $retrieved=$retrieved_users;
                // dd($retrieved);
       
                // if(!count($retrieved) > 0 && count($this->unselected($retrieved,$request->speciality,$request->region)) > 0 )
                // {
                //     dd('ici');
                // }else
                // {
                //     if(count($retrieved) > 0 && count($this->unselected($retrieved,$request->speciality,$request->region)) > 0  )
                //     {

                //     }
                // }
                // dd($retrieved);

                 if(!count($retrieved) > 0 )
                {
                    // $unselected=$this->unselected($retrieved,$request->speciality,$request->region);
                    // dd($unselected);
                     return view('lawyer.consulting.search',compact('retrieved'));
                   
                }else{
                    //   dd($retrieved);
                    // session()->put('search','Aucun avocat trouvé dans ce domaine');
                    // dd(session()->get('notfound'));
                    // return back();
                    $unselected=$this->unselected($retrieved,$request->speciality,$request->region);
                    // dd($unselected);
                     return view('lawyer.consulting.search',compact('retrieved','unselected'));
                }
                     
                    // return view('lawyer.consulting.search',compact('retrieved_users'));
               
            }

            
                
    }

    public function unselected($user,$speciality,$region)
    {
        // dd('stop');
        $retrieved='';
        $isvalid=[];

            // dd($user);

            if(!count($user) > 0)
            {
                foreach ($user as $key => $value) {
                   $retrieved.=','.(string) $value->id;
                }

                $retrieved=explode(',',trim($retrieved,','));
                $users=User::whereNotIn('id',$retrieved)->get();
                // dd($users);
                foreach ($users as $key => $value) {
                    if($value->isAutority($speciality) && 
                        $value->profileOfLawyer()->first()->province == $region){
                        $isvalid=$users;
                    // dd($isvalid);
                    }

                }

                return $isvalid;

            }
    
            return $isvalid;

    }

    public function send($lawyer)
    {
        // dd($lawyer);
        if(empty(User::find($lawyer)))
        {
            return redirect('/');
        }

        session()->put(['lawyer'=>$lawyer]);

        return redirect()->route('create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

       return view('lawyer.consulting.add_consulting');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
                'reason'        =>'required|string',
                'explanation'   =>'required|string',
                'name'          =>'required|string|max:255',
                'lastname'      =>'required|string|max:255',
                'firstname'     =>'required|string:max:255',
                'number'        =>'required|unique:profiles,phone|regex:/^[243]{3}[0-9]{9}$/',
                'email'         =>'email'
            ],[
                'number.regex'=>'Le numero de telephone que vous avez renseigné n\'est pas valide',
                'number.required'=>'Le numero de téléphone est obligatoire',
                'number.unique'=>'Ce numero de téléphone est déjà utilisé'
            ]);

        // try{

            $client=Client::create([
                'Name'          => $request->name,
                'Lastname'      => $request->lastname,
                'Firstname'     => $request->firstname,
                'Number'        => $request->number,
                'Email'         => $request->email?$request->email:'',
                
            ]);

            $consulting=Consulting::create([
                'Reason'        =>$request->reason,
                'Explanation'   =>$request->explanation,
                'user_id'       =>session()->get('lawyer'),
                // 'user_id'       =>$client->id
            ]);
            
            Client::where('id',$client->id)->update(['consulting_id'=>$consulting->id]);
            

            

            // $notify_by=Setting::where('TypeSetting',Setting::NOTIFICATION_GAT_AWAY)->first();
         
            // switch ($notify_by->Value) {
            //     case Setting::NOTIFICATION_POSSIBILITY[0]:
                    // dd("L'avocat sera notifié par sms");
                //     break;
                // case Setting::NOTIFICATION_POSSIBILITY[1]:  
                //     # code...
                    // dd("Notification par mail");
                //     break;
                // case Setting::NOTIFICATION_POSSIBILITY[2]:
                //     # code...
                    // dd("Notification en inbox");
                    // break;
            // }

            session()->put(['message'=>'Dossier envoyé avec succès']);

            return back();

        // }catch(\Exception $e)
        // {

        // }


        // dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function checkout(Request $request)
    {

        $this->validate($request,[
            'email'=>'required|email',
            'code'=>'required|numeric'
        ]);

        if($client=Client::where('email',$request->email)->first())
        {
            session()->put('client',$client);

            return redirect()->route('infollow');
        }

        // dd($request->all()); 
    }

    public function infollow()
    {

        $messages=session()->get('client')->Consulting()->first()->messages()->get();
        // dd($messages->last());
        $sender_type='client';
        $lawyer=null;
 
        // session()->get('client')->Consulting()->first()->messages()->first()->user_id;
        if(session()->get('client')->Consulting()->first()->messages()->first()){
                   $lawyer=User::find(session()->get('client')->Consulting()->first()->messages()->first()->user_id)->profileOfLawyer()->first();
        }else{
            $m=User::find(session()->get('client')->Consulting()->first()->user_id)->get()->id;
            Conversation::create([
                    'message'=> '',
                    'sender_type'=>'client',
                    'message_type'=>'MESSAGE_TEXT',
                    'consulting_id'=>session()->get('client')->Consulting()->first()->id,
                    'user_id'=>$m
                ]);
        }
        $consulting=session()->get('client')->Consulting()->first()->id;
    
        
        return view('lawyer.conversation.discusion',compact('messages','id','sender_type','lawyer','consulting','count'));  
    }


    public function save(Request $request)
    {
            // dd($request->all());
        $help=Help::where('speciality_id',$request->help)->where('user_id',auth()->user()->id)->first();
        


        if($help){
            Help::where('speciality_id',$request->help)->where('user_id',auth()->user()->id)->first()->delete();
            session()->put('message','Retrait de l\'aide éfféctué');
            return back();
        }else{
                Help::create([
                'speciality_id'=>$request->help,
                'user_id'=>auth()->user()->id
             ]);



        session()->put('message','Configuration éfféctuée avec succès');
            return back();

        }

       
    }


}
