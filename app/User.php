<?php

namespace App;

use App\Models\Billing;
use App\Models\Capability;
use App\Models\DefaultInstitutionAdmin;
use App\Models\Feature;
use App\Models\OfficeManager;
use App\Models\Personnel;
use App\Models\School;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;

use App\Models\Profile;
use App\Models\Setting;
use App\Models\Consulting;
use App\Conversation;
use App\Models\Help;
use App\Models\Autority;
use App\Models\Speciality;

class User extends Authenticatable
{
    use Notifiable;

    const SUPER_ADMIN_LEVEL = 1;
    const ADMIN_LEVEL = 2;
    const ADMIN_USER = 3;

    const PAYMENT_STATUT= 'INVALID';

    protected $features;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email','password','barreaux_id','profile_id','legal_fields','state','remember_token','autority_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function articles()
    {
        return $this->hasMany(Models\Article::class);
    }

    public function autorities()
    {
        return $this->hasMany(Models\Autority::class);
    }

    public function sms()
    {
        return $this->hasMany(Models\SubscriptionSms::class);
    }

    public function profileOfLawyer()
    {
        return $this->hasOne(Models\Profile::class,'profile_id');
    }

    public function message()
    {
        return $this->hasMany(Conversation::class);
    }

    public function help()
    {
        return $this->hasOne(Help::class);
    }

    public function questions()
    {
        return $this->hasManyThrough('App\Models\Questions','App\Models\Speciality');
    }

    public function adminOf()
    {
        return $this->hasOne(DefaultInstitutionAdmin::class, 'user_id');
    }


    public function profile()
    {
        return $this->hasOne(Models\Profile::class,'profile_id');    
    }

    public function isAutority($id)
    {
        if(count($this->autority()->first()->speciality()->whereId($id)) > 0    )
        {
            return true;
        }

        return false;

    }


    // public function speciality()
    // {
    //     return $this->belongsTo(Models\Speciality::class,'specialities_id');
    // }

    public function setting()
    {
        return $this->hasOne(Models\Setting::class);
    }

    public function autority()
    {
        return $this->belongsTo(Autority::class,'autority_id');
    }

    public function fileOnStandbyOfTreatment() 
    {
        return $this->hasMany(Models\Consulting::class,'user_id');
    }

    public function isAdminOfLawyer()
    {
        return $this->id === 1;
    }

    /**
     * Retourne vrai si l'utilisateur est un personnel
     *
     * @return bool
     */
    public function isSchoolUser()
    {
        return $this->personnel->category !== null;
    }

    /**
     * Retourne vrai si l'utilisateur est un super administrateur
     *
     * @return bool
     */
    public function isSAdmin()
    {
        return $this->access_level == self::SUPER_ADMIN_LEVEL &&
            ($this->personnel === null || $this->adminOf == null);
    }

    public function personnel()
    {
        return $this->belongsTo(Personnel::class)->withDefault();
    }

    /**
     * Retourne vrai si l'utilisateur est un administrateur d'un etablissement
     *
     * @return bool
     */
    public function isAdminOfInstitution()
    {
        return $this->adminOf !== null;
    }

    /**
     * Retourne vrai si utilisateur est administrateur d'une école
     *
     * @return bool
     */
    public function isAdminOfSchoolOffice()
    {
        return $this->institution() instanceof School;
    }

    /**
     * Retourne vrai si utilisateur est administrateur d'un bureau de gestionnaire
     *
     * @return bool
     */
    public function isAdminOfOffice()
    {
        return $this->institution instanceof OfficeManager;
    }

    /**
     * Retourne l'établissement d'un personnel qui peut se connecter
     *
     * @return null
     */
    public function getInstitutionAttribute()
    {
        return $this->personnel->institution ?? null;
    }

    /**
     * Retourne l'établissement
     *
     * @return bool|mixed|null
     */
    public function institution()
    {
        if($this->isSAdmin())
        {
            return null;
        }

        if($this->isSchoolUser())
        {
            return $this->institution;
        }

        if($this->isAdminOfInstitution())
        {
            return $this->adminOf->institution;
        }

        return false;
    }

    public function capability()
    {
        return $this->hasOne(Capability::class)->withDefault();
    }

    public function canShow($feature_id)
    {
        $features = Cache::remember('features', 60*60*24, function(){
            return Feature::with(['submenus', 'parent'])->get();
        });
        $feature = $features->find($feature_id);
        return in_array($feature_id, $this->capability->access ?? []) or
            $feature === null or
            in_array($feature->parent->id ?? null, $this->capability->access ?? []);
    }

    public function billings()
    {
        return $this->hasMany(Billing::class);
    }
}
