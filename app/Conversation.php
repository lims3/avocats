<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Conversation extends Model
{
    //

	public $fillable=[
		'sender_type','message','message_type','consulting_id','user_id'
	];

    const MESSAGE_TYPE=[
    	'MESSAGE_TEXT',
    	'MESSAGE_IMAGE'
    ];


    public function lawyer()
    {
    	return $this->hasOne(User::class);
    }
}
