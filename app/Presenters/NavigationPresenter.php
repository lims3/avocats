<?php

namespace App\Presenters;

use App\Transformers\NavigationTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class NavigationPresenter.
 *
 * @package namespace App\Presenters;
 */
class NavigationPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new NavigationTransformer();
    }
}
