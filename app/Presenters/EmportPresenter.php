<?php

namespace App\Presenters;

use App\Transformers\EmportTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EmportPresenter.
 *
 * @package namespace App\Presenters;
 */
class EmportPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EmportTransformer();
    }
}
