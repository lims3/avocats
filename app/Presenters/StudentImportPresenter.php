<?php

namespace App\Presenters;

use App\Transformers\StudentImportTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class StudentImportPresenter.
 *
 * @package namespace App\Presenters;
 */
class StudentImportPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new StudentImportTransformer();
    }
}
