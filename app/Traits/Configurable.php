<?php

namespace App\Traits;

use App\Models\Config;
use App\Models\Option;
use App\Models\School;
use Illuminate\Database\Eloquent\ModelNotFoundException;


trait Configurable
{

    public function config($key=null)
    {
        $configs = Config::when($key, function($q) use ($key) {
            return $q->where('key', $key);
            })
            ->where('configurable_id', $this->id)
            ->where('configurable_type', get_class($this));

        return ($key) ? $configs->first() : $configs->get();
    }

    /**
     * Retourne la liste d'options d'une école
     *
     * @return array
     */
    public function getOptions()
    {

        if(!$this instanceof School)
        {
            throw new ModelNotFoundException('Model non trouvé');
        }

        $options = $this->config('SCHOOL_OPTION');

        $returns = [];

        $allOptions = Option::all();

        foreach($options->value ?? [] as $item)
        {

            if(!is_null($option = $allOptions->find($item)))
            {
                $returns[] = $option;
            }
        }

        return $returns;
    }

}