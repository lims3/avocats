<?php

namespace App\Traits;

use App\Models\Info;
use App\Models\Student;

trait Identifiable

{


    public function info()
    {
        return $this->morphOne(Info::class, 'identifiable');
    }

    public function getFullnameAttribute()
    {
        return $this->info->lastname . ' ' . $this->info->middlename;
    }

    public function generateUsername()
    {
        return str_slug($this->fullname, '_'). '_'. $this->id;
    }

    public function getPictureUrlAttribute()
    {
        return ($this->info->picture == null) ? asset('img/'. strtolower($this->info->sexe). '-default-profile.png') :
            asset('storage/'. $this->info->picture);
    }

    public function isFemale()
    {
        return strtolower($this->info->sexe) == 'f';
    }

    /**
     * @return bool
     */
    public function isStudent()
    {
        return $this instanceof Student;
    }

}