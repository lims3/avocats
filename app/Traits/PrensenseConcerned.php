<?php

namespace App\Traits;

use App\Models\Presence;
use Carbon\Carbon;

trait PrensenseConcerned
{
    /**
     * Retourne la présence du jour d'un concerné
     *
     * @return mixed
     */
    public function presence()
    {
        return $this->morphOne(Presence::class, 'concerned')
            ->whereBetween('created_at', [Carbon::today(), now()]);
    }

    /**
     * Retourne les présences présent d'un concerné
     *
     * @return mixed
     */
    public function present()
    {
        return $this->morphMany(Presence::class, 'concerned')
            ->where('state', Presence::SATE['PRESENT']);
    }

    /**
     * Retourne les présences "absent" d'un concerné
     *
     * @return mixed
     */
    public function absent()
    {
        return $this->morphMany(Presence::class, 'concerned')
            ->where('state', Presence::SATE['ABSENT']);
    }

    /**
     * Retourne les présences "malade" d'un concerné
     *
     * @return mixed
     */
    public function sick()
    {
        return $this->morphMany(Presence::class, 'concerned')
            ->where('state', Presence::SATE['MALADE']);
    }
}