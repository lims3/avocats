<?php

namespace App\Traits;

trait Localizable
{
    public function getFormatedAddressAttribute()
    {
        $addr = $this->getAddress();
        $address = (!isset($addr['number'])) ? null: $addr['number'] . ', ';
        $address .= (!isset($addr['avenue'])) ? null : 'av. ' .$addr['avenue']. ', ';
        $address .= (!isset($addr['quartier'])) ? null : 'Q/ ' .$addr['quartier']. ', ';
        $address .= (!isset($addr['avenue'])) ? null : 'C/ ' .$addr['commune'];

        return trim($address, ', ');
    }

    public function getAddress()
    {
        return $this->info->address;
    }
}