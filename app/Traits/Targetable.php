<?php

namespace App\Traits;

use App\Models\Communication;

trait  Targetable
{
    public function communiques()
    {
        return $this->morphMany(Communication::class, 'target');
    }
}