<?php
namespace App\Traits;

trait HasOfficeManager
{

    public function officeManager()
    {
        return $this->morphOne(\App\Models\OfficeManager::class, 'office'); 
    }

}