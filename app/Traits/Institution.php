<?php

namespace App\Traits;

use App\Models\Billing;
use App\Models\Cost;
use App\Models\DefaultInstitutionAdmin;
use App\Models\Group;
use App\Models\Parcour;
use App\Models\Personnel;
use App\Models\Presence;
use App\Models\School;
use App\Models\Session;
use App\Models\Student;
use App\Models\Subgroup;
use App\User;
use Illuminate\Database\Eloquent\Collection;

trait Institution
{
    /**
     * Retourne l'administrateur par défaut d'une institution
     *
     * @return mixed
     */
    public function admin()
    {
        return $this->morphOne(DefaultInstitutionAdmin::class, 'institution');
    }

    /**
     * Retourne tous les personnels d'une institution
     *
     * @return mixed
     */
    public function personnels()
    {
        return $this->morphMany(Personnel::class, 'institution');
    }

    /**
     * Retourne les personnels adminsitratifs d'une institution
     *
     * @return mixed
     */
    public function administratives()
    {
        return $this->morphMany(Personnel::class, 'institution')->administratives();
    }

    /**
     * Retourne les personnels enseignants d'une institution
     *
     * @return mixed
     */
    public function teachers()
    {
        return $this->morphMany(Personnel::class, 'institution')->teachers();
    }

    public function workers()
    {
        return $this->morphMany(Personnel::class, 'institution')->workers();
    }

    /**
     *
     */
    public function presense()
    {

    }

    /**
     *
     *
     * @return mixed
     */
    public function costs()
    {
        return $this->morphMany(Cost::class, 'institution')->orderBy('classe');
    }

    /**
     * Retourne
     *
     * @return Collection|static
     */
    public function billings()
    {
        $return = new Collection();
        $personnelsHaveAccount = $this->administratives->filter(function($personnel){
            return $personnel->hasUserAccount();
        });

        $personnelsHaveBillings = $personnelsHaveAccount->filter(function($personnel){
            return $personnel->user->billings->count() ?? null;
        });

        foreach ($personnelsHaveBillings as $personnel)
        {
            $return = $return->merge($personnel->billingsOf);
        }

        return $return;
    }

    /**
     * Retourne vrai si l'institution est un établissement scolaire
     *
     * @return bool
     */
    public function isSchool()
    {
        return $this instanceof School;
    }

    /**
     * Retourne vrai si l'institution est un bureau gestionnaire de sous-groupe
     *
     * @return bool
     */
    public function isSubgroup()
    {
        return $this->office instanceof Subgroup;
    }

    public function isGroup()
    {
        return $this->office instanceof Group;
    }


}