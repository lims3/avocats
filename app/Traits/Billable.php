<?php

namespace App\Traits;

use App\Models\Billing;
use Illuminate\Database\Eloquent\Model;

trait Billable
{
    static $CDF = 0;
    static $USD = 1;

    /**
     * @param $amount
     * @param $institution
     * @param $devise
     * @return float|int
     * @throws \Exception
     */
    static public function setAmount($amount, Model $institution, $devise)
    {
        if($devise == self::$CDF)
        {
            return $amount / $institution->config('SCHOOL_RATE')->value;
        }
        return $amount;
    }

    public function amountCdf()
    {
        return (int)($this->amount * $this->rate ?? auth()->user()->institution()->config('SCHOOL_RATE')->value);
    }
}