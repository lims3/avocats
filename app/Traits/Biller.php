<?php

namespace App\Traits;

use App\Models\Billing;
use App\Models\Cost;
use App\Models\Session;

trait Biller
{
    public function billings()
    {
        $currentSession = Session::current();
        return $this->morphMany(Billing::class, 'billable', 'biller_type', 'biller_id')
            ->where('created_at', '>=', $currentSession->begin_at)
            ->when($currentSession->end_at, function($q) use ($currentSession){
                return $q->where('created_at', '<=', $currentSession->end_at);
            })->orderBy('created_at', 'desc');
    }

}