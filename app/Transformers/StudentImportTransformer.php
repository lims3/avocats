<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\StudentImport;

/**
 * Class StudentImportTransformer.
 *
 * @package namespace App\Transformers;
 */
class StudentImportTransformer extends TransformerAbstract
{
    /**
     * Transform the StudentImport entity.
     *
     * @param \App\Models\StudentImport $model
     *
     * @return array
     */
    public function transform(StudentImport $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
