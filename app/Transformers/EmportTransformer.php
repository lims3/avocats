<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Emport;

/**
 * Class EmportTransformer.
 *
 * @package namespace App\Transformers;
 */
class EmportTransformer extends TransformerAbstract
{
    /**
     * Transform the Emport entity.
     *
     * @param \App\Models\Emport $model
     *
     * @return array
     */
    public function transform(Emport $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
