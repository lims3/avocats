<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SchoolRepository.
 *
 * @package namespace App\Contracts;
 */
interface SchoolRepository extends RepositoryInterface
{
    //
}
