<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface StudentRepository.
 *
 * @package namespace App\Contracts;
 */
interface StudentRepository extends RepositoryInterface
{
    //
}
