<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NavigationRepository.
 *
 * @package namespace App\Contracts;
 */
interface NavigationRepository extends RepositoryInterface
{
    //
}
