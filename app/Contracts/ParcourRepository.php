<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ParcourRepository.
 *
 * @package namespace App\Contracts;
 */
interface ParcourRepository extends RepositoryInterface
{
    //
}
