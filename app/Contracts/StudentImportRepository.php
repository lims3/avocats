<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface StudentImportRepository.
 *
 * @package namespace App\Contracts;
 */
interface StudentImportRepository extends RepositoryInterface
{
    //
}
