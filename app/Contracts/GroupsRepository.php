<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface GroupsRepository.
 *
 * @package namespace App\Contracts;
 */
interface GroupsRepository extends RepositoryInterface
{
    //
}
