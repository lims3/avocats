<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SectionRepository.
 *
 * @package namespace App\Contracts;
 */
interface SectionRepository extends RepositoryInterface
{
    //
}
