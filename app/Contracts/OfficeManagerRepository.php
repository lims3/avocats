<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OfficeManagerRepository.
 *
 * @package namespace App\Contracts;
 */
interface OfficeManagerRepository extends RepositoryInterface
{
    //
}
