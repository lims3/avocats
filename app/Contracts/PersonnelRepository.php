<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PersonnelRepository.
 *
 * @package namespace App\Contracts;
 */
interface PersonnelRepository extends RepositoryInterface
{
    //
}
