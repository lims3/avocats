<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CommunicationRepository.
 *
 * @package namespace App\Contracts;
 */
interface CommunicationRepository extends RepositoryInterface
{
    //
}
