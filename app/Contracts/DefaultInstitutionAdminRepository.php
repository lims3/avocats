<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DefaultInstitutionAdminRepository.
 *
 * @package namespace App\Contracts;
 */
interface DefaultInstitutionAdminRepository extends RepositoryInterface
{
    //
}
