<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoryStudentRepository.
 *
 * @package namespace App\Contracts;
 */
interface CategoryStudentRepository extends RepositoryInterface
{
    //
}
