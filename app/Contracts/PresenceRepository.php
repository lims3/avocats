<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PresenceRepository.
 *
 * @package namespace App\Contracts;
 */
interface PresenceRepository extends RepositoryInterface
{
    //
}
