<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RegionRepository.
 *
 * @package namespace App\Contracts;
 */
interface RegionRepository extends RepositoryInterface
{
    //
}
