<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BroadcastRepository.
 *
 * @package namespace App\Contracts;
 */
interface BroadcastRepository extends RepositoryInterface
{
    //
}
