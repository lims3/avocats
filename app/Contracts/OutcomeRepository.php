<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OutcomeRepository.
 *
 * @package namespace App\Contracts;
 */
interface OutcomeRepository extends RepositoryInterface
{
    //
}
