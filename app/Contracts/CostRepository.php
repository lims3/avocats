<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CostRepository.
 *
 * @package namespace App\Contracts;
 */
interface CostRepository extends RepositoryInterface
{
    //
}
