<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EmportRepository.
 *
 * @package namespace App\Contracts;
 */
interface EmportRepository extends RepositoryInterface
{
    //
}
