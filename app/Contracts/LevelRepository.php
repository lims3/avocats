<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LevelRepository.
 *
 * @package namespace App\Contracts;
 */
interface LevelRepository extends RepositoryInterface
{
    //
}
