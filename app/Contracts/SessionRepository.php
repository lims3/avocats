<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SessionRepository.
 *
 * @package namespace App\Contracts;
 */
interface SessionRepository extends RepositoryInterface
{
    //
}
