<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface GroupRepository.
 *
 * @package namespace App\Contracts;
 */
interface GroupRepository extends RepositoryInterface
{
    //
}
