<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OptionRepository.
 *
 * @package namespace App\Contracts;
 */
interface OptionRepository extends RepositoryInterface
{
    //
}
