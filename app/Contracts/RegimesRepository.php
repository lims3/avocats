<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RegimesRepository.
 *
 * @package namespace App\Contracts;
 */
interface RegimesRepository extends RepositoryInterface
{
    //
}
