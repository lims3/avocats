<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ConfigRepository.
 *
 * @package namespace App\Contracts;
 */
interface ConfigRepository extends RepositoryInterface
{
    //
}
