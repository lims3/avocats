<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SubgroupRepository.
 *
 * @package namespace App\Contracts;
 */
interface SubgroupRepository extends RepositoryInterface
{
    //
}
