<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface InfoRepository.
 *
 * @package namespace App\Contracts;
 */
interface InfoRepository extends RepositoryInterface
{
    //
}
