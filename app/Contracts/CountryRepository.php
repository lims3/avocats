<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ContryRepository.
 *
 * @package namespace App\Contracts;
 */
interface CountryRepository extends RepositoryInterface
{
    //
}
