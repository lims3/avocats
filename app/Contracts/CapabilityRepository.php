<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CapabilityRepository.
 *
 * @package namespace App\Contracts;
 */
interface CapabilityRepository extends RepositoryInterface
{
    //
}
