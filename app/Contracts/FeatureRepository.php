<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface FeatureRepository.
 *
 * @package namespace App\Contracts;
 */
interface FeatureRepository extends RepositoryInterface
{
    //
}
