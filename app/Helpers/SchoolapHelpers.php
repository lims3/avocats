<?php
define('SALT_LEFT', 'Je cherche la vie un peu partout en bossant dure');
define('SALT_RIGHT', csrf_token());
if(!function_exists('maintenance_mode_text'))
{
    /**
     * Text de mode maintenance
     *
     * @param null $more
     * @return string
     */
    function maintenance_mode_text($more = null)
    {
        return 'Service non disponible pour l\'instant ! Revenez plus tard. '. $more;
    }
}
if(!function_exists('schoolap_text'))
{
    function schoolap_text($text = '')
    {
        return mb_strtoupper($text);
    }
}

if(!function_exists('schoolap_crypt'))
{
    function schoolap_crypt($text)
    {
        return base64_encode(SALT_LEFT . $text . SALT_RIGHT);
    }
}
if(!function_exists('schoolap_decrypt'))
{
    function schoolap_decrypt($text)
    {
        return str_replace([SALT_LEFT, SALT_RIGHT], '', base64_decode( $text));
    }
}
if(!function_exists('usd2cdf'))
{
    function usd2cdf($amount, $prefix = true, $rate=null)
    {

    }
}
if(!function_exists('usd2cdf'))
{
    function cdf2usd($text)
    {
        return str_replace([SALT_LEFT, SALT_RIGHT], '', base64_decode( $text));
    }
}

if(!function_exists('percent'))
{
    function percent($value, $max)
    {
        if(!$max)
            return 0;
        return $value / $max * 100;
    }
}