<?php

use Illuminate\Http\Request;
use App\Models\Article;
use App\Conversation;
use App\User;
use App\Models\Autority;

use App\Models\Questions;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/',function(){

	// dd()
	 
	 // rescue(function(){

			if(auth()->check()){
				return redirect(url('/').'/home');
			}

			$lawyers=User::orderBy('created_at','desc')->orderBy('updated_at','desc')->simplePaginate(4);
			$specialities=App\Models\Speciality::orderBy('name','asc')->get();
			$articles=Article::orderBy('created_at','desc')->where('state','PUT_ON_LINE')->simplePaginate(4);

			return view('papers',compact('articles','specialities','lawyers'));
			
	 // });
	
});


// Route::get('/register',function(){
// 	return view('registers');
// });



Route::post('/test/ajax',function(Request $request){

		if(!session()->get('client'))
		{
			return;
		}

		if($request->mtype == 'MESSAGE_TEXT')
		{
			$conversation=Conversation::create([
					'message'=> $request->message,
					'sender_type'=>'client',
					'message_type'=>'MESSAGE_TEXT',
					'consulting_id'=>(int) $request->cid,
					'user_id'=>(int) $request->uid
				]);

			return response()->json(['isSend'=>true,'conv'=>$conversation->id]);
		}else{


		}
		return response()->json(['données'=>$request->all()]);
});


Route::get('/test/message',function(Request $request){

		if(!session()->get('client'))
		{
			return;
		}

		if($request->ajax())
		{
			return response()->json([
				'isSend'=>true,
				'conv'=>session()->get('client')->Consulting()->first()->messages()->get()->last()->id,
				'message'=> session()->get('client')->Consulting()->first()->messages()->get()->last()
			]);
		}
});


Route::get('/faq','QuestionController@faq');

Route::get('/question/create','QuestionController@create')->name('question');


Route::get('/question/show/{id}','QuestionController@show')->name('faq');

Route::get('/follow',function(){
	return view('lawyer.consulting.follow');
});

// Route::get('/documents','DocumentController@index',function(){
// 	return 'charment des documents';
// });

Route::get('/docs','DocumentController@index')->name('document.index');

Route::post('/checkout','ConsultingController@checkout')->name('checkout');

Route::get('/client/infolow','ConsultingController@infollow')->name('infollow');

Route::post('/store','QuestionController@store')->name('save');

Route::post('/test',function(Request $request){
	dd($request->all());
});

Route::post('/help/save','QuestionController@store')->name('help.save');

Route::group(['prefix'=>'avocat','middelware'=>['auth',
								  'AccountCheck']],function(){
	Route::get('/checkout','HomeController@checkout')->name('lwck');

	Route::get('/paymentCheckout','PaymentCheckout@index');

	Route::get('settings/profession','SettingsController@profession')->name('profession');

	Route::get('/article/delete/{id}','ArticleController@delete')->name('delete');

	Route::resources(['article'=>'ArticleController']);


	Route::resources(['autority'=>'AutorityController']);
	Route::resources(['sms'=>'SubscriptionSmsController']);
	// Route::get('/sms','SubscriptionSms@subscribe');

	Route::resources(['profile'=>'ProfileController']);

	Route::resources(['setting'=>'SettingsController']);

	Route::resources(['notification'=>'NotificationController']);


	// Route::resources(['consulting'=>'ConsultingController']);
	Route::post('/consulting/apply','ConsultingController@apply')->name('apply');

	Route::get('/consulting/treating/{id}','ConsultingController@treating')->name('treating');

	Route::post('/avocat/article/apply','ArticleController@redirect_by_action')->name('byaction');
	Route::post('/avocat/article/update_article','ArticleController@update_article')
				->name('update');
	Route::get('/question/','QuestionController@index')->name('response');

	// Route::resources(['question'=>'QuestionController']);

	Route::post('/question/response','QuestionController@response')->name('provide-answers');


	Route::get('/question/answers/{id}','QuestionController@answers')->name('answers');

	Route::get('/help/','ConsultingController@index')->name('help');

	Route::post('/help/save','ConsultingController@save')->name('save');


	Route::post('/active','HomeController@active')->name('active');


});

Route::group(['prefix'=>'admin'],function(){
		Route::get('/account','RootController@checklist')->name('checklist');
		Route::get('/allowed/{id}','RootController@allowed')->name('allowed');
		Route::get('/articles','RootController@articles')->name('articles');
		Route::resources(['document'=>'DocumentController']);
});

Route::post('/consulting/search','ConsultingController@search')->name('search');

Route::get('/consulting/send/{lawyer}','ConsultingController@send')->name('send');

Route::get('/consulting/create','ConsultingController@create')->name('create');

Route::get('/consulting/show/{id}','ConsultingController@show')->name('show');



Route::post('/consulting/store','ConsultingController@store')->name('store');


// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
	