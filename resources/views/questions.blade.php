@extends('layouts.stability')

	@section('content')
			<div class="container" style="padding-top:65px;padding-right:30px;padding-left:30px;">
		      <div class="row">
		        <div class="col-md-4 col-md-offset-4 text-center ">

		          <h3 style="color:#1b2254e3;font-size:33px;margin-bottom:-6px;">Posez votre question!</h3>
		          <hr>
		        </div>
		        <form action="{{route('help.save')}}" method="post">
		        	{{csrf_field()}}
		      <div class="row">
		        <div class="col-md-8 col-md-offset-2" >
		          <div class="panel panel-default" style="border-radius:0;border-top:3px solid #1b2254e3;box-shadow:0px 0px 7px rgba(0,0,0,0.40);">
		          <!--   <div class="panel-heading" style=";border-radius:0;color:#1b2254e3;text-align:center;font-size:18px;">
		              <span>Inscrivez-vous!</span>
		            </div> -->
		          <!-- <div class="panel-heading" style="background:#1b2254e3;">
		             <p style="text-align: center;"><i style="font-size:50px;color:#fff;" class="glyphicon glyphicon-envelope"></i></p>
		          </div> -->
		          <div class="panel-body">
		              <p style="text-align: center;"><i style="font-size:50px;color:#ccc;" class="glyphicon glyphicon-question-sign"></i></p>
		            <hr>
		            

		            @if($errors->any())
		           
		            	<div class="row">
		            		<div class="col-md-12">
		            			<div class="alert alert-danger">
		            			@foreach($errors->all() as $error)

				            		<li>{{$error}}</li>

				            	@endforeach
		            		</div>

		            		</div>
		            	</div>

		            @endif
		          	<div class="row">
		          		<div class="col-md-12">
		          			  <div class="form-group">
		            	        @include('inc.message')
		                      </div>
		          		</div>
		          	</div>
		          	<div class="row">
		          		<div class="col-md-12">
		          			<div class="form-group">
		                   	<select required="required" name="speciality" class="form-control" style="border-radius:0;">
		                   		<option>Thematique<eoption>

				                   @foreach($specialities as $speciality)

				                   		  <option value="{{$speciality->id}}">{{$speciality->name}}</option>

				                   @endforeach
		                   	</select>
		               </div>
		          		</div>
		          	</div>
		            <div class="row">
		            	<!-- <div class="input-group"> -->
		            		<div class="col-md-6">
		            			<div class="form-group">
		            				<div class="input-group">
		            		   		<input type="text" style="border-radius:0;" placeholder="email" class="form-control" name="">
		            		   		<span class="input-group-addon custom-addon-other"><i class="glyphicon-envelope"></i></span>
		            		   	</div>
		            		   	<br>
		            		   	 	<p>Indiquez votre e-mail pour obtenir la réponse de l'avocat.Votre question sera anonyme</p>
		            			</div>
		            	    </div>
		            	    <div class="col-md-6">
		            		   <div class="form-group">
		            		   	<div class="input-group">
		            		   		<!-- <span class="input-group-addon custom-addon-other">+243</span> -->
		            		   			<input type="text" style="border-radius:0;" placeholder="téléphone ex:+243" class="form-control" name="">
		            		   			<span class="input-group-addon custom-addon-other"><i style="font-style:normal;" class="glyphicon glyphicon-phone"></i></span>
		            		   	</div>

		            		   </div>
		            	    </div>
		            	<!-- </div> -->
		            </div>
		            <div class="row">
		            	<div class="col-md-12">
		            		
		               		<div class="form-group">
				                 <textarea  required="required" class="form-control" rows="6" name="question" style="border-radius:0;text-align:left;">
				                 	
				                 </textarea>
		               		</div>
		            	</div>
		            </div>
		            <div class="row">
		            	<div class="col-md-12">
		            		<a href="#">En cliquant poser la question vous accepterez le CUG.</a>
		            	</div>
		            </div>
		     	
		                <div class="form-group">
		                 <button class="btn btn-primary " style="background:#1b2254e3;border-radius:0;height:43px;float:right;">Posez la question</button>
		               </div>
		          </div>
		        </div>  
		      </div>
		    </div>
			</form>
		  </div>
		</div>
		</div>

@stop

