


<!DOCTYPE html><html class=''>
<head>

<!-- <link rel='stylesheet prefetch' href=''> -->
<title>Avocat.cd | suivez l'évolution de votre dossier facilement</title>
<link href="{{asset('css/news/css/bootstrap.css')}}" rel="stylesheet" id="bootstrap-css">
<!-- <link href="bootstrap.css" rel="stylesheet" id="bootstrap-css"> -->
<!-- <script src="jquery.js"></script> -->
<!-- <script src="bootstrap.min.js"></script> -->



<!-- <script src="bootstrap.min.js"></script> -->


<style class="cp-pen-styles">
  .field-box{
    border-bottom:3px solid #1b2254e3;
  }
  .field-text{
    background:#1b2254e3;
    color:#fff;
    padding-top:6px;
    padding-left:3px;
    padding-right:3px;
  }
  .custom-btn
  {
    /*background:#1b2254e3 !important;*/
    border-radius:0 !important;
    border-left:0 !important;
    background:#fff !important;
    height:35px !important;


    /*border:0;*/
     height:40px !important;
  }
  .custom-addon
  {
    border-radius:0 !important;
    background:#fff !important;
    border-right:0 !important;
    color:#ccc !important;

  }
  .separator
  {
    height:5px;
    border-top:2px solid red;
    width:400px;
    position:absolute;
    /*width:100px;*/
    /*transform:translateX(50%); */
    /*transform:rotate(20%,20%);*/
   -webkit-transform: rotateX(90);
  }
</style>

</head>
<body style="overflow:hidden;background:#FAFAFA;">
    <div class="container" style="padding-top:70px;">
      <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center ">

          <h3 style="color:#1b2254e3;font-size:33px;margin-bottom:-6px;">Suivez votre dossier!</h3>
          <hr>
        </div>
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <div class="panel panel-default" style="border-radius:0;border-top:3px solid #1b2254e3;box-shadow:0px 0px 7px rgba(0,0,0,0.40);">
          <!--   <div class="panel-heading" style=";border-radius:0;color:#1b2254e3;text-align:center;font-size:18px;">
              <span>Inscrivez-vous!</span>
            </div> -->
          <!-- <div class="panel-heading" style="background:#1b2254e3;">
             <p style="text-align: center;"><i style="font-size:50px;color:#fff;" class="glyphicon glyphicon-envelope"></i></p>
          </div> -->
          <div class="panel-body">
              <p style="text-align: center;"><i style="font-size:50px;color:#ccc;" class="glyphicon glyphicon-envelope"></i></p>
            <hr>
             <div class="form-group">
                 <div class="input-group">
                    <span class="input-group-addon custom-addon" id="basic-addon1"><i class="glyphicon glyphicon-envelope"></i></span>
                    <input type="text" class="form-control custom-btn" placeholder="adresse mail" aria-describedby="basic-addon1">
                  </div>
               </div>
               <div class="form-group">
                 <div class="input-group">
                    <span class="input-group-addon custom-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="text" class="form-control custom-btn" placeholder="code de suivi" aria-describedby="basic-addon1">
                  </div>
               </div>
                <div class="form-group">
                 <button class="btn btn-primary btn-block" style="background:#1b2254e3;border-radius:0;height:43px;">Suivre</button>
               </div>
          </div>
        </div>  
      </div>
    </div>
  </div>
</div>
</div>
</body>
</html> 