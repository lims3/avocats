<!doctype html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('css/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/vendor/linearicons/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/vendor/chartist/css/chartist-custom.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/demo.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
    <style>
        body{
            font-family: 'Roboto', 'sans-serif';
        }
        .table-heading{
            text-align:center;
            background-color: #00AAFF;
            color: white;
        }
    </style>
</head>
<body>
<!-- WRAPPER -->
<div id="wrapper">
    <!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="brand">
            <a href="{{ route('home') }}">
                <img src="assets/img/logo-dark.png" alt="SCHOOLAP"
                                      class="img-responsive logo">
            </a>
        </div>
                                    {{--  {{dd(auth()->user())}}  --}}
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
            </div>
            {{--  <form class="navbar-form navbar-left">
                <div class="input-group">
                    <input type="text" value="" class="form-control" placeholder="Search dashboard...">
                    <span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>
                </div>
            </form>  --}}
            <div class="navbar-btn navbar-btn-right">
                &nbsp;<a href="#"><i class="fa fa-user"></i> {{ auth()->user()->username }}</a>
            </div>
            <div id="navbar-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <form action="{{ route('home') }}">
                            <button type="submit" class="btn btn-defaut"><i class="lnr lnr-home"></i> Accueil</button>
                        </form>
                    </li>
                    <li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="">
                            {!! csrf_field() !!}
                            <button class="btn btn-success" type="submit">Sé deconnecter</button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    {{-- Sidebar  --}}
    @include('inc.sidebar')
    {{-- End of sidebar --}}
    <!-- END NAVBAR -->
    <!-- LEFT SIDEBAR -->

    <!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    <div class="main">
        <div class="main-content">
            @if(session()->has('message'))
                <div data-toggle="alert-dismissable" role="alert" class="alert alert-{{ session('message')['type'] ?? 'success' }}">
                    <i class="fa fa-{{ session('message')['type'] ?? 'success' }}"></i>
                    {{ session('message')['content'] ?? session('message') }}
                </div>
            @endif
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/plugin/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('js/plugin/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/plugin/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('js/plugin/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('js/plugin/vendor/chartist/js/chartist.min.js')}}"></script>
<script src="{{asset('js/plugin/klorofil-common.js')}}"></script>
@yield('scripts')

</body>
</html>