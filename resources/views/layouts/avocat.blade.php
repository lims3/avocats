<!doctype html>
<html lang="en">
  <head>
    <title>Free Education Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet"> -->

    <link rel="stylesheet" href="{{asset('css/custom/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom/owl.carousel.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/custom/fonts/ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom/fonts/fontawesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom/fonts/flaticon/font/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('css/custom/style.css')}}">
    <!-- Theme Style -->
    @yield('other')
    <style type="text/css">
        .btn .btn-primary {background:#060f26cf;}

    </style>
    <script type="text/javascript">
        // var btn=document.querySelectorAll('#btn-custom');
        //     btn[0].style.background="#060f26cf";
        //     btn[1].style.background="#060f26cf";

    </script>
  </head>

  <body>
     <header role="banner">
     
      <nav class="navbar navbar-expand-lg navbar-light bg-light" style="height:70px;">
        <div class="container">
          <a class="navbar-brand absolute" href="index.html">Avocat.cd</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse navbar-light" id="navbarsExample05">
            <ul class="navbar-nav mx-auto">
              <li class="nav-item">
                <!-- <a class="nav-link active" href="index.html">Accueil</a> -->
              </li>
              <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="courses.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Courses</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="courses.html">HTML</a>
                  <a class="dropdown-item" href="courses.html">WordPress</a>
                  <a class="dropdown-item" href="courses.html">Laravel</a>
                  <a class="dropdown-item" href="courses.html">JavaScript</a>
                  <a class="dropdown-item" href="courses.html">Python</a>
                </div>

              </li> -->
<!-- 
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories</a>
                <div class="dropdown-menu" aria-labelledby="dropdown05">
                  <a class="dropdown-item" href="#">HTML</a>
                  <a class="dropdown-item" href="#">WordPress</a>
                  <a class="dropdown-item" href="#">Laravel</a>
                  <a class="dropdown-item" href="#">JavaScript</a>
                  <a class="dropdown-item" href="#">Python</a>
                </div>

              </li> -->
            <!--   <li class="nav-item">
                <a class="nav-link" href="blog.html">Blog</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="about.html">About</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="contact.html">Contact</a>
              </li> -->
            </ul>
            <ul class="navbar-nav absolute-right">
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="courses.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Questions</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="courses.html">HTML</a>
                  <a class="dropdown-item" href="courses.html">WordPress</a>
                  <a class="dropdown-item" href="courses.html">Laravel</a>
                  <a class="dropdown-item" href="courses.html">JavaScript</a>
                  <a class="dropdown-item" href="courses.html">Python</a>
                </div>

              </li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="courses.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Mon compte</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="courses.html">Créer un compte</a>
                  <!-- <a class="dropdown-item" href="courses.html">WordPress</a> -->
                  <a class="dropdown-item" href="courses.html">Se connecter</a>
                  <!-- <a class="dropdown-item" href="courses.html">JavaScript</a> -->
                  <!-- <a class="dropdown-item" href="courses.html">Python</a> -->
                </div>

              </li>
              <!-- <li><a href="">Connexion</a></li> -->
              <!-- <li> -->
                <!-- <a href="login.html">Connexion</a>  -->
                <!-- / <a href="register.html">Register</a> -->
              <!-- </li> -->
            </ul>
            
          </div>
        </div>
      </nav>
    </header>
    <!-- END header -->


    <section class="site-hero overlay" data-stellar-background-ratio="0.5" style="background-image: url({{asset('img/avocats.jpg')}});">
      <div class="container">
        <div class="row align-items-center justify-content-center site-hero-inner">
          <div class="col-md-10">
  
            <div class="mb-5 element-animate">
              <div class="block-17">
                <h2 class="heading text-center mb-4">Trouver un avocat facilement!</h2>
                <form action="{{route('search')}}" method="post" class="d-block d-lg-flex mb-4">
                  {{csrf_field()}}
                  <div class="fields d-block d-lg-flex">
                    <div class="textfield-search one-third"><input type="text" class="form-control" placeholder="Keyword search..."></div>
                    <div class="select-wrap one-third">
                      <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                      <select name="legalfield" id="" class="form-control">
                        <option value="">Domaine d'activité</option>
                        <!-- <option value="">Laravel</option>
                        <option value="">PHP</option>
                        <option value="">JavaScript</option>
                        <option value="">Python</option> -->
                      </select>
                    </div>
                    <div class="select-wrap one-third">
                      <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                      <select name="entity" id="" class="form-control">
                        <option value="">Barréaux</option>
                        <option value="">Beginner</option>
                        <option value="">Intermediate</option>
                        <option value="">Advance</option>
                      </select>
                    </div>
                  </div>
                  <!-- <input type="submit" class="search-submit btn btn-primary" style="background:#060f26cf;" value="Search">   -->
                  <button type="submit" class="search-submit btn btn-primary" id="btn-custom" style="background:060f26cf;">trouver!</button>
                </form>
                <p class="text-center mb-5">We have more than 500 courses to improve your skills</p>
                <p class="text-center"><a href="#" class="btn py-3 px-5" id="btn-custom" style="background:#060f26cf;">Suivre mon dossier</a></p>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

        @yield('content')

        <!-- <H1>Bienvenue sur avocat.cd</H1> -->

        <script src="{{asset('css/custom/js/jquery-3.2.1.min.js')}}"></script>
        <script src="{{asset('css/custom/js/jquery-migrate-3.0.0.js')}}"></script>
        <script src="{{asset('css/custom/js/popper.min.js')}}"></script>
        <script src="{{asset('css/custom/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('css/custom/js/owl.carousel.min.js')}}"></script>
        <script src="{{asset('css/custom/js/jquery.waypoints.min.js')}}"></script>
        <script src="{{asset('css/custom/js/jquery.stellar.min.js')}}"></script>
        <script src="{{asset('css/custom/js/jquery.animateNumber.min.js')}}"></script>
        
        <script src="{{asset('css/custom/js/jquery.magnific-popup.min.js')}}"></script>

        <script src="{{asset('css/custom/js/main.js')}}"></script>
  </body>