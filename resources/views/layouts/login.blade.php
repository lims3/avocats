<!DOCTYPE html>
<html>
<head>
	<title>Avocat.cd| Connexion au compte avocat</title>

	<!-- <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css"> -->

	 <!-- <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}"> -->
	 <meta name="viewport" content="width=device-width, initial-scale=1">
	 <link rel="stylesheet" href="{{asset('css/custom/other/util.css')}}">
	 <link rel="stylesheet" href="{{asset('css/custom/other/main.css')}}">
	 <link href="{{asset('css/news/css/pretty.min.css')}}" rel="stylesheet" id="bootstrap-css">

</head>
<body >

	@yield('content');

<script type="text/javascript" src="{{asset('css/news/js/jquery.min.js')}}"></script>

<script type="text/javascript" src="{{asset('css/custom/other/main.js')}}"></script>
<script type="text/javascript">
	
$("input[name='password']").val('');
	   
</script>
</body>
</html>

