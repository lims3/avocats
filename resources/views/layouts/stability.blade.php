
  
<!-- /*
* Avocats.cd is Developed by :
* Name: Lims Mbanza 
* Email: limsmbanza3@gmail.com
*
*/ -->


<!DOCTYPE html><html class=''>
<head>

<!-- <link rel='stylesheet prefetch' href=''> -->
<title>Avocat.cd | @yield('title')</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="Ohelene" />


<meta property="og:title" content=""/>
<meta property="og:image" content=""/>
<meta property="og:url" content=""/>
<meta property="og:site_name" content=""/>
<meta property="og:description" content=""/>
<meta name="twitter:title" content="" />
<meta name="twitter:image" content="" />
<meta name="twitter:url" content="" />
<meta name="twitter:card" content="" />
<link href="{{asset('css/news/css/bootstrap.css')}}" rel="stylesheet" id="bootstrap-css">
<link href="{{asset('css/news/css/pretty.min.css')}}" rel="stylesheet" id="bootstrap-css">
<link href="{{asset('css/news/css/style.css')}}" rel="stylesheet" id="bootstrap-css">
<link href="{{asset('css/news/css/selectize.bootstrap3.css')}}" rel="stylesheet" id="bootstrap-css">
<!-- <link href="{{asset('css/news/css/selectize.default.css')}}" rel="stylesheet" id="bootstrap-css"> -->
<link rel="stylesheet" type="text/css" href="{{asset('css/news/font-awesome/css/font-awesome.min.css')}}">
<script type="text/javascript" src="{{asset('css/news/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('css/news/js/bootstrap.min.js')}}"></script>
 <script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{asset('css/news/js/selectize.min.js')}}"></script>
<script type="text/javascript" src="{{asset('css/news/js/index.js')}}"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5c1e68a882491369ba9f347e/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<style class="cp-pen-styles">
  .selectize-control 
  {
    width:350px !important;
  }
 
  @yield('custom-style')

</style>

</head>
@if(isset($toblack))
    <body style="overflow-x:hidden;background:#fff;" >
@else
    <body style="overflow-x:hidden;background:#fafafa;" >
@endif

    <nav class="navbar navbar-default navbar-fixed-top custom-navbar">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}">
            <img alt="Brand" src="{{asset('img/LOGO.jpg')}}" style="width:90px;">
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
        <!-- <li><a href="#">Link</a></li> -->
        <li><a href="{{route('question')}}" style="color:#1b2254e3;font-family:Helvetica;">Posez une question</a></li>
        <li><a href="{{url('/follow')}}" style="color:#1b2254e3;font-family:Helvetica;">Suivre mon dossier</a></li>
        <li><a href="{{url('/faq')}}" style="color:#1b2254e3;font-family:Helvetica;">Questions</a></li>  
        <li><a href="/register" style="color:#1b2254e3;font-family:Helvetica;" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">Créer un compte</a></li>
        <li><a href="{{url('/login')}}" style="" class="btn-login">Connexion</a></li>
        <!-- <li><a href="#blog" style="color:#1b2254e3;">Documents</a></li> -->
        <!-- <li><a href="" style="color:#1b2254e3;">A propos</a></li> -->
      </ul>
      
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


<!-- bar de navigation destinée aux grands écrans -->
    <nav id="navigation" class="navbar navbar-default custom-navbar navbar-fixed-top hidden-xs hidden-sm " style="padding-left:10px;padding-right:5px;">
      <div class="container-fluid custom-container">
         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <div class="navbar-header">
          <a class="navbar-brand" href="{{url('/')}}">
            <img alt="Brand" src="{{asset('img/LOGO.jpg')}}" style="width:90px;">
          </a>
        </div>
        <div class="navbar-form navbar-right" class="hidden-xs hidden-sm" role="search">
         
          <a href="/login" class="btn btn-login btn-default custom-btn hidden-xs hidden-sm"  >Connexion</a >
        </div>
    <!--     <div class="hidden-xs hidden-sm"> -->
     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right " style="padding-top:5px;color:#1b2254e3;">
        <li><a href="{{route('question')}}" style="color:#1b2254e3;font-family:Helvetica;">Posez une question</a></li>
        <li><a href="{{url('/follow')}}" style="color:#1b2254e3;font-family:Helvetica;">Suivre mon dossier</a></li>
        <li><a href="{{url('/faq')}}" style="color:#1b2254e3;font-family:Helvetica;">Questions</a></li>  
        <li><a href="/register" style="color:#1b2254e3;font-family:Helvetica;">Créer un compte</a></li>
        <!-- <li><a href="#blog" style="color:#1b2254e3;">Documents</a></li> -->
        <!-- <li><a href="" style="color:#1b2254e3;">A propos</a></li> -->
        
      </ul>
      </div>
    </div>
    </nav>
    @yield('content')
    <!-- <br>
    <br>
    <br> -->

   

    <script type="text/javascript">
        $('.content-c').html($('.c').text());

        $('.remote-box').on('click',function(){
            // alert($('#navigation').offset().top);
            // $(window).animate({ scrollTop:$('.messages > ul > li').last().offset().top});
            // $(window).animate({duration:500}).scrollTop(0);
            $('html,body').animate({scrollTop:0}, 700,'linear');
        });

        
    </script>

 
</body>
</html>
