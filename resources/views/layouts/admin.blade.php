@inject('pedding','App\Http\Controllers\QuestionController')

<!DOCTYPE html>
<html lang="en" style="background:#fff;">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  
    <title>Avocat.cd | @yield('title') </title>

    <link rel="stylesheet" type="text/css" href="{{asset('css/news/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/news/css/custom.min.css')}}">
    <link href="{{asset('css/news/css/pretty.min.css')}}" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/news/font-awesome/css/font-awesome.min.css')}}">
     <style type="text/css">
      .img-default input
      {
        display:none;

      }

      .img-default
      {
        margin-left:35%;
        /*height:60px;*/
        line-height:40px;
        vertical-align:middle;
        padding-right:50px;
        padding-left:50px;  
        /*background:#1b2254e3;*/
        color:#fff;
      }

       #picture
      {
        display:none !important;
      }

      .picture
      {
        /*display:none !important;*/
        background:#1b2254e3 !important;
        padding:13px !important;
        color:#fff !important;
        border-radius:4px  !important;
        font-family:Helvetica !important; 
      }

      .picture:hover
      {
        cursor:pointer !important;
        opacity:0.6 !important;
      }

      .custom-addon-other
      {
        border-radius:0 !important;
      }
    </style>
  </head>

  <body class="nav-md">
  	<div class="container body">
      <div class="main_container" style="background:#1b2254e3;">
        <div class="col-md-3 left_col" style="background:#1b2254e3;">
          <div class="left_col scroll-view" style="background:#1b2254e3;">
            <div class="navbar nav_title" style="border: 0;background:#1b2254e3;">
              <a href="{{url('/home')}}" class="site_title"><span>&nbsp;Avocat.cd</span></a>
            </div>

            <div class="clearfix"></div>
            <span style="display:none;">{{$pic='avatar.jpg'}}</span>
            @if(auth()->user()->profile->picture()->orderBy('id','desc')->first())
              <span style="display:none;">{{$pic=auth()->user()->profile->picture()->orderBy('id','desc')->first()->Name}}</span>
            @endif
            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
             
                <img 
                src="{{asset('img/upload/'.$pic??'avatar.jpg'
                    )}}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bienvenue,</span>
                <h2>Avocat</h2>
              </div>
            </div>

               <br />

            <!-- sidebar menu -->
              @if(!auth()->user()->isAdminOfLawyer())


                 <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Acceuil 
                  <!-- <span class="fa fa-chevron-down"></span> -->
                </a>
                    <!-- <ul class="nav child_menu">
                      <li><a href="index.html">Dashboard</a></li>
                  
                    </ul> -->
                  </li>
                    <li><a ><i class="fa fa-user"></i>Profil
                  <span class="fa fa-chevron-down"></span>
                </a>
                    <ul class="nav child_menu">
                      <li><a href="{{url('/avocat/profile')}}">Editer</a></li>
                     
                    </ul>
                  </li>
                   <li><a><i class="fa fa-question"></i>Questions posées <span class="badge badge-success">
                      {{$pedding::getCountQuestion()}}
                   </span>
                   <span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                      <li><a href="{{route('response')}}">Voir toutes les questions</a></li>
                      <!-- <li><a href="tables_dynamic.html">dossier en attente de traitement</a></li> -->
                    </ul>
                  </li>
            
                    <!-- <a href="" ><i class="fa fa-wechat"></i><a href="">Conversation</a>  -->
                  <!-- <span class="fa fa-chevron-down"></span> -->
            
                  
                 
                    <!-- <li><a href=""><i class="fa fa-pie-chart"></i> Statistiques  -->
                  <!-- <span class="fa fa-chevron-down"></span> -->
                <!-- </a>
                  
                  </li>  -->
                   <!-- <li><a href=""><i class="fa fa-question"></i> Questions posées  -->
                  <!-- <span class="fa fa-chevron-down"></span> -->
                <!-- </a> -->
                  
                  <!-- </li> -->
                  <li><a><i class="fa fa-envelope-o"></i> Dossiers <span class="badge badge-success">{{isset($dossier)?$dossier:'0'}}</span>  <span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                      <li><a href="{{url('/avocat/notification')}}">Voir tous les dossiers</a></li>
                      <!-- <li><a href="tables_dynamic.html">dossier en attente de traitement</a></li> -->
                    </ul>
                  </li>
                  <li><a disabled="disabled"><i class="fa fa-cogs"></i> Configurations <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a disabled="disabled" href="{{route('sms.create')}}">Souscription au services Sms</a></li>
                      <li><a disabled="disabled" href="{{route('setting.create')}}">Configuration des notifications</a></li>
                      <!-- <li><a href="tables_dynamic.html">Paiement par consultation</a></li> -->
                    </ul>
                  </li>
                  <li><a><i class="fa fa-newspaper-o"></i> Articles <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('article.create')}}">Redaction</a></li>
                      <li><a href="{{route('article.index')}}">Affichage et suppression</a></li>
                      
                    </ul>
                  </li> 
                  <li><a><i style="font-size:18px;" class="glyphicon glyphicon-hand-right"></i> Aide juridique <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{url('/avocat/help')}}">ajouter une aide</a></li>
                      <!-- <li><a href="{{route('article.index')}}">Affichage et suppression</a></li> -->
                      
                    </ul>
                  </li>
                <!--   <li><a ><i class="fa fa-wechat"></i>Conversation  
                  <span class="fa fa-chevron-down"></span>
                </a>
                    <ul class="nav child_menu">
                      <li><a href="fixed_sidebar.html">Voir tout</a></li>
                      <li><a href="fixed_footer.html">Par dossier</a></li>
                    </ul>
                  </li> -->
                </ul>
              </div>
              <div class="menu_section">
             
              </div>

            </div>


              @else

                 <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Acceuil 
                  <!-- <span class="fa fa-chevron-down"></span> -->
                </a>
                    <!-- <ul class="nav child_menu">
                      <li><a href="index.html">Dashboard</a></li>
                  
                    </ul> -->
                  </li>
                    <li><a ><i class="fa fa-cogs"></i>Admnistrer
                  <span class="fa fa-chevron-down"></span>
                </a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('checklist')}}">Activer les comptes</a></li>
                      <li><a href="{{route('articles')}}">Rétirer les articles</a></li>
                     
                    </ul>
                  </li>
                   <li><a ><i class="fa fa-cogs"></i>Gérer les documents
                  <span class="fa fa-chevron-down"></span>
                </a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('checklist')}}">Voir et Editer</a></li>
                      <li><a href="{{route('document.create')}}">Ajouter un document</a></li>
                     
                    </ul>
                  </li>
                
               
                 
                <!--   <li><a ><i class="fa fa-wechat"></i>Conversation  
                  <span class="fa fa-chevron-down"></span>
                </a>
                    <ul class="nav child_menu">
                      <li><a href="fixed_sidebar.html">Voir tout</a></li>
                      <li><a href="fixed_footer.html">Par dossier</a></li>
                    </ul>
                  </li> -->
                </ul>
              </div>
              <div class="menu_section">
             
              </div>

            </div>


              @endif
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small" style="background:#1b2254e3;display:none;">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu" style="background:#fff;">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          
                    <img  style="height:30px;width:30px;" 
                src="{{asset('img/upload/'.$pic??'avatar.jpg'
                    )}}" alt="..." class="img-circle " >
                    {{ucfirst(auth()->user()->profile()->get()[0]->firstname).' '.auth()->user()->profile()->get()[0]->lastname}}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <!-- <li><a href="javascript:;"> Profile</a></li> -->
                 <!--    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Parametre </span>
                      </a>
                    </li> -->
                    <!-- <li><a href="javascript:;">Aide</a></li> -->
                      <li><a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Déconnexion
                                        </a>
                  </li>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                  </ul>
                </li>


                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green"> {{$pedding::getCountQuestion()}}</span>
                  </a>
                <!--   <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a> -->
                        <!-- <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span> -->
                        <!-- <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li> -->
                      <!-- <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a> -->
                        <!-- <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span> -->
                      <!--   <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>Repondre aux Messages </strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li> -->
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
          <!-- <div class=""> -->
           <!--  <div class="page-title">
              <div class="title_left">
                <h3>&nbsp;&nbsp;Souscrivez au service sms </h3>
              </div>

            </div> -->
            <div class="clearfix"></div>
           <!--  <div class="row" style="padding-left:18px;">
              <div class="col-md-12 " style="background:#fff;height:50px;margin-bottom:15px;border-left:4px solid #1b2254e3;box-shadow:0px 0px 2px rgba(0,0,0,0.40);  ">
                <div> -->
                  <!-- <p>Note d'itulisation</p> -->
              <!--     <p>En souscrivant à ce service, vous bénéficiez d'une possibilité qui va vous permettre de récevoir les sollicitations des consultations qui vous séront faites par des clients voulant une assistance juridique directement sur votre téléphone au moyen de sms.</p>
                </div>
              </div>
            </div> -->


            		@yield('content')
            		<div class="clear-fix"></div>
			        <!-- /page content -->

			        <!-- footer content -->
			        <footer style="background: transparent;">
			          <div class="pull-right">
			            <!-- Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a> -->
			          </div>
			          <!-- <div class="clearfix"></div> -->
			        </footer>
			        <!-- /footer content -->
			      <!-- </div>
			    </div> -->

 				<!-- </div> -->
			</div>
		</div>
	</div>
	<script type="text/javascript" src="{{asset('css/news/js/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('css/news/js/bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('css/news/js/bootstrap-wysiwyg.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('css/news/js/custom.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('css/news/js/prettify.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('css/news/js/jquery.hotkeys.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>
       <script type="text/javascript">
          $('form').on('submit',function(){
              $('#descr').val($('#editor-one').html());
          
          }); 


          if($('#retype').html() !="")
          {
            $('#editor-one').html($('#retype').text());
          }

          $("input[type='file']").on('change',function(){
              // alert($(this).val());
              if($(this).val() !="")
              {
                // $('#image-panel').empty().append("<img src='"+$(this).val()+"'/>");
                swal ( "Image" ,  "Image selectionnée avec succès" ,  "success" );

              }
          });


         // $('#addHelp').each(function(value){
            // alert(value)
            var selectedItem    ={};
            var constructString ='';
            $('.addHelp').on('click',function(){
                if(!$(this).attr('data-selected')){
                    $(this).attr('data-selected','selected');

                    if(constructString.length == 0){
                        constructString=$(this).attr('value');
                    }else{
                      constructString+=','+$(this).attr('value');
                    }
                }else{
                   $(this).removeAttr('data-selected');
                   constructString.toString().replace($(this).attr('value'),"3");

                }

               
            })
             $('#selectedItem').attr('value',constructString);
         // })

         $('#sexe').on('change',function(){
            $(this).parent().find("input[name='sexe']").val($(this).val());
         })  

         $('#region').on('change',function(){
            $(this).parent().find("input[name='region']").val($(this).val());
         })

        </script>

            
        
</body>
</html>

