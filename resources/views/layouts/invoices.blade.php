<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Facture/Reçu</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
</head>
<body>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-6">
                    <div class="list-group">
                        <div class="list-group-item">
                            {{ schoolap_text($billing->user->institution()->name) }}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="list-group">
                        <div class="list-group-item">
                            Nom complet : <span class="badge">{{ schoolap_text($billing->biller->fullname) }}</span>
                        </div>
                        @if($billing->biller->isStudent())
                            <div class="list-group-item">
                                Option : <span class="badge">{{ schoolap_text($billing->biller->parcourt->option->name) }}</span>
                            </div>
                            <div class="list-group-item">
                                Classe : <span class="badge">{{  $billing->biller->parcourt->classe}}<sup>è</sup>{{ $billing->biller->parcourt->salle }}</span>
                            </div>
                        @else
                            <div class="list-group-item">
                                Category : <span class="badge">{{ $billing->biller->category }}</span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
        <div class="panel-body">
            @yield('content')
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-6">
                    Signature : {{ schoolap_text($billing->user->personnel->fullname ?? $billing->user->username ?? 'admin') }}
                </div>
                <div class="col-sm-6">
                    Date du paiement : {{ $billing->created_at->format('d/m/Y à h:i') }}
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>