<!doctype html>
<html lang="en">
  <head>
    <title>Free Education Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet"> -->

    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <!-- <link rel="stylesheet" href="{{asset('css/custom/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom/owl.carousel.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/custom/fonts/ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom/fonts/fontawesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom/fonts/flaticon/font/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom/magnific-popup.css')}}"> -->

    <link rel="stylesheet" href="{{asset('css/custom/style.css')}}">

    <!-- Theme Style -->
    @yield('other')
    <style type="text/css">
        .btn .btn-primary {background:#060f26cf;}

    </style>


  </head>

  <body style="background:#fff;"> 
     <!-- <header role="banner">
     
      <nav class="navbar navbar-expand-lg navbar-light bg-light" style="height:70px;">
        <div class="container">
          <a class="navbar-brand absolute" href="index.html">Avocat.cd</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse navbar-light" id="navbarsExample05">
            <ul class="navbar-nav mx-auto">
              <li class="nav-item"> -->
                <!-- <a class="nav-link active" href="index.html">Accueil</a> -->
              <!-- </li> -->
              <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="courses.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Courses</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="courses.html">HTML</a>
                  <a class="dropdown-item" href="courses.html">WordPress</a>
                  <a class="dropdown-item" href="courses.html">Laravel</a>
                  <a class="dropdown-item" href="courses.html">JavaScript</a>
                  <a class="dropdown-item" href="courses.html">Python</a>
                </div>

              </li> -->
<!-- 
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories</a>
                <div class="dropdown-menu" aria-labelledby="dropdown05">
                  <a class="dropdown-item" href="#">HTML</a>
                  <a class="dropdown-item" href="#">WordPress</a>
                  <a class="dropdown-item" href="#">Laravel</a>
                  <a class="dropdown-item" href="#">JavaScript</a>
                  <a class="dropdown-item" href="#">Python</a>
                </div>

              </li> -->
            <!--   <li class="nav-item">
                <a class="nav-link" href="blog.html">Blog</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="about.html">About</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="contact.html">Contact</a>
              </li> -->
         <!--    </ul>
            <ul class="navbar-nav absolute-right">
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="courses.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Questions</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="courses.html">HTML</a>
                  <a class="dropdown-item" href="courses.html">WordPress</a>
                  <a class="dropdown-item" href="courses.html">Laravel</a>
                  <a class="dropdown-item" href="courses.html">JavaScript</a>
                  <a class="dropdown-item" href="courses.html">Python</a>
                </div>

              </li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="courses.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Mon compte</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04"> -->
                  <!-- <a class="dropdown-item" href="courses.html">Créer un compte</a> -->
                  <!-- <a class="dropdown-item" href="courses.html">WordPress</a> -->
                  <!-- <a class="dropdown-item" href="courses.html">Se connecter</a> -->
                  <!-- <a class="dropdown-item" href="courses.html">JavaScript</a> -->
                  <!-- <a class="dropdown-item" href="courses.html">Python</a> -->
               <!--  </div>

              </li> -->
              <!-- <li><a href="">Connexion</a></li> -->
              <!-- <li> -->
                <!-- <a href="login.html">Connexion</a>  -->
                <!-- / <a href="register.html">Register</a> -->
              <!-- </li> -->
          <!--   </ul>
            
          </div>
        </div>
      </nav>
    </header> -->
    <!-- END header -->

    <nav class="navbar navbar-default navbar-fixed-top" style="height:70px;background:#fff;box-shadow:0px 0px 20px rgba(0,0,0,0.50);border:0;padding-right:20px;">
      <div class="navbar-header" style="background:#fff;color:#000;">
          <a class="navbar-brand" href="{{url('/')}}" style="color:#000;">Avocat.cd</a>
          <!-- <a href="">Avocat.cd</a> -->
      </div>
      <ul class="nav navbar-nav pull-right" style="padding-right:30px;">
        <!-- <li class=""><a href="#" class="btn btn-primary">Accueil <span class="sr-only">(current)</span></a></li> -->
        <li><a href="{{url('/')}}" class="btn btn-primary" style="background:#fff;">Accueil</a></li>
      <a href="{{url('/login')}}" class="btn btn-primary " style="padding:9px;margin-top:15px;border-radius:0;margin-right:;">Connexion</a>
      </ul>
    </nav>
    <br>
    <br>
    <!-- END section -->

        @yield('content')

        <!-- <H1>Bienvenue sur avocat.cd</H1> -->

        <script src="{{asset('css/custom/js/jquery-3.2.1.min.js')}}"></script>
        <script src="{{asset('css/custom/js/jquery-migrate-3.0.0.js')}}"></script>
        <script src="{{asset('css/custom/js/popper.min.js')}}"></script>
        <script src="{{asset('css/custom/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('css/custom/js/owl.carousel.min.js')}}"></script>
        <script src="{{asset('css/custom/js/jquery.waypoints.min.js')}}"></script>
        <script src="{{asset('css/custom/js/jquery.stellar.min.js')}}"></script>
        <script src="{{asset('css/custom/js/jquery.animateNumber.min.js')}}"></script>
        
        <script src="{{asset('css/custom/js/jquery.magnific-popup.min.js')}}"></script>

        <script src="{{asset('css/custom/js/main.js')}}"></script>

        <script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>
          @if(session('status') and session()->get('status') == 'true')
         
          
          <script type="text/javascript">
              // var btn=document.querySelectorAll('#btn-custom');
              //     btn[0].style.background="#060f26cf";
              //     btn[1].style.background="#060f26cf";
              swal ( "Envoie du dossier" ,  "Le dossier a été envoyé" ,  "success" );
            
          </script>
          {{session()->put(['status'=>''])}}
      @endif
  </body>