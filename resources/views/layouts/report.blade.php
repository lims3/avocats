<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <title>@yield('title')</title>
    <style>
        table th{
            font-size: 12px;
        }
    </style>
</head>
<body>
    <div class="container-fuid">
        <div class="row">
            <div class="col-md-6">
                <h3>Coordination sous provincial {{ schoolap_text($etablissement->subgroup->name ?? null) }}</h3>
            </div>
            <div class="col-md-6">

            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <table class="table">
                    <tr>
                        <th>ECOLE</th>
                        <td>{{ schoolap_text($etablissement->name) }}</td>
                    </tr>
                    <tr>
                        <th>PAROISSE</th>
                        <td>NOM PAROISSE</td>
                    </tr>
                    <tr>
                        <th>ADRESSE EXACTE</th>
                        <td>{{ schoolap_text($etablissement->formatedAddress) }}</td>
                    </tr>
                    <tr>
                        <th>COMMUNE D'IMPLANTATION</th>
                        <td>{{ schoolap_text($etablissement->address['commune'] ?? '-') }}</td>
                    </tr>
                    <tr>
                        <th>N° MATRICULE</th>
                        <td>{{ $etablissement->matricule ?? '-' }}</td>
                    </tr>
                    <tr>
                        <th>N° SECOPE</th>
                        <td>{{ $etablissement->secope }}</td>
                    </tr>
                    @if($etablissement->isSchool())
                    <tr>
                        <th>OPTIONS ORGANISÉES</th>
                        <td>
                            @foreach($etablissement->getOptions() as $option)
                               {{ $option->name. ', '  }}
                            @endforeach
                        </td>
                    </tr>
                    @endif
                </table>
            </div>
            <div class="col-md-6">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th colspan="2">ADMINISTRATIF</th>
                        <th colspan="2">ENSEIGNANT</th>
                        <th colspan="2">OUVRIER</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>HF</td>
                            <td>F</td>
                            <td>HF</td>
                            <td>F</td>
                            <td>HF</td>
                            <td>F</td>
                        </tr>
                        <tr>
                            <td>{{ $etablissement->administratives->count() }}</td>
                            <td>{{ $etablissement->administratives->filter(function($item){
                                return strtolower($item->info->sexe) == 'f';
                            })->count() }}</td>
                            <td>{{ $etablissement->teachers->count() }}</td>
                            <td>{{ $etablissement->teachers->filter(function($item){
                                return strtolower($item->info->sexe) == 'f';
                            })->count() }}</td>
                            <td>{{ $etablissement->workers->count() }}</td>
                            <td>{{ $etablissement->workers->filter(function($item){
                                return strtolower($item->info->sexe) == 'f';
                            })->count() }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        @yield('list')
    </div>
</body>
</html>
