@extends('layouts.stability',['toblack'=>true])



@section('content')

		<!-- <h1>Le roi des documents</h1> -->


	<div class="container" style="margin-top:110px;">
		<!-- <div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<input type="text" class="form-control" name="" style="border-radius:0;border:1px solid #1b2254e3;border-right:0; " placeholder="entrez votre nom">
					<span class="input-group-addon" style="background:#fff;border-radius:0;border:1px solid #1b2254e3;border-left:0;">
						<i class="glyphicon glyphicon-user"></i>
					</span>
				</div>
			</div>
		</div> -->
			<div class="row">
				<div class="col-md-12 text-center">
					<h2 class="text-style" style="font-family:Helvetica;color:#4f565d;">Explorez notre espace documents juridiques</h2>
					<i class="glyphicon glyphicon-pencil" style="color:#1b2254e3;font-size:35px;"></i>
            <hr style="width:20%;">
				</div>
			</div>
			<div class="row">
				  <br>
            <br>
				<div class="col-md-12 text-center">
					@foreach($documents as $document)
					   	<div class="col-sm-6 col-md-3">
				          <div class="thumbnail custom-thumb" style="box-shadow:0px 0px 3px rgba(0,0,0,0.45)!important;border:0;">
					         	
					          		  @if($document->cover)

							          		  <img src="{{asset('documents/cover/'.$document->cover)}}" alt="...">

							          	@else
							          		  <img src="{{asset('img/upload/item-5.jpg')}}" alt="...">
							          	@endif
					       
				            <div class="caption text-left">
				              <h3><a style="color:#4f565d;font-family:Helvetica;" href="">{{$document->title}}</a></h3>
				              <hr>
				            <!--   <p style="color:#1b2254e3;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				              tempor incididunt ut labore et dolore magna aliqua. </p> -->
				              <!-- <hr> -->
				              <p class="text-right">
				                <a href="{{asset('documents/cover/'.$document->cover)}}" class="btn btn-primary " style="background:#1b2254e3;border-radius:0;border:0; ">Voir&nbsp;&nbsp;
				                  <i class="glyphicon glyphicon-eye-open"></i></a>
				              </p>
				            </div>
				          </div>
				        </div>

					@endforeach
		        
				</div>
			</div>
	</div>



@stop



@section('title')



@stop