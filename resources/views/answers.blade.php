@extends('layouts.stability')

@section('custom-style')

  ul {
        

    }

  .custom-ul
  {
    border-left:3px solid #1b2254e3;
   
    list-style-type:none;
    position:relative;
  }

  .custom-li
  {
    
    background:#fff;
    
    margin-bottom:15px;
    padding-top:9px;
    padding-bottom:9px;
    padding-left:9px;
    padding-right:9px;
    box-shadow:0px 0px 4px rgba(0,0,0,0.60);


  }

  .custom-dot
  {
    position:absolute;
    width:15px;
    height:15px;
    background:#1b2254e3;
    border-radius:15px;
    left:-9px;

  }
@stop



@section('content')
		
		<div class="container">
		<div class="row" style="margin-bottom:35px;margin-top:90px;">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
           <div class="custom-text text-center">
              <h2 class="text-center text-style">Réponses</h2>
              <i class="glyphicon glyphicon-pencil" style="color:#1b2254e3;font-size:35px;"></i>
              <hr style="width:60%;">
            </div>

            <div class="row ">
            	<div class="col-md-12 ">
            	   <ul class="custom-ul">
                    @foreach($answers as $answer)

                      <li class="custom-li">
                        <span class="custom-dot"></span>
                        <img src="{{asset('img/avatar.jpg')}}" class="img-responsive img-circle" style="width:40px;height:40px; display:inline;">
                         <strong>
                            {{ 
                              ucfirst($answer->lawyer()->first()->profileOfLawyer()->first()->firstname).' '.$answer->lawyer()->first()->profileOfLawyer()->first()->name

                            }}
                         </strong>
                        <hr>
                        <h4>{{$answer->answer}}</h4>
                      </li> 

                    @endforeach
                     
                 </ul>
            	</div>	
            </div>

            <div class="row text-center">
              {{$answers->links()}}
            </div>


			</div>
			</div>
			</div>
			</div>
			</div>



@stop