<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="{{asset('css/news/css/bootstrap.css')}}" rel="stylesheet" id="bootstrap-css">
	<link href="{{asset('css/news/css/pretty.min.css')}}" rel="stylesheet" id="bootstrap-css">

	<style type="text/css">
		*{color:#fff;font-family:Helvetica;}
		.custom-field
		{
			background:transparent !important;
			color:#fff !important;
			border-radius:0 !important;
			line-height:38px !important;
			height:38px !important;
			border:0 !important;
			border-bottom:1px solid #d6cddb !important;
			border-top:transparent !important;
			/*transition-property:border !important;*/
			transition-duration: 0.9s !important;
		}

		.custom-add
		{
			background:none !important;
			border:0 !important;
			border-bottom:1px solid #fff !important;
			border-radius:0 !important;
		}

		.custom-label
		{
			font-size:16px !important;
		}

		.custom-title
		{
			font-size:19px !important;
		}

		.focus-field
		{
			border-bottom:2px solid #541b51 !important;
			/*transition-property:border !important;*/
			transition-duration: 0.9s !important;
			width:100% !important;
		}

		.select-color
		{
			color:#ccc !important;
		}

		.custom-btn
		{
			background:transparent !important;
			border:2px solid #fff !important;
			border-radius:15px !important;
			transition:0.6s;
		}

		.custom-btn:hover
		{
			background:#541b51 !important;
			transition:0.6s;
		}
	</style>
</head>
<body style="background:url('{{asset('img/couverture 2.png')}}');background-origin:border-box;background-repeat:repeat;background-size:100%;">

	<div class="container" style="padding-top:6px">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<div class="row">
					<div class="col-md-12">
						<h1>Inscrivez-vous</h1>
						<!-- <hr style="width:35%;"> -->
						<!-- <br> -->
						<svg style="margin-top:-4px;color:#1b2254e3;font-size:4px;" viewBox="0 0 54 50" id="hammer" xmlns="http://www.w3.org/2000/svg" width="5%" height="4%"><g fill="currentColor" fill-rule="evenodd"><path d="M51.976 33.23a.999.999 0 0 0-1.398.209l-.595.803-24.108-17.856 3.572-4.822.804.596a.997.997 0 0 0 1.399-.21l2.38-3.213a1 1 0 0 0-.208-1.4L24.179.197a1.002 1.002 0 0 0-1.4.208L20.4 3.618a1 1 0 0 0 .208 1.399l.803.595-10.713 14.465-.804-.595a.999.999 0 0 0-1.399.208l-2.38 3.214a1 1 0 0 0 .208 1.399l9.643 7.143a1 1 0 0 0 1.398-.21l2.38-3.215a1 1 0 0 0-.208-1.398l-.803-.595 3.57-4.821L46.41 39.063l-.595.804a1 1 0 0 0 .208 1.398 4.954 4.954 0 0 0 2.972.98 5.024 5.024 0 0 0 4.02-2.023 5.002 5.002 0 0 0-1.04-6.992zM23.792 2.399l8.036 5.952-1.19 1.607-8.036-5.952 1.19-1.607zm-7.439 26.845L8.317 23.29l1.19-1.607 8.036 5.952-1.19 1.608zm-4.048-7.976L23.018 6.804l4.822 3.57L17.126 24.84l-4.821-3.571zm12.38-3.274l24.108 17.855-1.19 1.608L23.495 19.6l1.19-1.607zm26.726 21.04c-.75 1.013-2.11 1.444-3.29 1.084l2.768-3.74.106-.143.492-.665c.69 1.025.7 2.416-.076 3.464zM17 42h-1.586l-1.707-1.707A.997.997 0 0 0 13 40H5a.997.997 0 0 0-.707.293L2.586 42H1a1 1 0 0 0-1 1v6a1 1 0 0 0 1 1h16a1 1 0 0 0 1-1v-6a1 1 0 0 0-1-1zm-1 6H2v-4h1c.265 0 .52-.105.707-.293L5.414 42h7.172l1.707 1.707A.997.997 0 0 0 15 44h1v4z"></path></g></svg>
					</div>
				</div>
				<div class="row" style="text-align:left;">
					<div class="col-md-12">
					    <div class="row">
					    	<div class="col-md-12">
					    		<h3 class="custom-title"><i style="" class="glyphicon glyphicon-user"></i>&nbsp; Identité</h3>
					    	</div>
					    </div>
						<div class="row">
							<div class="col-md-6 form-group ">
								<div class="input-group">
									<!-- <div class="input-group-addon"> -->
										<input type="text" class="form-control custom-field " placeholder="nom" name="">
										<span class="input-group-addon custom-add"><i class="glyphicon glyphicon-user"></i></span>
									<!-- </div> -->
								</div>
							</div>

							<div class="col-md-6 form-group">
							<div class="input-group">
									<!-- <div class="input-group-addon"> -->
										<input type="text" class="form-control custom-field" placeholder="postnom" name="">
										<span class="input-group-addon custom-add"><i class="glyphicon glyphicon-user"></i></span>
									<!-- </div> -->
								</div>
						</div>
						</div>
					</div>
				</div>
				<div class="row" style="text-align:left;">
					<div class="col-md-12">
					   <!--  <div class="row">
					    	<div class="col-md-12">
					    		<h4>Identité</h4>
					    	</div>
					    </div> -->
						<div class="row">
							<div class="col-md-6 form-group">
							<div class="input-group">
									<!-- <div class="input-group-addon"> -->
										<input type="text" class="form-control custom-field" placeholder="prenom" name="">
										<span class="input-group-addon custom-add"><i class="glyphicon glyphicon-user"></i></span>
									<!-- </div> -->
								</div>
							</div>

							<div class="col-md-6 form-group">
								<label class="custom-label">Sexe</label>
								<br>
							<!-- <div class="input-group">
									
										<input type="text" class="form-control custom-field" placeholder="nom" name="">
										<span class="input-group-addon custom-add"><i class="glyphicon glyphicon-user"></i></span>
								
								</div> -->
							<div class="pretty p-default">
					        	<input type="radio" name="help" checked="checked" />
						        <div class="state p-primary">
						            <label>
						            	<span style="color:#fff;font-family:Helvetica;">
						            		Feminin
			            				</span>
									</label>
			        			</div>
			    			</div>
			    			<div class="pretty p-default">
					        	<input type="radio" name="help" />
						        <div class="state p-primary">
						            <label>
						            	<span style="color:#fff;font-family:Helvetica;">
						            		Mascculin
			            				</span>
									</label>
			        			</div>
			    			</div>
						</div>
						</div>
					</div>
				</div>
				<div class="row" style="text-align:left;">
					<div class="col-md-12">
					    <div class="row">
					    	<div class="col-md-12">
					    		<h3 class="custom-title"><i style="" class="glyphicon glyphicon-map-marker"></i>&nbsp; Localisation</h3>
					    	</div>
					    </div>
						<div class="row">
							<div class="col-md-6 form-group ">
								<div class="input-group">
									<!-- <div class="input-group-addon"> -->
										<input type="text" class="form-control custom-field" placeholder="adresse" name="">
										<span class="input-group-addon custom-add"><i class="glyphicon glyphicon-map-marker"></i></span>
									<!-- </div> -->
								</div>
							</div>

							<div class="col-md-6 form-group">
							<div class="input-group">
									<!-- <div class="input-group-addon"> -->
										<select class="form-control custom-field select-color" style="color: #ccc;background:#ccc;">
											<option>Ville</option>
											<option value="1">Kinshasa</option>
					                        <option value="2">Lubumbashi</option>
					                        <option value="3">Matadi</option>
					                        <option value="3">Mbuji-mayi</option>
										</select>
										<span class="input-group-addon custom-add"><i class="glyphicon glyphicon-map-marker"></i></span>
									<!-- </div> -->
								</div>
						</div>
						</div>
					</div>
				</div>
				<div class="row" style="text-align:left;">
					<div class="col-md-12">
					    <div class="row">
					    	<div class="col-md-12">
					    		<h3 class="custom-title"><i style="" class="glyphicon glyphicon-lock"></i>&nbsp; Protection</h3>
					    	</div>
					    </div>
						<div class="row">
							<div class="col-md-6 form-group ">
								<div class="input-group">
									<!-- <div class="input-group-addon"> -->
										<input type="password" class="form-control custom-field" placeholder="mot de passe" name="">
										<span class="input-group-addon custom-add"><i class="glyphicon glyphicon-lock"></i></span>
									<!-- </div> -->
								</div>
							</div>

							<div class="col-md-6 form-group">
							<div class="input-group">
									<!-- <div class="input-group-addon"> -->
										<input type="password" class="form-control custom-field" placeholder="confirmation " name="">
										<span class="input-group-addon custom-add"><i class="glyphicon glyphicon-lock"></i></span>
									<!-- </div> -->
								</div>
						</div>
						</div>
					</div>
				</div>
				<!--  -->
				<div class="row" style="text-align:left; margin-top:-35px;padding-top:6px;" >
					<div class="col-md-12">
					    <div class="row">
					    	<div class="col-md-12">
					    		<h3 class="custom-title"><i style="" class="glyphicon glyphicon-map-marker"></i>&nbsp; Contact</h3>
					    	</div>
					    </div>
						<div class="row">
							<div class="col-md-6 form-group ">
								<div class="input-group">
									<!-- <div class="input-group-addon"> -->
										<input type="text" class="form-control custom-field" placeholder="email" name="">
										<span class="input-group-addon custom-add"><i class="glyphicon glyphicon-envelope"></i></span>
									<!-- </div> -->
								</div>
							</div>

							<div class="col-md-6 form-group">
							<div class="input-group">
									<!-- <div class="input-group-addon"> -->
									<span class="input-group-addon custom-add">+243</span>
											<input type="text" class="form-control custom-field" placeholder="téléphone" name="">
										<span class="input-group-addon custom-add"><i class="glyphicon glyphicon-phone"></i></span>
									<!-- </div> -->
								</div>
						</div>
						</div>
					</div>
				</div>

				<div class="row" style="text-align:left; margin-top:-35px;padding-top:6px;" >
					<div class="col-md-12">
					    <div class="row">
					    	<div class="col-md-12">
					    		<h3 class="custom-title"><i style="" class="glyphicon glyphicon-briefcase"></i>&nbsp; Domaine de compétence</h3>
					    	</div>
					    </div>
						<div class="row">
							<div class="col-md-6 form-group ">
								<div class="input-group">
									<!-- <div class="input-group-addon"> -->
										<select class="form-control custom-field select-color" style="color: #ccc;background:#ccc;">
											<option>Domaine de compétence</option>
											<option value="1">Kinshasa</option>
					                        <option value="2">Lubumbashi</option>
					                        <option value="3">Matadi</option>
					                        <option value="3">Mbuji-mayi</option>
										</select>
										<span class="input-group-addon custom-add"><i class="glyphicon glyphicon-briefcase"></i></span>
									<!-- </div> -->
								</div>
							</div>

						</div>
					</div>
				</div>
				<!--  -->

					<div class="row" style="text-align:left; margin-top:-35px;padding-top:6px;" >
					<div class="col-md-12">
						<br>
						<!-- <br> -->
					  	<button class="pull-right btn btn-primary custom-btn">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Créer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
					  	<br>
					  	<br>
					  	<br>
					  	<br>
					</div>
				</div>


				<!--  -->


				
				
			</div>
		</div>
	</div>
	<script type="text/javascript" src="{{asset('css/news/js/jquery.min.js')}}"></script>
	<script type="text/javascript">
		
		$('.custom-field').on('focus',function(){
			if(!$(this).hasClass('focus-field'))
			{
				$(this).addClass('focus-field');
				return;
			}
		})
		
		$('.custom-field').on('focusout',function(){
			if($(this).hasClass('focus-field'))
			{
				$(this).removeClass('focus-field');
				return;
			}
		})

	</script>
</body>
</html>