@extends('layouts.stability',['behind'=>'#fff'])




@section('content')


	<div class="container" style="margin-top:120px;">
		<div class="col-md-8 " style="border-radius:0;border:0;box-shadow:0px 0px 5px rgba(0,0,0,0.50)!important;background:#fff;">
			<div class="row">
				<div class="col-md-12">
					<h2 style="color:#4f565d;font-family:Helvetica;">{{$article->title}}</h2>
					<hr>
				</div>
			</div>
		
			<div class="row text-center">
				<div class="col-md-12 text-center">
					
					@if($article->cover)

		          		  <img src="{{asset('img/upload/'.$article->cover)}}" class="" alt="..." style="max-width:550px;max-height:550px;"  >

		          	@endif
				</div>
			</div>
			<br>
			<div class="row">
				<div class="c" style="display:none;">
					<div style="padding-left:60px;padding-right:60px;color:#4f565d;font-size:13px;font-family:Helvetica; text-align:justify;">
							{{$article->content}}

						<hr>
					</div>
				
				</div>
				<div class="col-md-12 content-c" style="padding-left:126px;padding-right:126px;color:#4f565d;font-size:14px;font-family:Helvetica; text-align:justify;">
					
				</div>

			</div>

		
			<br>


				<div class="row">
					<div class="col-md-12 text-center ">
						<div class="btn-group" role="group" aria-label="...">
							  <a type="button"  style="border-radius:0;background:#6f6fd2;color:#fff;" class="btn btn-default ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-facebook">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i></a>
							  <a type="button"  style="border-radius:0;background:#92bede;color:#fff;" class="btn btn-default ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-twitter">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i></a>
							  <a type="button" style="border-radius:0;background:#d83535;color:#fff; " class="btn btn-default ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-google-plus">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i></a>
						</div>
					</div>
				</div>
				<br>	
		</div>


		<div class="col-md-4 hidden-xs hidden-sm" >
			<div class="row">
				<!-- <br> -->
				<!-- <br> -->
				<div class="col-md-12">
					<div class="panel panel-default" style="border-radius:0;border:0;box-shadow:0px 0px 5px rgba(0,0,0,0.50)!important;">
					<!-- 	<div class="panel-heading" style="background:#1b2254e3;color:#fff;border-radius:0;height:40px;line-height:40px;">
							<div class="panel-title">
								<h5>Articles récents</h5>
							</div>
						</div> -->
						<div class="panel-body">
							<h3 style="font-family:Helvetica;color:#4f565d;;">Articles récents</h3>
							<hr style="">
							@foreach($recents as $recent)

								<h6><a href="{{route('article.show',['id'=>$recent->id])}}">{{$recent->title}}</a></h6>

							@endforeach
						</div>
						<!-- <div class="panel-footer">
							iihi uihi iuhiuhhuihui hui
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
@stop



@section('title','tout les  articles juridiques')



