@extends('layouts.stability',['behind'=>''])

@section('content')
	
		 <div class="row back-image" style="margin-top:-15px; background:url('{{asset('img/avocats.jpg')}}');background-origin:border-box;background-repeat:no-repeat;background-size:100% 600px;">
	      <div class="col-md-12" style="height:600px;padding-top:6%;">
	        <form method="post" action="{{route('search')}}"> 
	          {{csrf_field()}}
	          <div class="row " >
	            <div class="col-md-6 col-md-offset-3 text-center"  >
	            	<br>
	            	<br>
	              <h2 style="color:#fff;font-family:Helvetica,Arial;">Trouvez facilement votre avocat  </h2>
	              <!-- #898b94e8 -->
            </div>
          </div>
          <br>
          <br>
          <div class="container">
          			<div class="row">
          				<div class="col-md-6 col-md-offset-3  hidden-lg ">
          					<div class="col-md-12">
          						<div class="input-group">
          							           <span class="input-group-addon custom-addon-select custom-addon"><i class="glyphicon glyphicon-briefcase"></i></span>
  							 <select style="" id="other-sepeciality" class="form-control  
				  			   	custom-select custom-field " name="speciality">
				          		  <option value="">Thematique</option>
				              		@foreach($specialities as $speciality)
				                  <option value="{{$speciality->id}}">{{$speciality->name}}</option>
				             		 @endforeach
				               </select>
          						</div>	
          					</div>
          					<div class="col-md-12 hidden-lg hi">
          						<br>
          						<!-- <br> -->
          						<div class="input-group">
          							      <span class="input-group-addon custom-addon-select custom-addon"><i class="glyphicon glyphicon-map-marker"></i></span> 
          							<select style="" id="other-region" class="form-control custom-select custom-field " name="region">
								 <option style="color:#fff;">Ville</option>
				          
								 <option value="1">Kinshasa</option>
			                     <option value="2">Lubumbashi</option>
			                     <option value="3">Matadi</option>
			                     <option value="3">Mbuji-mayi</option>
		            			 </select>
          						</div>
          					</div>
          					<div class="col-md-12 ">
          						<br>
          								<div class="pretty p-default" style="padding-left:-20px;" >
					<input type="checkbox" name="help"  />
				<div class="state p-primary">
			<label>
			<span style="color:#fff;font-family:Helvetica;">
				Vous n'avez pas des revenus pour consulter un avocat? 
			</span>
			</label>
			</div>
			</div>
          					</div>
          				</div>
          			</div>
          		</div>	
          <div class="row " style="margin-top:9px;">   
          	
          		
            <div class="row ">
            	<div class="col-md-6 col-md-offset-3 " style="background:;height:80px;line-height:80px;padding-top:20px;"  >
              		<div class="col-md-6 col-xs-6" style="padding:10px;">
               		<div class="input-group">
               <!--  <span class="input-group-addon custom-addon-select custom-addon"><i class="glyphicon glyphicon-briefcase"></i></span> -->
          		<!-- <div class="col-md-12 hidden-md hidden-lg hidden-sm hidden-xs" style="">
      			   <select style="max-width:410px; " id="other-sepeciality" class="form-control  
      			   	custom-select custom-field hidden-xs" name="speciality">
              		  <option value="">Thematique</option>
                  		@foreach($specialities as $speciality)
                      <option value="{{$speciality->id}}">{{$speciality->name}}</option>
                 		 @endforeach
	               </select>
          		</div> -->
               		<select id="select-state" class="hidden-xs hidden-md hidden-sm" 
               		placeholder="Thématique..." style="width:270px;height:40px;">
       				    <option>Thématique</option>
					 @foreach($specialities as $speciality)
	                    <option value="{{$speciality->id}}">{{$speciality->name}}</option>
	                 @endforeach
					</select>	
					<input type="hidden" name="speciality" id="autority" value="">
               </div>
              </div>
              	<div class="col-md-6 col-xs-12" style="padding:10px;">
            		 <div class="input-group">
           <!--      <span class="input-group-addon custom-addon-select custom-addon"><i class="glyphicon glyphicon-map-marker"></i></span> -->
	               <!-- <select class="form-control custom-select custom-field" name="region">
	                  <option style="color:#fff;">Ville</option>
	                  	<option value="1">Kinshasa</option>
	           		  	<option>Lubumbashi</option>
	           		  	<option>Matadi</option>
	           		  <option>Mbuji-Mayi</option>
	               </select> -->
	              <!--  <div class="col-md-12 hidden-md hidden-lg hidden-sm hidden-xs" style="">
	          		<select style="max-width:410px;" id="other-region" class="form-control custom-select custom-field hidden-xs" name="region">
		                 <option style="color:#fff;">Ville</option>
	          
					 <option value="1">Kinshasa</option>
                     <option value="2">Lubumbashi</option>
                     <option value="3">Matadi</option>
                     <option value="3">Mbuji-mayi</option>
		             </select>
          				</div> -->
	               <select id="select-state-region" class="hidden-xs hidden-md hidden-sm	" 
	               placeholder="Ville..." style="width:270px;height:40px;">
           			<option>Ville</option>
					<option value="1">Kinshasa</option>
                    <option value="2">Lubumbashi</option>
                    <option value="3">Matadi</option>
                    <option value="3">Mbuji-mayi</option>					
					</select>
					<input type="hidden" name="region" id="region" value="">
             </div>
				</div>
						</div>
							</div>
									<div class="row  ">
								<div class="col-md-6 col-md-offset-3 hidden-sm hidden-xs hidden-md    " >
							<br>
						<div class="pretty p-default" style="padding-left:20px;" >
					<input type="checkbox" name="help"  />
				<div class="state p-primary">
			<label>
			<span style="color:#fff;font-family:Helvetica;">
				Vous n'avez pas des revenus pour consulter un avocat? 
			</span>
			</label>
			</div>
			</div>
			</div>
				</div>
	        		</div>
            <!-- <br class="hidden-lg"> -->
            	<div class="row" style="margin-top:20px;">	
               <div class="col-md-6 col-md-offset-3  text-center"  >
                <button class="btn btn-research btn-primary" 
                	style="background:#1b2254e3;border-radius:0;border:0;padding-right:40px;padding-left:40px;height:40px; ">Rechercher</button>
            	</div>
            	</div>
            	</form>
          	</div>
      		</div>

      <!--  Autre bare de navigation -->

    
      <!-- Autre barre de navigation -->
    <!-- </div> -->
        <div class="row" style="margin-bottom:35px;margin-top:25px;">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
					<div class="custom-text">
					<br class="hidden-lg hidden-md">
					<br class="hidden-lg hidden-md">
				<h2 class="text-center text-style" style="font-family:Helvetica;">Avocats récemment inscrits</h2>
            	<div class="text-center">
                  <svg style="margin-top:-10px;color:#1b2254e3;" viewBox="0 0 54 50" id="hammer" xmlns="http://www.w3.org/2000/svg" width="10%" height="10%"><g fill="currentColor" fill-rule="evenodd"><path d="M51.976 33.23a.999.999 0 0 0-1.398.209l-.595.803-24.108-17.856 3.572-4.822.804.596a.997.997 0 0 0 1.399-.21l2.38-3.213a1 1 0 0 0-.208-1.4L24.179.197a1.002 1.002 0 0 0-1.4.208L20.4 3.618a1 1 0 0 0 .208 1.399l.803.595-10.713 14.465-.804-.595a.999.999 0 0 0-1.399.208l-2.38 3.214a1 1 0 0 0 .208 1.399l9.643 7.143a1 1 0 0 0 1.398-.21l2.38-3.215a1 1 0 0 0-.208-1.398l-.803-.595 3.57-4.821L46.41 39.063l-.595.804a1 1 0 0 0 .208 1.398 4.954 4.954 0 0 0 2.972.98 5.024 5.024 0 0 0 4.02-2.023 5.002 5.002 0 0 0-1.04-6.992zM23.792 2.399l8.036 5.952-1.19 1.607-8.036-5.952 1.19-1.607zm-7.439 26.845L8.317 23.29l1.19-1.607 8.036 5.952-1.19 1.608zm-4.048-7.976L23.018 6.804l4.822 3.57L17.126 24.84l-4.821-3.571zm12.38-3.274l24.108 17.855-1.19 1.608L23.495 19.6l1.19-1.607zm26.726 21.04c-.75 1.013-2.11 1.444-3.29 1.084l2.768-3.74.106-.143.492-.665c.69 1.025.7 2.416-.076 3.464zM17 42h-1.586l-1.707-1.707A.997.997 0 0 0 13 40H5a.997.997 0 0 0-.707.293L2.586 42H1a1 1 0 0 0-1 1v6a1 1 0 0 0 1 1h16a1 1 0 0 0 1-1v-6a1 1 0 0 0-1-1zm-1 6H2v-4h1c.265 0 .52-.105.707-.293L5.414 42h7.172l1.707 1.707A.997.997 0 0 0 15 44h1v4z"></path></g></svg>
           </div>
        <hr style="width:20%;">
    <br>
    </div>
    </div>
     </div>
<!-- hidden-sm hidden-xs -->
    <div class="wrapper " style="padding-left:15px;padding-right:15px;">
    <div class="row" >
    <div class="col-md-6 col-md-offset-3  col-sm-12 col-xs-12">
                <!-- <div class="custom-box"> -->
	  <div class="row">
	   @foreach($lawyers as $key => $lawyer)

	  	  	@if($key <= 1)
		<div class="col-md-6 col-sm-6 col-xs-12   " >
	    	<div class="box-style text-center overlay" >
		                   
                @if($lawyer->profile->picture()->orderBy('id','desc')->first())

                	 <img src="{{asset('img/upload/'.$lawyer->profile->picture()->orderBy('id','desc')->first()->Name)}}" class="img-circle " style="" >
                
                @else

                  @switch((int) $lawyer->profileOfLawyer()->first()->sexe)
						    @case(1)
						     		 <img src="{{asset('img/upload/avatar.jpg')}}" class="img-circle">
						        @break

						    @case(2)
						          <img src="{{asset('img/upload/f-default-profile.png')}}" class="img-circle">
						        @break
						   

						@endswitch	

                    @endif
                    <br>
                    <!-- <br> -->
                    <h3 style="color:#1b2254e3;">{{ucfirst($lawyer->profileOfLawyer()->first()->firstname.' '.$lawyer->profileOfLawyer()->first()->name)}}</h3>
                    <i style="color:#1b2254e3;font-size:23px;" class="glyphicon glyphicon-map-marker"></i>
                    <p><strong style="font-size:18px;color:#676464;" >
                    	@switch($lawyer->profileOfLawyer()->first()->province)
						    @case(1)
						        {{'Kinshasa'}}
						        @break

						    @case(2)
						         {{'Lubumbashi'}}
						        @break
						    @case(3)
						         {{'Matadi'}}
						        @break 
						    @case(4)
						         {{'Mbuji-Mayi'}}
						        @break

						@endswitch	
                    </strong></p>
		                    <hr style="width:60%;">
		                    <p style="color:#1b2254e3;font-family:Helvetica; ">
		                      {{$lawyer->profile()->first()->about?$lawyer->profile()->first()->about:'Aucune mention'}}
		                     </p>
		                     <hr style="width:60%;">
		                     		                       <p>
		                     	<a style="background:#1b2254e3;border-radius:15px;border:0;line-height:30px;" href="#data" data-target="{{'#'.$lawyer->id}}" class="btn btn-primary" data-toggle="modal">Voir le profil</a>
		                     </p>
		                     <div class="modal fade"   tabindex="-1" id="{{$lawyer->id}}" role="dialog">
							  <div class="modal-dialog"  role="document" >
							    <div class="modal-content" style="border-radius:0;">
							      <div class="modal-header" style="border-top:3px solid #1b2254e3;  ">
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        <h4 class="modal-title">Profile de l'Avocat</h4>
							      </div>
							      <div class="modal-body">
							        <div class="row">
							        	<div class="col-md-5" style="background:#1b2254e3;">
							        		<br>
							        		   @if($lawyer->profile->picture()->orderBy('id','desc')->first())

						                    	 <img src="{{asset('img/upload/'.$lawyer->profile->picture()->orderBy('id','desc')->first()->Name)}}" class="img-circle " style="" >
						                    
						                    @else

						                  @switch((int) $lawyer->profileOfLawyer()->first()->sexe)
												    @case(1)
												     		 <img src="{{asset('img/upload/avatar.jpg')}}" class="img-circle">
												        @break

												    @case(2)
												          <img src="{{asset('img/upload/f-default-profile.png')}}" class="img-circle">
												        @break
												   

												@endswitch	

						                    @endif
							        		 <br>
							        		 <br>
							        		<p>
						        				 <a class="btn btn-default" style="border-radius:0;background:#6f6fd2;color:#fff;" href=""><i style="color:#fff;" class="fa fa-facebook"></i></a>
								        		 <a class="btn btn-default" style="border-radius:0;background:#92bede;color:#fff;" href=""><i style="color:#fff;" class="fa fa-twitter"></i></a>
								        		 <a class="btn btn-default" style="border-radius:0;background:#d83535;color:#fff;" href=""><i style="color:#fff;" class="fa fa-google-plus"></i></a>
							        		</p>
							        		<br>
							        		<br>
							        		<br>
							        		<br>
							        		<br>
							        		<br>
							        		
							        	

							        	</div>
							        	<div class="col-md-7 text-left">
							        		<div class="row">
							        			<div class="col-md-12 text-left" >
							        				<h4 style="color:#9e9c9c;line-height:15px;padding-bottom:6px;padding-top:6px; "><i style="color:#9e9c9c;" class="glyphicon glyphicon-user"></i>&nbsp;Identité</h4>
							        				<hr style="">
							        				<h4 style="color:#4f565d;">{{ucfirst($lawyer->profileOfLawyer()->first()->firstname.' '.$lawyer->profileOfLawyer()->first()->name)}}</h4>
							        			</div>
							        		</div>
							        		<div class="row">
							        			<div class="col-md-12 text-left" >
							        				<h4 style="color:#9e9c9c;line-height:15px;padding-bottom:6px;padding-top:6px; "><i style="color:#9e9c9c;" class="glyphicon glyphicon-user"></i>&nbsp;A propos</h4>
							        				<hr style="">
							        				<p style="color:#1b2254e3;font-family:Helvetica; ">
								                      {{$lawyer->profile()->first()->about?$lawyer->profile()->first()->about:'Aucune mention'}}
								                     </p>
							        			</div>
							        		</div>
							        		<div class="row">
							        			<div class="col-md-12 text-left" >
							        				<h4 style="color:#9e9c9c;line-height:15px;padding-bottom:6px;padding-top:6px; "><i style="color:#9e9c9c;" class="glyphicon glyphicon-briefcase"></i>&nbsp;Thematique</h4>
							        				<hr style="">
							        				<h4 style="color:#4f565d;">{{ucfirst($lawyer->autority()->first()->name)}}</h4>
							        			</div>
							        		</div>
							        		
							        	</div>
							        </div>
							      </div>
							      <div class="modal-footer">
							        <button style="border-radius:0;background:#1b2254e3;color:#fff;border:0;  " type="button" class="btn btn-default" data-dismiss="modal">Fermer</button >
							        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
							      </div>
							    </div><!-- /.modal-content -->
							  </div><!-- /.modal-dialog -->
							</div><!-- /.modal -->
		                    </div>
		                        <br class="hidden-lg hidden-md hidden-sm">
		                		<br class="hidden-lg hidden-md hidden-sm">
		                </div>
		         

              	  	@endif

              	  @endforeach
              
              </div>
               <br class="">
               <br class="hidden-xs hidden-sm ">
           <!-- </div> -->
           	<div class="row">
               @foreach($lawyers as $key => $lawyer)
               		
              	  	@if($key > 1 and $key <= 3)

              	  		 <div class="col-md-6  col-sm-6 col-xs-12 " >
		                    <div class="box-style text-center overlay" >
		                       
		                    @if($lawyer->profile->picture()->orderBy('id','desc')->first())

		                    	 <img src="{{asset('img/upload/'.$lawyer->profile->picture()->orderBy('id','desc')->first()->Name)}}" class="img-circle"  >
		                    
		                    @else

		                    	@switch((int)$lawyer->profileOfLawyer()->first()->sexe)
								    @case(1)
								     		 <img src="{{asset('img/upload/avatar.jpg')}}" class="img-circle">
								        @break

								    @case(2)
								          <img src="{{asset('img/upload/f-default-profile.png')}}" class="img-circle">
								        @break
								   

								@endswitch	

		                    @endif
		                    <br>
		                    <!-- <br> -->
		                    <h3 style="color:#1b2254e3;">{{ucfirst($lawyer->profileOfLawyer()->first()->firstname.' '.$lawyer->profileOfLawyer()->first()->name)}}</h3>
		                    <i style="color:#1b2254e3;font-size:23px;" class="glyphicon glyphicon-map-marker"></i>
		                    <p><strong style="font-size:18px;color:#676464;" >
		                    	@switch($lawyer->profileOfLawyer()->first()->province)
								    @case(1)
								        {{'Kinshasa'}}
								        @break

								    @case(2)
								         {{'Lubumbashi'}}
								        @break
								    @case(3)
								         {{'Matadi'}}
								        @break 
								    @case(4)
								         {{'Mbuji-Mayi'}}
								        @break

								@endswitch	
		                    </strong></p>
		                    <hr style="width:60%;">
		                    <p style="color:#1b2254e3;font-family:Helvetica; ">
		                      {{$lawyer->profile()->first()->about?$lawyer->profile()->first()->about:'Aucune mention'}}
		                     </p>
		                     <hr style="width:60%;">
		                       <p>
		                     	<a style="background:#1b2254e3;border-radius:15px;border:0;line-height:30px;" href="#data" data-target="{{'#'.$lawyer->id}}" class="btn btn-primary" data-toggle="modal">Voir le profil</a>
		                     </p>
		                     <div class="modal fade"   tabindex="-1" id="{{$lawyer->id}}" role="dialog">
							  <div class="modal-dialog"  role="document" >
							    <div class="modal-content" style="border-radius:0;">
							      <div class="modal-header" style="border-top:3px solid #1b2254e3;  ">
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        <h4 class="modal-title">Profile de l'Avocat</h4>
							      </div>
							      <div class="modal-body">
							        <div class="row">
							        	<div class="col-md-5" style="background:#1b2254e3;">
							        		<br>
							        		  @if($lawyer->profile->picture()->orderBy('id','desc')->first())

						                    	 <img src="{{asset('img/upload/'.$lawyer->profile->picture()->orderBy('id','desc')->first()->Name)}}" class="img-circle " style="" >
						                    
						                    @else
						                    

						                  @switch((int) $lawyer->profileOfLawyer()->first()->sexe)
												    @case(1)
												     		 <img src="{{asset('img/upload/avatar.jpg')}}" class="img-circle">
												        @break

												    @case(2)
												          <img src="{{asset('img/upload/f-default-profile.png')}}" class="img-circle">
												        @break
												   

												@endswitch	

						                    @endif
							        		 <br>
							        		 <br>
							        		<p>
						        				 <a class="btn btn-default" style="border-radius:0;background:#6f6fd2;color:#fff;" href=""><i style="color:#fff;" class="fa fa-facebook"></i></a>
								        		 <a class="btn btn-default" style="border-radius:0;background:#92bede;color:#fff;" href=""><i style="color:#fff;" class="fa fa-twitter"></i></a>
								        		 <a class="btn btn-default" style="border-radius:0;background:#d83535;color:#fff;" href=""><i style="color:#fff;" class="fa fa-google-plus"></i></a>
							        		</p>
							        		<br>
							        		<br>
							        		<br>
							        		<br>
							        		<br>
							        		<br>
							        		
							        	

							        	</div>
							        	<div class="col-md-7 text-left">
							        		<div class="row">
							        			<div class="col-md-12 text-left" >
							        				<h4 style="color:#9e9c9c;line-height:15px;padding-bottom:6px;padding-top:6px; "><i style="color:#9e9c9c;" class="glyphicon glyphicon-user"></i>&nbsp;Identité</h4>
							        				<hr style="">
							        				<h4 style="color:#4f565d;">{{ucfirst($lawyer->profileOfLawyer()->first()->firstname.' '.$lawyer->profileOfLawyer()->first()->name)}}</h4>
							        			</div>
							        		</div>
							        		<div class="row">
							        			<div class="col-md-12 text-left" >
							        				<h4 style="color:#9e9c9c;line-height:15px;padding-bottom:6px;padding-top:6px; "><i style="color:#9e9c9c;" class="glyphicon glyphicon-user"></i>&nbsp;A propos</h4>
							        				<hr style="">
							        				<p style="color:#1b2254e3;font-family:Helvetica; ">
								                      {{$lawyer->profile()->first()->about?$lawyer->profile()->first()->about:'Aucune mention'}}
								                     </p>
							        			</div>
							        		</div>
							        		<div class="row">
							        			<div class="col-md-12 text-left" >
							        				<h4 style="color:#9e9c9c;line-height:15px;padding-bottom:6px;padding-top:6px; "><i style="color:#9e9c9c;" class="glyphicon glyphicon-briefcase"></i>&nbsp;Thematique</h4>
							        				<hr style="">
							        				<h4 style="color:#4f565d;">{{ucfirst($lawyer->autority()->first()->name)}}</h4>
							        			</div>
							        		</div>
							        		
							        	</div>
							        </div>
							      </div>
							      <div class="modal-footer">
							        <button style="border-radius:0;background:#1b2254e3;color:#fff;border:0;  " type="button" class="btn btn-default" data-dismiss="modal">Fermer</button >
							        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
							      </div>
							    </div><!-- /.modal-content -->
							  </div><!-- /.modal-dialog -->
							</div><!-- /.modal -->
		                    </div>
		                    <br class="hidden-lg hidden-md hidden-sm">
		                	<br class="hidden-lg hidden-md hidden-sm">
		                </div>

              	  	@endif

              	  @endforeach
              
              </div>
          
            </div>
        </div>
         
         </div>




      </div>
    </div>
    <div class="row" style="background:#fff;height:auto;" id="blog">
      <div class="col-md-6 col-md-offset-3 " style="height:auto;padding-top:35px;" >
        <div class="custom-text text-center">
            <h2 class="text-center text-style">Articles récemment publiés</h2>
            <i class="glyphicon glyphicon-pencil" style="color:#1b2254e3;font-size:35px;"></i>
            <hr style="width:20%;">
            <br>
        </div>
       
        
        <div class="wrapper" style="padding-left:15px;padding-right:15px;">
        	<div class="row">

        @foreach($articles as $key => $article)

          <!-- {{$article->title}} -->

          <!-- <div class="row"> -->
         @if($key <= 1 )
        <div class="col-sm-6 col-md-6">
          <div class="thumbnail custom-thumb" style="box-shadow:0px 0px 3px rgba(0,0,0,0.45)!important;border:0;">
          	@if($article->cover)

          		  <img src="{{asset('img/upload/'.$article->cover)}}" alt="...">

          	@else
          		  <img src="{{asset('img/upload/item-5.jpg')}}" alt="...">
          	@endif
            <div class="caption">
              <h3><a style="color:#4f565d;font-family:Helvetica;" href="{{route('article.show',['id'=>$article->id])}}">{{str_limit($article->title,50)}}</a></h3>
              <hr>
            <!--   <p style="color:#1b2254e3;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. </p> -->
              <!-- <hr> -->
              <p class="text-right">
                <a href="{{route('article.show',['id'=>$article->id])}}" class="btn btn-primary " style="background:#1b2254e3;border-radius:0;border:0; ">Lire la suite 
                  <i class="glyphicon glyphicon-chevron-right"></i></a>
              </p>
            </div>
          </div>
        </div>
        <!-- </div>   -->
        @endif

        @endforeach
        </div>
        <!-- <br> -->
        <div class="row">
        	 @foreach($articles as $key => $article)

		          <!-- {{$article->title}} -->

		          <!-- <div class="row"> -->
		         @if($key > 1 and $key <= 3 )
		        <div class="col-sm-6 col-md-6">
		          <div class="thumbnail custom-thumb">
		            @if($article->cover)

		          		  <img src="{{asset('img/upload/'.$article->cover)}}" alt="...">

		          	@else
		          		  <img src="{{asset('img/upload/item-5.jpg')}}" alt="...">
		          	@endif
		            <div class="caption">
		              <h3><a style="color:#4f565d;font-family:Helvetica;" href="{{route('article.show',['id'=>$article->id])}}">{{str_limit($article->title,50)}}</a></h3>
		              <hr>
		            <!--   <p style="color:#1b2254e3;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		              tempor incididunt ut labore et dolore magna aliqua. </p> -->
		              <!-- <hr> -->
		              <p class="text-right">
		                <a href="{{route('article.show',['id'=>$article->id])}}" class="btn btn-primary " style="background:#1b2254e3;border-radius:0;border:0; ">Lire la suite 
		                  <i class="glyphicon glyphicon-chevron-right"></i></a>
		              </p>
		            </div>
		          </div>
		        </div>
		        <!-- </div>   -->
        @endif

        @endforeach
        </div>
        </div>


        <div class="row">
          <div class="col-md-12">
            <nav aria-label="...">
              <ul class="pager">
                <li><a href="#" class="pager-left"><i class="glyphicon glyphicon-chevron-left"></i></a></li>
                <li><a href="#" class="pager-right"><i class="glyphicon glyphicon-chevron-right"></i></a></li>

              </ul>
            </nav>
              {{$articles->links()}}
          </div>  
        </div>
      </div>

      

    </div>
    <div class="row" style="background:#1b2254e3;">
      <div class="col-md-6 col-xs-12 text-center " style="background:;padding:30px;color:#fff;" >
        <h2 style="margin-top:70px;font-family:Helvetica;">"Etes-vous avocat?"</h2>
        <p><strong style="font-size:13px;font-family:Helvetica;">Ici!</strong>
            <br>
            <i style="color:#fff;font-size:50px;" class="glyphicon glyphicon-chevron-down"></i>
        </p>
        <a href="{{url('/register')}}" class="btn btn-primary register-btn" >Inscrivez-vous!</a>
        <p style="font-family:Helvetica;">
        	<br>
        	Payez votre inscription en ligne par Orange Money,M-PSA, Airtel Money, Mastercard ou contacter nous directement aux numéros de téléphone pour un paiement sur place, <a href="#contact">Ici!</a>
        </p>
      </div>
      <div class="col-md-6 col-xs-12  " style="background:#;" >
        <img src="{{asset('img/item-6.jpg')}}" style="width:100%;height:400px;">
      </div>
    </div>
    <div class="row" style="height:200px;padding-top:15px;" >
      <div class="col-md-6 col-md-offset-3 text-center" style="color:#1b2254e3; ">
          <h2 style="color:#4f565d;">Trouvez un avocat en RDC est devenu simple grâce au numérique</h2>
          <hr>
          <strong>  Accedez au droit en un clic</strong>
      </div>
    </div>
    

    <div class="wrapper row" style="padding-left:15px;padding-right:15px;">
    	<div class="row" style="padding-top:45px;background:#fff;padding-bottom:18px;padding-left:15px;padding-right:15px;" >
      <div class="col-md-8 col-md-offset-2 text-center" style="color:#1b2254e3;padding-bottom:15px; ">
        <div class="row">
          <div class="col-md-4">
            <div class="box-content text-center" >
              <i class="glyphicon glyphicon-map-marker" style="font-size:24px;"></i>
              <br>
              <h4>Venez nous voir</h4>
              <hr>
              <p style="color:#828384;">2 avenue OUA, Concession Procoki 
                  commune de Ngaliema, 
                  République Démocratique du Congo
              </p>
              <br class="hidden-lg hidden-md hidden-sm">
		      <br class="hidden-lg hidden-md hidden-sm">
            </div>
          </div>
          <br>
          <div class="col-md-4">
            <div class="box-content text-center" >
              <i class="glyphicon glyphicon-phone-alt" style="font-size:24px;"></i>
              <br>
              <h4 id="contact">Contactez-nous</h4>
              <hr>
              <p style="color:#828384;">Tel: 00 243 811 615 180
                <br>
                 Tel: 00 243 811 615 180
                 <br>
                 Email: info@avocats.cd
              </p>
              <br>
              <br>
            </div>
          </div>
          <br class="hidden-lg hidden-md hidden-sm">
		  <br class="hidden-lg hidden-md ">
          <div class="col-md-4">
            <div class="box-content text-center" >
              <i class="glyphicon glyphicon-dashboard" style="font-size:24px;"></i>
              <br>
              <h4>Heures d'ouvertures</h4>
              <hr>
              <p style="color:#828384;">
              
                Lundi - Vendredi: 9 heures - 17 heures 
                <br>
                Vendredi - Samedi: 10 heures - 15 heures
                <br>
                Dimanche: Fermé:)
              </p>
            </div>
          </div>
          <br>
        </div>
      </div>

    </div>
    </div>
    <!-- <br> -->
    <!-- <br> -->
  
     <div class="row wrapper" style="padding-left:15px;padding-right:15px;" id="blog" style="background:#fff;">
      <div class="col-md-6 col-md-offset-3 " style="height:auto;padding-top:35px;" >
        <div class="custom-text text-center">
            <h2 class="text-center text-style" style="font-family:Helvetica;">Nos Partenaires</h2>
            <i class="glyphicon glyphicon-briefcase" style="color:#1b2254e3;font-size:27px;"></i>
            <hr style="width:20%;">
            <!-- <br> -->
            <div class="row">
            	<div class="col-md-12">
            		<div class="row">
            			<div class="col-md-4" >
            			<img src="{{asset('img/LOGI-IKODI.png')}}" style="width:160px;height:190px;"/>
            		</div>
            		<div class="col-md-4">
            			<img src="{{asset('img/logo-ingeniouscity.png')}}" style="width:200px;height:190px;"/>
            		</div>
            		<br class="hidden-lg"> 
            		<br class="hidden-lg"> 
            		<div class="col-md-4" >
            			<img src="{{asset('img/logo-sliconbantu.png')}}" style="width:160px;height:190px;"/>
            		</div>
            		</div>
            		<br>
            		
            	</div>
            </div>
            <br>
            <br>
        </div>
        </div>
        </div>


    <div class="row" style="background:#343A40;color:#fff;">
      <div class="col-md-8 col-md-offset-2 " >
      	<br class="hidden-xs hidden-sm">
          <div class="row">
          	 <div class="col-md-3 hidden-xs hidden-sm">
            <h4 
"><span >A propos</span></h4>
             <!-- <hr style="color:#fff;"> -->
            <p style="color:#c3c0c0;">
            Charte des avocats
            	<br>
            	CGU
     

 </p>
          </div>
            <div class="col-md-5 hidden-xs hidden-sm 	">
            <h4 ><span >Mentions légales</span></h4>
            <!-- <hr style="color:#fff;"> -->
            <p style="color:#c3c0c0;">
            Le présent site internet est conçu et édité par la société Avocats.cd. SAS, 
capital: 2.000 USSD, 
immatriculation:CD/KNG/RCCM/18-B-01316 AT
Directeur de la publication: Junior Luyindula. 
Le site internet est hébergé par la société Amazon Web Services Inc (Ireland)
            	
             </p>
          </div>
         
          <div class="col-md-4 hidden-xs hidden-sm">
            <h4 style=" "><span>Contactez nous</span></h4>
             <!-- <hr style="color:#fff;"> -->
            <input type="text" class="form-control custom-field-add" style="background:#1b2254e3;border-radius:20px;" name="" placeholder="email">
            <br>
            <textarea class="form-control custom-field-add" placeholder="message..." style="border-radius:10px;padding-left:0px;"  >
             
            </textarea>
            <br>
            <button class="btn btn-primary pull-right" style="background:#1b2254e3;border-radius:3px;border:0;padding:10px;margin-top:4px;">Envoyer</button>
          </div>
      </div>
      </div>
      </div>
      <div class="row" style="background:#2a2e31;padding-top:15px;color:#fff;">
      	<!-- <br class="hidden-lg"> -->
      	<!-- <br class="hidden-lg"> -->
      	<!-- <br class="hidden-lg"> -->
        <div class="col-md-6 col-md-offset-3 text-center">
          <!-- <hr> -->
            <p>Copyright © 2018 Avocats.cd - Tous droits réservés</p>
            <p>Powered by ohelene</p>

        </div>
        <!-- <br class="hidden-lg"> -->
      	<br class="hidden-lg hidden-sm">
      	<br class="hidden-lg hidden-sm">
      </div>
    <!--       </div>
    </div> -->

    <span class="remote-box" style="display:none;">
    	<!-- <span class="remote-btn"> -->
    		<i class="glyphicon glyphicon-chevron-up"></i>
    	<!-- </span> -->
    </span>



     <script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>
    
     @if(session()->get('search') )

    	<script type="text/javascript">
    		
    		    swal ( "Recherche" ,  "Aucun avocat trouvé" ,  "error" );

    	</script>
   
    	{{session()->put('search','')}}

      @endif


	 <script type="text/javascript">
          $('.pagination').css({display:'none'});

          $('.pager li a').each(function(){
                     $(this).on('click',function(e){
                        e.preventDefault();
                        if($(this).attr('class') == 'pager-left')
                        {
                          if($('.pagination li').eq(0).attr('class')
                              ){
                              return;
                          }else{
                            window.location=$('.pagination li a').attr('href');
                          }

                        }else
                        {
                          if($('.pagination li').eq(1).attr('class')
                              ){
                              return;
                          }else{
                            window.location=$('.pagination li:eq(1) a').attr('href');
                          }
                        }


                     })
              })


          		$select_state = $('#select-state').selectize({
					onChange: function(value) {
						// if (!value.length) return;
						$('#autority').val(value);
						// alert(value);
						return;
						
					}
				});

				 $('#select-state-region').selectize({
					onChange: function(value) {
						$('#region').val(value);	
					}
				}); 

				 $('#other-sepeciality').on('change',function(){
					// onChange: function(value) {
						// alert($(this).val());
						$('#autority').val($(this).val());	
					// }
				});

				 $('#other-region').on('change',function(){
					// onChange: function(value) {
						$('#region').val($(this).val());	
					// }
				});




				 $('#select-state-region').css({display:'none'});
    
     
      </script>


     


@stop


@section('papers')	

			
		 


@stop

