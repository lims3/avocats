@extends('layouts.admin')



@section('content')
	
	 <div class="row">
    @include('inc.message')
    
  </div>
  <div class="row">
  	@if($errors->any())
  		<ul>
  			@foreach($errors->all() as $error)

  				<li>{{$error}}</li>

  			@endforeach
  		</ul>
  	@endif
  </div>	
	<div class="row">
		<div class="title_left">
	        <h3>&nbsp;&nbsp;Mettre en ligne un document </h3>
	    </div>
	</div>

	<div class="row">
              
              <div class="col-md-6 col-xs-12" >
                

                

                <div class="x_panel" style="border-top:3px solid #1b2254e3; ">
                  <div class="x_title">
                    <h2>Votre réponse <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p>
                      	
                    </p>
                    <!-- start form for validation -->
                    <form id="demo-form" action="{{route('document.store')}}" method="post" data-parsley-validate="" novalidate="" enctype="multipart/form-data">
                    	{{csrf_field()}}

                    		<div class="row">
                    			<div class="col-md-12">
                    				<label class="label-control">Titre du doocument</label>
                    				<input type="text" class="form-control" placeholder="ex:contrant de confidentialité" name="title">
                    			</div>
                    		</div>
                    		<div class="row">
                    			<div class="col-md-12">
                    				<label class="label-control">Couverture du document</label>
                    				<input type="file" class="form-control" placeholder="ex:contrant de confidentialité" name="cover">
                    			</div>
                    		</div>
                    		<div class="row">
                    			<div class="col-md-12">
                    				<label class="label-control">Document(pdf,word,autres)</label>
                    				<input type="file" class="form-control" placeholder="ex:contrant de confidentialité" name="document">
                    			</div>
                    		</div>

                          <br>
                          <button type="submit" class="btn btn-primary pull-right" style="background:#1b2254e3;border-radius:0;">Ajouter</button>

                    <p></p></form>
                    <!-- end form for validations -->

                  </div>
                </div>


              </div>


            </div>



@stop





