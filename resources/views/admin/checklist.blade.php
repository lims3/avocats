@extends('layouts.admin',['dossier'=>count($inactived)])


@section('content')
<div class="row">
    <div class="col-md-12">
        @include('inc.message')
    </div>
</div>
	<div class="row">
		
		<div class="col-md-12 col-xs-12 hidden-xs ">
                <div class="x_panel" style="border-top:0;border-top:3px solid #1b2254e3; ">
                   <div class="x_title">
                   
                    <div class="clearfix"></div>
                  </div> 
                  <div class="x_content" style="height:33px; ">
                 <!--    <p style="margin-left:1%;">Donnez du sens à votre article en lui donnant un titre</p> -->
                    <!-- <br /> -->
                   
                         <form class="form-horizontal form-label-left">

                      <div class="form-group">
                        
                        <div class="col-md-2 col-sm-9 col-xs-12" style="border-right:1px solid #ccc;">
                            Dossiers <span class="badge badge-success">{{count($inactived)}}</span>
                        </div> 
                      <!--   <div class="col-md-2 col-sm-9 col-xs-12" style="border-right:1px solid #ccc;">
                            Traités <span class="badge badge-success">1</span>
                        </div>
                        <div class="col-md-2 col-sm-9 col-xs-12" style="border-right:1px solid #ccc;">
                            En attente <span class="badge badge-success">1</span>
                        </div>
 -->
                      <!-- <div class="col-md-6 "> -->
                           <!-- <button class="btn btn-primary">Trier</button> -->
                    <!--        <div class="pull-right">
                             <div class="col-md-6 ">
                             <input type="text" class="form-control" name="" placeholder="Rechercher un article rapidement">
                           </div>
                            <div class="col-md-6">
                             <button class="btn btn-primary" style="border-radius:0;background:#1b2254e3;">Rechercher</button>
                           </div>
                           </div>
                      </div> -->

                      
                      </div>

                    </form>

                    
                  </div>
                </div>

              </div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-6 col-xs-12" style="padding-right:20px;">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tous les comptes à traiter<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table table-bordered table-hover">
                      <thead style="background:#1b2254e3;color:#fff;">
                        <tr>
                          <th>N°</th>
                          <!-- <th>Client</th> -->
                          <th>Noms</th>
                          <!-- <th>Explicatino du dossier</th> -->
                          <!-- <th>Date de création</th> -->
                          <!-- <th>Etat</th> -->
                          <th>Action à appliquer</th>
                         <!--  <th>Date de création</th>
                          <th>Status actuel</th> -->
                          <th>Activation du compte</th>
                        </tr>
                      </thead>
                      <tbody>

                    @foreach($inactived as $user)
                 
                        <tr>
                           <!-- <th scope="row"><input type="checkbox" name=""></th> -->
                           <th scope="row">{{$loop->iteration}}</th>
                          <td>{{ucfirst($user->profileOfLawyer()->first()->firstname.' '.$user->profileOfLawyer()->first()->name)}}</td>
                          <td>
                             <button type="button" class="btn btn-primary" style="background:#1b2254e3;color:#fff;border-radius:0;" data-toggle="modal" data-target=".{{$user->id}}">lire le dossier</button>
                                                <div class="modal fade {{$user->id}}" style="display:none;" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                      <div class="modal-dialog modal-lg " role="document" style="
                                      border-radius:0;">
                                        <div class="modal-content" style="
                                      border-radius:0;" >
                                            <div class="modal-header" style="background:#1b2254e3;color:#fff;height:48px;line-height:48px;">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" 
                                            ><span aria-hidden="true" style="color:#fff;">&times;</span></button>
                                            <h4 class="modal-title" id="gridSystemModalLabel" ">Explication du dossier</h4>
                                          </div>

                                          <div class="modal-body">
                                            <p>Dossier avocat</p>
                                                
                                            <hr>
                                            <div class="row">
                                              <div class="col-md-12 text-center">
                                                @if($user->profile->picture()->orderBy('id','desc')->first())

                                                         <img style="height:80px;" src="{{asset('img/upload/'.$user->profile->picture()->orderBy('id','desc')->first()->Name)}}" class="img-circle " style="" >
                                                      
                                                      @else

                                                    @switch((int) $user->profileOfLawyer()->first()->sexe)
                                                  @case(1)
                                                       <img style="height:20px;" src="{{asset('img/upload/avatar.jpg')}}" class="img-circle">
                                                      @break

                                                  @case(2)
                                                        <img style="height:20px;" src="{{asset('img/upload/f-default-profile.png')}}" class="img-circle">
                                                      @break
                                                 

                                              @endswitch  

                                                      @endif
                                              </div>
                                            </div> 
                                            <!-- <hr> -->
                                            <div class="row">
                                              <div class="col-md-6">
                                                  <h3>Identité</h3>
                                                  <hr style="width:35%;">
                                                  <h2>Nom:&nbsp;{{$user->profileOfLawyer()->first()->name}}</h2>  
                                                  <h3>Contact</h3>
                                                  <hr style="width:35%;">
                                                  <h2>Email:{{$user->email}}
                                                    <br>
                                                    Téléphone:{{$user->profileOfLawyer()->first()->phone}}</h2>
                                              </div>
                                              <div class="col-md-6">
                                                  <h3>Autres informations</h3>
                                                  <hr>
                                                  <h2 style="margin-top:5px;">Numero d'ordre:<br>{{$user->profileOfLawyer()->first()->numero_ordre}}</h2>  
                                                  <h2 style="margin-top:5px;">nom du barreau:<br>{{$user->profileOfLawyer()->first()->barreau}}</h2> 
                                                  <h2 style="margin-top:5px;">Date de prestation:<br>{{$user->profileOfLawyer()->first()->date_prestation}}</h2>  
                                                 <h2 style="margin-top:5px;">Avocat stagiaire ou inscrit au tableau:{{$user->profileOfLawyer()->first()->type_avocat}}</h2>  
                                                <h2 style="margin-top:5px;">Nom du cabinet:<br>{{$user->profileOfLawyer()->first()->nom_cabinet}}</h2>  
                                                
                                                  
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                          </td>
                         
                          <!-- <td>Traiter}</td> -->
                          <td><a  href="{{route('allowed',['id'=>$user->id])}}" class="btn btn-default" style="background:#1b2254e3;color:#fff;">Activer le compte&nbsp;<i class="fa fa-check"></i></a>
                          
                        </td>
                        </tr>



                    @endforeach
                    </table>

                  </div>
                </div>
              </div>
            
  </div>


                       

      
@stop