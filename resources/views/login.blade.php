@extends('layouts.login')



@section('content')

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100" style="height:550px;padding-bottom:30px;border-radius:0;">
				<form class="login100-form validate-form" method="post" action="{{route('login')}}">
					{{csrf_field()}}
					<span class="login100-form-title p-b-26" style="color:#060f26cf;;">
						<img src="{{asset('img/LOGO.jpg')}}"> 
					</span>
					<span class="login100-form-title p-b-48" style="border-top:-80px;">
						<i class="zmdi zmdi-font"></i>
					</span>

					<div class="wrap-input100 validate-input" data-validate = "email non valide">
						<input class="input100" type="text" id="email" name="email">
						<span class="focus-input100" value="{{old('email')}}" data-placeholder="Email"></span>

					</div>
					@if ($errors->has('email'))
                                    <span class="help-block" style="font-size:9px;color:#dc2020cf;">
                                        <strong style="font-style:3px;color:#dc2020cf;">{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
					<div class="wrap-input100 validate-input" data-validate="entrez un mot de passe">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" autocomplete="off" value="{{old('password')}}" id="pass" name="password">
						<span class="focus-input100" data-placeholder="Mot de passe"></span>
					</div>
					@if ($errors->has('password'))
                        <span class="help-block" style="font-size:9px;color:#dc2020cf;">
                            <strong style="font-style:3px;color:#dc2020cf">{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
					<div class="wrap-input100 " style="border-bottom:0;" >
						<div class="pretty p-default">
			        <input type="checkbox" name="help" />
			        <div class="state p-primary">
			            <label>
			            	<span style="color:#828384;font-family:Helvetica;font-size:12px;">
            					Garder ma session ouverte
							</span>
				</label>
			        </div>
			    </div>
					</div>
					<div class="container-login100-form-btn" style="border-radius:0;">
						<div class="wrap-login100-form-btn" style="border-radius:0;background:#060f26cf;">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" style="border-radius:0;background:#060f26cf;border-top:5px #ccc solid;">
								Se connecter
							</button>
						</div>
					</div>
				
					<!-- <div class="text-center p-t-115">
						<span class="txt1">
							Don’t have an account?
						</span>

						<a class="txt2" href="#">
							Sign Up
						</a>
					</div> -->
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>



@stop