@inject('autorities','App\Http\Controllers\NotificationController')

<!DOCTYPE html>
<html class=''>
<head>

<!-- <link rel='stylesheet prefetch' href=''> -->
<title>Avocat.cd | création d'un compte</title>
<meta charset='UTF-8'/>
<link href="{{asset('css/news/css/bootstrap.css')}}" rel="stylesheet" id="bootstrap-css">
<!-- <link href="bootstrap.css" rel="stylesheet" id="bootstrap-css"> -->
<!-- <script src="jquery.js"></script> -->
<!-- <script src="bootstrap.min.js"></script> -->



<!-- <script src="bootstrap.min.js"></script> -->


<style class="cp-pen-styles">
  .field-box{
    /*border-bottom:3px solid #1b2254e3;*/
  }
  .field-text{
    /*background:#9e9c9c;*/
    color:#9e9c9c;
    padding-top:6px;
    padding-left:3px;
    padding-right:3px;
  }

  .custom-btn
  {
    /*background:#1b2254e3 !important;*/
    border-radius:0 !important;
    border-left:0 !important;
    background:#fff !important;
    height:35px !important;


    /*border:0;*/
  }
  .custom-addon
  {
    border-radius:0 !important;
    background:#fff !important;
    border-right:0 !important;
    color:#ccc !important;
  }
  .separator
  {
    height:5px;
    border-top:2px solid red;
    width:400px;
    position:absolute;
    /*width:100px;*/
    /*transform:translateX(50%); */
    /*transform:rotate(20%,20%);*/
   -webkit-transform: rotateX(90);
  }
</style>

</head>

<body style="{{$errors->any()?'':'overflow:scroll;'}};background:#FAFAFA;" >
    <div class="container" style="padding-top:2px;">
      <form method="post" action="{{route('register')}}">  
        {{csrf_field()}}
      <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center ">

          <h3 style="color:#1b2254e3;font-size:33px;margin-bottom:-6px;">Inscrivez-vous!</h3>
          <hr>
                    <svg style="margin-top:-10px;color:#1b2254e3;font-size:4px;" viewBox="0 0 54 50" id="hammer" xmlns="http://www.w3.org/2000/svg" width="10%" height="10%"><g fill="currentColor" fill-rule="evenodd"><path d="M51.976 33.23a.999.999 0 0 0-1.398.209l-.595.803-24.108-17.856 3.572-4.822.804.596a.997.997 0 0 0 1.399-.21l2.38-3.213a1 1 0 0 0-.208-1.4L24.179.197a1.002 1.002 0 0 0-1.4.208L20.4 3.618a1 1 0 0 0 .208 1.399l.803.595-10.713 14.465-.804-.595a.999.999 0 0 0-1.399.208l-2.38 3.214a1 1 0 0 0 .208 1.399l9.643 7.143a1 1 0 0 0 1.398-.21l2.38-3.215a1 1 0 0 0-.208-1.398l-.803-.595 3.57-4.821L46.41 39.063l-.595.804a1 1 0 0 0 .208 1.398 4.954 4.954 0 0 0 2.972.98 5.024 5.024 0 0 0 4.02-2.023 5.002 5.002 0 0 0-1.04-6.992zM23.792 2.399l8.036 5.952-1.19 1.607-8.036-5.952 1.19-1.607zm-7.439 26.845L8.317 23.29l1.19-1.607 8.036 5.952-1.19 1.608zm-4.048-7.976L23.018 6.804l4.822 3.57L17.126 24.84l-4.821-3.571zm12.38-3.274l24.108 17.855-1.19 1.608L23.495 19.6l1.19-1.607zm26.726 21.04c-.75 1.013-2.11 1.444-3.29 1.084l2.768-3.74.106-.143.492-.665c.69 1.025.7 2.416-.076 3.464zM17 42h-1.586l-1.707-1.707A.997.997 0 0 0 13 40H5a.997.997 0 0 0-.707.293L2.586 42H1a1 1 0 0 0-1 1v6a1 1 0 0 0 1 1h16a1 1 0 0 0 1-1v-6a1 1 0 0 0-1-1zm-1 6H2v-4h1c.265 0 .52-.105.707-.293L5.414 42h7.172l1.707 1.707A.997.997 0 0 0 15 44h1v4z"></path></g></svg>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-default" style="border-radius:0;border-top:3px solid #1b2254e3;box-shadow:0px 0px 7px rgba(0,0,0,0.40);">
          <!--   <div class="panel-heading" style=";border-radius:0;color:#1b2254e3;text-align:center;font-size:18px;">
              <span>Inscrivez-vous!</span>
            </div> -->
            <div class="panel-body">
              <div class="col-md-6" style="">
                <h4 class="field-box" ><span class="field-text"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;Indentité</span></h4>
                <hr>
                <br>
               <div class="form-group">
                 <div class="input-group">
                    <span class="input-group-addon custom-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
                    <input type="text" class="form-control custom-btn" placeholder="nom" aria-describedby="basic-addon1" name="name" value="{{old('name')}}">
                  </div>
                     @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong style="font-size:13px;color:#f18888;">{{ $errors->first('name') }}</strong>
                                    </span>
                     @endif
               </div>
                <div class="form-group">
                 <div class="input-group">
                    <span class="input-group-addon custom-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
                    <input type="text" class="form-control custom-btn" placeholder="postnom" aria-describedby="basic-addon1" name="lastname" value="{{old('lastname')}}">
                  </div>
                     @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong style="font-size:13px;color:#f18888;">{{ $errors->first('lastname') }}</strong>
                                    </span>
                     @endif
               </div>
                <div class="form-group">
                 <div class="input-group">
                    <span class="input-group-addon custom-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
                    <input type="text" class="form-control custom-btn" placeholder="prénom" aria-describedby="basic-addon1" name="firstname" value="{{old('firstname')}}">
                  </div>
                     @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        <strong style="font-size:13px;color:#f18888;">{{ $errors->first('name') }}</strong>
                                    </span>
                     @endif
               </div>
                <div class="form-group">
                 <div class="input-group">
                    <span class="input-group-addon custom-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
                    <select class="form-control custom-btn" name="sexe" value="{{old('sexe')}}">
                      <!-- <label>Sexe</label> -->
                      <option>Sexe</option>
                      <option value="1">Masculin</option>
                      <option value="2">Féminin</option>
                    </select>
                  </div>
                     @if ($errors->has('sexe'))
                                    <span class="help-block">
                                        <strong style="font-size:13px;color:#f18888;">{{ $errors->first('sexe') }}</strong>
                                    </span>
                     @endif
               </div>
               <!-- <br> -->
               <h4 class="field-box" ><span class="field-text"><i class="glyphicon glyphicon-lock"></i>&nbsp;&nbsp;Protection</span></h4>
               <hr>
               <div class="form-group">
                 <div class="input-group">
                    <span class="input-group-addon custom-addon " id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" class="form-control custom-btn" placeholder="mot de passe" aria-describedby="basic-addon1" name="password" value="{{old('password')}}">
                  </div>
                     @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong style="font-size:13px;color:#f18888;">{{ $errors->first('password') }}</strong>
                                    </span>
                     @endif
               </div>
               <div class="form-group">
                 <div class="input-group">
                    <span class="input-group-addon custom-addon " id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" class="form-control custom-btn" placeholder="confirmation" aria-describedby="basic-addon1" name="password_confirmation">
                  
                  </div>
               </div>
              </div>
               <div class="col-md-6">
                <h4 class="field-box" ><span class="field-text"><i class="glyphicon glyphicon-map-marker"></i>&nbsp;&nbsp;Localisation</span></h4>
                <hr>
                <br>
                 <div class="form-group">
                 <div class="input-group">
                    <span class="input-group-addon custom-addon " id="basic-addon1"><i class="glyphicon glyphicon-map-marker"></i></span>
                    <input type="text" class="form-control custom-btn" placeholder="adresse physique" aria-describedby="basic-addon1" name="adresse" value="{{old('adresse')}}">
                  </div>
                     @if ($errors->has('adresse'))
                                    <span class="help-block">
                                        <strong style="font-size:13px;color:#f18888;">{{ $errors->first('adresse') }}</strong>
                                    </span>
                    @endif
               </div>
               <div class="form-group">
                 <div class="input-group">
                    <span class="input-group-addon custom-addon " id="basic-addon1"><i class="glyphicon glyphicon-map-marker"></i></span>
                    <!-- <input type="text" class="form-control custom-btn" placeholder="province " aria-describedby="basic-addon1"> -->
                    <select class="form-control custom-btn" name="province">
                        <option>Ville</option>
                        <option value="1">Kinshasa</option>
                        <option value="2">Lubumbashi</option>
                        <option value="3">Matadi</option>
                        <option value="3">Mbuji-mayi</option>
                    </select>
                  </div>
                     @if ($errors->has('province'))
                                    <span class="help-block">
                                        <strong style="font-size:13px;color:#f18888;">{{ $errors->first('province') }}</strong>
                                    </span>
                      @endif
               </div>
               <!-- <br> -->
               <h4 class="field-box" ><span class="field-text"><i class="glyphicon glyphicon-phone"></i>&nbsp;&nbsp;Contact</span></h4>
               <hr>
               <div class="form-group">
                 <div class="input-group">
                    <span class="input-group-addon custom-addon " id="basic-addon1"><i class="glyphicon glyphicon-phone"></i></span>
                    <input type="text" class="form-control custom-btn" placeholder="téléphone ex:243846871678 " aria-describedby="basic-addon1" name="phone" value="{{old('phone')}}">
                  </div>
                     @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong style="font-size:13px;color:#f18888;">{{ $errors->first('phone') }}</strong>
                                    </span>
                     @endif
               </div>

               <div class="form-group">
                 <div class="input-group">
                    <span class="input-group-addon custom-addon " id="basic-addon1"><i class="glyphicon glyphicon-envelope"></i></span>
                    <input type="text" class="form-control custom-btn" placeholder="adresse e-mail " aria-describedby="basic-addon1" name="email" value="{{old('email')}}">
                  </div>
                     @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong style="font-size:13px;color:#f18888;">{{ $errors->first('email') }}</strong>
                                    </span>
                     @endif
                     <br>
               <h4 class="field-box" ><span class="field-text"><i class="glyphicon glyphicon-briefcase"></i>&nbsp;&nbsp;Domaine de compétence </span></h4>
               <hr>
                      <div class="input-group">
                    <span class="input-group-addon custom-addon " id="basic-addon1"><i class="glyphicon glyphicon-briefcase"></i></span>
                    <!-- <input type="text" class="form-control custom-btn" placeholder="province " aria-describedby="basic-addon1"> -->
                    <select class="form-control custom-btn" name="speciality">
                      <option>Domaine de compétence</option>

                      @foreach($autorities->autority() as $autority)
                      
                          <option value="{{$autority->id}}">{{$autority->name}}</option>
                      @endforeach
                    </select>
                  </div>
                     @if ($errors->has('speciality'))
                                    <span class="help-block">
                                        <strong style="font-size:13px;color:#f18888;">{{ $errors->first('speciality') }}</strong>
                                    </span>
                      @endif
               </div>
               <br>

              </div>
            </div>
            <!-- <div class="separator"></div> -->
            <div class="panel-footer text-right" style="">
              <button class="btn btn-primary " style="background:#1b2254e3;border-radius:0; padding-right:40px;padding-left:40px;">Créer</button>
            </div>
          </div>
        </div>
      </div>
      </form>
    </div>
</body>
</html>

