<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                @foreach( $menus as $menu)
                        @if(!$menu->submenus->count() > 0)
                        @if( empty($menu->description['hidden']))
                            <li>
                                <a href="{{ route($menu->route, array_merge(['__p'=>$menu->id ], isset($menu->description['param']) ? $menu->description['param'] : [] )) }}" class="{{ (route($menu->route) == app('url')->full()) ? 'active':'' }}">
                                    <i class="{{ $menu->description['icon'] ?? 'lnr lnr-file-empty' }}"></i>
                                    <span>{{ ucfirst(mb_strtolower($menu->name)) }}</span>
                                </a>
                            </li>
                            @endif
                        @else
                            <li>
                                <a href="#{{ str_slug($menu->name, '-') }}" data-toggle="collapse" class="collapsed">
                                    <i class="{{ $menu->description['icon'] ?? 'lnr lnr-file-empty' }}"></i>
                                    <span>{{ ucfirst(mb_strtolower($menu->name)) }}</span>
                                    <i class="icon-submenu lnr lnr-chevron-right"></i>
                                </a>
                                <div id="{{ str_slug($menu->name, '-') }}" class="collapse">
                                    <ul class="nav fa-dol">
                                        @foreach($menu->submenus as $submenu)
                                            @if(empty($submenu->description['hidden']))
                                                <li>
                                                    <a title="{{ $submenu->description['title'] ?? null }}"
                                                       href="{{route($submenu->route, array_merge(['__p'=>$submenu->id ], (isset($submenu->description['param'])) ? $submenu->description['param']: [])) }}"
                                                       class="">
                                                        <i class="{{ $submenu->description['icon'] ?? 'lnr lnr-file-empty' }}"></i>
                                                        {{ $submenu->name }}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                        {{--  <li><a href="page-lockscreen.html" class="">Lockscreen</a></li>  --}}
                                    </ul>
                                </div>
                            </li>
                        @endif
                @endforeach
                {{--<li><a href="{{ route('home') }}" class="active"><i class="lnr lnr-home"></i> <span>Dashboard</span></a>
                </li>
                --}}{{--  <li><a href="elements.html" class=""><i class="lnr lnr-code"></i> <span>Elements</span></a></li>
                <li><a href="charts.html" class=""><i class="lnr lnr-chart-bars"></i> <span>Charts</span></a></li>
                <li><a href="panels.html" class=""><i class="lnr lnr-cog"></i> <span>Panels</span></a></li>
                <li><a href="notifications.html" class=""><i class="lnr lnr-alarm"></i> <span>Notifications</span></a></li>  --}}{{--
                @if(auth()->user()->isSAdmin())
                    <li>
                        <a href="#etablissement" data-toggle="collapse" class="collapsed"><i
                                    class="glyphicon glyphicon-education"></i> <span>Etablissement</span> <i
                                    class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="etablissement" class="collapse ">
                            <ul class="nav">
                                <li><a href="{{route('schools.create')}}" class="">Création école</a></li>
                                <li><a href="{{route('offices.switch')}}" class="">Création du bureau de gestion</a>
                                </li>
                                --}}{{--  <li><a href="page-lockscreen.html" class="">Lockscreen</a></li>  --}}{{--
                            </ul>
                        </div>
                    </li>

                @endif
                @if(auth()->user()->adminOf)

                    <li>
                        <a href="#personnel" data-toggle="collapse" class="collapsed"><i
                                    class="lnr lnr-file-empty"></i> <span>Personnels</span> <i
                                    class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="personnel" class="collapse ">
                            <ul class="nav">
                                <li><a href="{{route('stakeholders.create', ['stakeholder'=>'personnel'])}}"
                                       class="">Nouveau</a></li>
                                <li><a href="{{route('stakeholders.index', ['type'=>'personnel'])}}"
                                       class="">Liste</a></li>
                                --}}{{--  <li><a href="page-lockscreen.html" class="">Lockscreen</a></li>  --}}{{--
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#config" data-toggle="collapse" class="collapsed"><i class="lnr lnr-cog"></i>
                            <span>Configuration</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="config" class="collapse ">
                            <ul class="nav">
                                <li><a href="{{route('choose', ['conf_type'=> 'option'])}}" class="">Configuration
                                        des options</a></li>
                                --}}{{--  <li><a href="page-lockscreen.html" class="">Lockscreen</a></li>  --}}{{--
                            </ul>
                        </div>
                    </li>
                @endif
                @if(auth()->user()->isSchoolUser())

                    <li>
                        <a href="#student" data-toggle="collapse" class="collapsed"><i class="lnr lnr-user"></i>
                            <span>Eleves</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="student" class="collapse ">
                            <ul class="nav">
                                <li><a href="{{route('stakeholders.create', ['stakeholder'=>'student'])}}" class="">Nouveau</a>
                                </li>
                                <li><a href="{{route('stakeholders.index', ['type'=>'student'])}}"
                                       class="">Liste</a></li>
                                --}}{{--  <li><a href="page-lockscreen.html" class="">Lockscreen</a></li>  --}}{{--
                            </ul>
                        </div>
                    </li>
                @endif
                <li>
                    <a href="{{route('users.create')}}"><i class="lnr lnr-user"></i> Crée un utlisateur</a>
                </li>

                <li>
                    <a onclick="event.preventDefault();
                        document.getElementById('form-logout').submit()" href="{{route('logout')}}"><span
                                class="fa fa-logout"></span> Deconnexion</a>
                </li>
                --}}{{--  <li>
                    <a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Création de l'utlisateur</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="subPages" class="collapse ">
                        <ul class="nav">
                            <li><a href="{{route('schools.create')}}" class="">Création école</a></li>
                            <li><a href="" class="">Création du bureau de gestion</a></li>
                            {{--  <li><a href="page-lockscreen.html" class="">Lockscreen</a></li>  --}}
                {{--  </ul>  --}}
                {{--  </div>  --}}
                {{--  </li>  --}}
                {{--  <li><a href="tables.html" class=""><i class="lnr lnr-dice"></i> <span>Tables</span></a></li>
                <li><a href="typography.html" class=""><i class="lnr lnr-text-format"></i> <span>Typography</span></a></li>
                <li><a href="icons.html" class=""><i class="lnr lnr-linearicons"></i> <span>Icons</span></a></li>  --}}
            </ul>
        </nav>
    </div>
</div>