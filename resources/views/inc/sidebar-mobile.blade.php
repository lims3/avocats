<div class="panel panel-headline hidden-md hidden-lg " style="background:#F3F5F8;border:none;">
    <div class="row">
        <!-- ./col -->
        @foreach(\App\Models\Feature::unroot()->get() as $menu)
            @if((auth()->user()->canShow($menu->id) || auth()->user()->isSAdmin()))
                @if( empty($menu->description['hidden']))
                    <div class="col-lg-3 col-xs-6 col-md-6">
                        <!-- small box -->
                        <a href="{{ route($menu->route, array_merge(['__p'=>$menu->id ], isset($menu->description['param']) ? $menu->description['param'] : [] )) }}" style="color:#fff;">
                            <div class="small-box bg-{{ array_random(['orange', 'blue', 'yellow', 'red', 'green']) }}">
                                <div class="inner">
                                    <h3> <i class="{{ $menu->description['icon'] ?? 'lnr lnr-file-empty' }}"></i></h3>
                                    {{ ucfirst(strtolower($menu->name)) }}
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                            </div>
                        </a>
                    </div>
                {{--      <li>
                          <a href="{{ route($menu->route, array_merge(['__p'=>$menu->id ], isset($menu->description['param']) ? $menu->description['param'] : [] )) }}" class="{{ (route($menu->route) == app('url')->full()) ? 'active':'' }}">
                              <i class="{{ $menu->description['icon'] ?? 'lnr lnr-file-empty' }}"></i>
                              <span>{{ ucfirst(strtolower($menu->name)) }}</span>
                          </a>
                      </li>--}}
            @endif
        @endif
    @endforeach
    <!-- ./col -->
    </div>
</div>