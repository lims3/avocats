@extends('layouts.admin')



@section('content')

	<div class="row">
              <div class="col-md-12 col-xs-12 hidden-xs hidden-sm " style="">
                <div class="x_panel" style="border-top:0;border-top:3px solid #1b2254e3; ">
                   <div class="x_title">
                   
                    <div class="clearfix"></div>
                  </div> 
                  <div class="x_content" style="height:33px; ">
                 <!--    <p style="margin-left:1%;">Donnez du sens à votre article en lui donnant un titre</p> -->
                    <!-- <br /> -->
                   
                         <form class="form-horizontal form-label-left">

                      <div class="form-group">
                        
                        
                        <div class="col-md-2 col-sm-9 col-xs-12">
                         <!-- <div class="col-md-8"> -->
                         <!-- <span style="margin-top:-12px;">donnée à project</span> -->
                           <select class="form-control" style="">
                             <option>Information à projecter</option>
                             <option>Articles</option>
                             <option>Questions</option>
                             <option>Dossiers</option>
                           </select>
                         
                        <!-- </div> -->
                      </div>
                      <div class="col-md-2 col-sm-9 col-xs-12">
                         <!-- <div class="col-md-8"> -->
                           <select class="form-control" style="">
                             <option>Année en cours</option>
                             <option></option>
                             <option>Questions</option>
                             <option>Dossiers</option>
                           </select>
                         
                        <!-- </div> -->
                      </div>
                      <div class="col-md-2">
                           <button class="btn btn-primary" style="border-radius:0;background:#1b2254e3;">Projecter</button>
                      </div>
                      <!-- <div class="col-md-6 "> -->
                           <!-- <button class="btn btn-primary">Trier</button> -->
                   <!--         <div class="pull-right">
                             <div class="col-md-6 ">
                             <input type="text" class="form-control" name="" placeholder="Rechercher un article rapidement">
                           </div>
                            <div class="col-md-6">
                             <button class="btn btn-primary" style="border-radius:0;background:#1b2254e3;">Rechercher</button>
                           </div>
                           </div>
                      </div> -->

                      
                      </div>

                    </form>

                    
                  </div>
                </div>

              </div>
            </div>

            <div class="row">
              
                <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Les pulications faites au courant de l'année <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    <!--   <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content"><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe>
                    <canvas id="lineChart" width="484" height="242" style="width: 484px; height: 242px;"></canvas>
                  </div>
                </div>
              </div>

              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Répresentation de la part des articles <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    <!--   <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content"><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe>
                    <canvas id="pieChart" width="484" height="242" style="width: 484px; height: 242px;"></canvas>
                  </div>
                </	div>
              </div>

            </div>
@stop