@extends('layouts.admin')



@section('content')
		
	 <div class="row">
    	@include('inc.message')
    
  	</div>
  	<div class="row">
  		@if($errors->any())
  			<ul>
  			@foreach($errors->all() as $error)

  				<li>{{$error}}</li>

  			@endforeach

  			</ul>
  		@endif
  	</div>	
	<div class="row">
		<div class="title_left col-md-offset-3">
	        <h3>&nbsp;&nbsp;Editer votre profil</h3>
	    </div>
	</div>

	<div class="row">
              
              <div class="col-md-6 col-xs-12 col-md-offset-3" >
                

                

                <div class="x_panel" style="border-top:3px solid #1b2254e3; ">
                  <div class="x_title">
                    <h2>Mon profil <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   
                    <form id="demo-form" action="{{route('profile.store')}}" method="post" enctype="multipart/form-data" data-parsley-validate="" novalidate="">
                    	{{csrf_field()}}
                    	<div class="row">
			              <div class="col-md-12 col-sm-12 col-xs-12 text-center">
			                <img src="{{asset('img/upload/').$picture}}" class="" style="border:3px solid #1b2254e3;width:140px;height:140px;border-radius:3px;">
			              </div>
			              <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    			<div class="form-group text-center">
                            <br>
                    				<label for="picture" class="picture">Choisir une photo &nbsp; <i class="glyphicon glyphicon-picture"></i> </label>
                            <input type="file" id="picture" name="file">
                            <br>
                    			</div>
                    		</div>
			            </div>
                    	<div class="row">
                    		
                    	</div>
                    	<div class="row">
                    		<div class="col-md-6 col-sm-12 col-xs-12">
                    			<div class="form-group">
                    				<div class="input-group">
                    					<input type="text" class="form-control" placeholder="prénom" name="firstname"  value="{{$lawyerProfile->firstname}}">
                    					<span class="input-group-addon custom-addon-other">
                    						<i class="glyphicon glyphicon-user"></i>
                    					</span>
                    				</div>
                    			</div>
                    		</div>
                    		<div class="col-md-6 col-sm-12 col-xs-12">
                    			<div class="form-group">
                    				<div class="input-group">
                    					<input type="text" class="form-control" placeholder="nom" name="name"  value="{{$lawyerProfile->name}}">
                    					<span class="input-group-addon custom-addon-other">
                    						<i class="glyphicon glyphicon-user"></i>
                    					</span>
                    				</div>
                    			</div>
                    		</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-6 col-sm-12 col-xs-12">
                    			<div class="form-group">
                    				<div class="input-group">
                    					<input type="text" class="form-control" placeholder="postnom" name="lastname"  value="{{$lawyerProfile->lastname}}">
                    					<span class="input-group-addon custom-addon-other">
                    						<i class="glyphicon glyphicon-user"></i>
                    					</span>
                    				</div>
                    			</div>
                    		</div>
                    		<div class="col-md-6 col-sm-12 col-xs-12">
                    			<div class="form-group">
                    				<div class="input-group">
                    					<input type="text" class="form-control" placeholder="adresse" name="adresse"  value="{{$lawyerProfile->adresse}}">
                    					<span class="input-group-addon custom-addon-other">
                    						<i class="glyphicon glyphicon-map-marker"></i>
                    					</span>
                    				</div>
                    			</div>
                    		</div>
                    	</div>
                    	 	<div class="row">
                    		<div class="col-md-6 col-sm-12 col-xs-12">
                    			<div class="form-group">
                    				<!-- <div class="input-group"> -->
                    					<select class="form-control" id="sexe">
                                <option>Sexe</option>
                                <option value="1">Masculin</option>
                    						<option value="2">Feminin</option>
                    						<!-- <option>Domaine de compétence</option> -->
                    					</select>
                              <input type="hidden"  name="sexe" value="{{$lawyerProfile->sexe}}">
                    				<!-- </div> -->
                    			</div>
                    		</div>
                    		<div class="col-md-6 col-sm-12 col-xs-12">
                    			<div class="form-group">
                    				<select class="form-control" id="region">
                                <option>Region</option>
                                <option value="1">Kinshasa</option>
                    						<option value="2">Lubumbashi</option>
                                <option value="3">Matadi</option>
                                <option value="4">Mbuji-Mayi</option>
                    						<!-- <option>Domaine de compétence</option> -->
                    					</select>
                              <input  type="hidden" name="region" value="{{$lawyerProfile->province}}">
                    			</div>
                    		</div>
                    	</div>
                    	 	<div class="row">
                    		<div class="col-md-6 col-sm-12 col-xs-12">
                    			<div class="form-group">
                    				<div class="input-group">
                    					<input type="text" class="form-control" placeholder="téléphone" name="phone"  value="{{$lawyerProfile->phone}}"   >
                    					<span class="input-group-addon custom-addon-other">
                    						<i class="glyphicon glyphicon-phone"></i>
                    					</span>
                    				</div>
                    			</div>
                    		</div>
                    	
                    	</div>
                    	 <p>
	                      Faites une petite description de vous-mêmes.
	                    </p>

                          <textarea rows="5" id="message" required="required" class="form-control" name="description" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" name="answer" style="text-align:left;">
                          	
                          	{{$lawyerProfile->about}}


                          </textarea>

                          <br>
                          <button type="submit" class="btn btn-primary pull-right" style="background:#1b2254e3;border-radius:0;">Modifier</button>

                    <p></p></form>
                    <!-- end form for validations -->

                  </div>
                </div>


              </div>


            </div>



@stop