@extends('layouts.app')



@section('content')

	@if(session('message'))
			<h1>{{session('message')}}</h1>
			{{session()->put('message','')}}
		@endif
		@if($errors->any())

		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $errors)

					<li>{{$errors}}</li>

				@endforeach
			</ul>
		</div>

		@endif
	
	<div class="col-md-12">
		<div class="col-md-8 col-md-offset-2">
			<!-- <h1 style="text-align:center;">Souscrire au service sms</h1> -->

			<div class="col-md-12 panel panel-default">
				<div class="panel-heading">
					<div class="panel-title">
						Mon profil
					</div>
				</div>
			<!-- </div>	 -->
				<form action="{{route('profile.store')}}" method="post" enctype="multipart/form-data" >
				<div class="panel-body">
					<div class="col-md-4">
				<!-- 	<form action="{{route('sms.store')}}" method="post" > -->
						{{csrf_field()}}
						<div class="circle" style="width:90px;height:90px;background:#ccc;border-radius:50%;">
							<img src="{{asset('img/upload/'.$picture)}}" style="width:90px;height:90px;background:#ccc;border-radius:50%;">
						</div>
						<br>
						<!-- <button type="submit" class="btn btn-primary">Souscrire</button> -->
						<input type="file" class="" name="file">
					
				</div>

				<div class="col-md-5">
					<div class="form-group">
					<label>Nom</label> 
					<input type="text" class="form-control" disabled="true" value="{{$lawyerProfile->Name}}" name="">
					</div>

					<div class="form-group">
						<label>Postnom</label> 
						<input type="text" class="form-control" disabled="true" value="{{$lawyerProfile->Lastname}}" name="">
					</div>

					<div class="form-group">
						<label>Prénom</label> 
						<input type="text" class="form-control" value="{{$lawyerProfile->FirstName}}" disabled="true" name="">
					</div>

					<div class="form-group">
						<label>Adresse</label> 
						<input type="text" class="form-control" value="{{$lawyerProfile->Adresse}}" disabled="true" name="">
					</div>

					<div class="form-group">
						<label>Domaine d'activité</label> 
						<input type="text" class="form-control" disabled="true" name="">
					</div>
					<div class="form-group">
						<label>Barreau</label> 
						<input type="text" class="form-control" name="">
						<!-- <input type="text" class="form-control" name=""> -->
						
					</div>



	
				</div>
				</div>

				<div class="panel-footer">
					<input type="submit" class="btn btn-primary" value="Mettre à jours" name="">
				</div>

			</form>
			</div>

		</div>

		

	</div>

@stop