@extends('layouts.admin')


@section('content')
		<div class="row">
        <div class="col-md-3   " style="background:#1b2254e3;color:#fff;height:590px;padding-top:20px;box-shadow:0px 0px 5px rgba(0,0,0,0.60);">
            <div class="row">
              <div class="col-md-12 text-center">
                <img src="{{asset('img/avatar.jpg')}}" class="img-circle" style="border:3px solid #fff;">
              </div>
            </div>
             <div class="row">
              <div class="col-md-12 " style="padding-top:18px;">
                <h4 style="color:#1b2254e3;border-bottom:3px solid #fff;padding-bottom:5px;"><span style="background:#fff;padding-top:8px;padding-bottom:8px;padding-right:5px;padding-left:5px;"><i class="glyphicon glyphicon-briefcase" style="color:#1b2254e3;"></i>&nbsp;&nbsp;Compétences</span></h4>
              </div>
            </div>
             <div class="row">
              <div class="col-md-12 " style="padding-top:10px;">
                  <p>nom de la compétence</p>
                  <div class="authorty-level">
                    <span class="authorty-point">10%</span>
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 " style="padding-top:10px;">
                  <p>nom de la compétence</p>
                  <div class="authorty-level">
                    <span class="authorty-point" style="left:80%;">10%</span>
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 " style="padding-top:10px;">
                  <p>nom de la compétence</p>
                  <div class="authorty-level">
                    <span class="authorty-point" style="left:60%">10%</span>
                  </div>
              </div>
            </div>
        </div>
        <div class="col-md-7">
          <div class="panel panel-default" style="border-radius:0;box-shadow:0px 0px 3px rgba(0,0,0,0.60);border:0;height:100%;border-top:3px solid #1b2254e3;">
            <div class="panel-heading" style="background:#fff;height:36px;border-radius:0;">
              <div class="panel-title" style="color:#1b2254e3;">
                <i class="glyphicon glyphicon-user" style="color~:#ccc;"></i>&nbsp;Identité
              </div>
            </div>
            <div class="panel-body">
              <div class="col-md-3">
                <p>Nom <br>
                  </p><h4>Musimbi</h4>
              <p></p>
              </div>
              <div class="col-md-3">
                <p>Posnom <br>
                  </p><h4>Musimbi</h4>
              <p></p>
              </div>
              <div class="col-md-3">
                <p>Prénom <br>
                  </p><h4>Musimbi</h4>
              <p></p>
              </div>
            </div>
          </div>
          <div class="panel panel-default" style="border-radius:0;box-shadow:0px 0px 3px rgba(0,0,0,0.60);border:0;height:150px;border-top:3px solid #1b2254e3;">
            <div class="panel-heading" style="background:#fff;height:36px;border-radius:0;">
              <div class="panel-title" style="color:#1b2254e3;">
                <i class="glyphicon glyphicon-phone" style="color~:#ccc;"></i>&nbsp;Contact
              </div>
            </div>
            <div class="panel-body">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
          </div>
          <div class="panel panel-default" style="border-radius:0;box-shadow:0px 0px 3px rgba(0,0,0,0.60);border:0;height:220px;border-top:3px solid #1b2254e3;">
            <div class="panel-heading" style="background:#fff;height:36px;border-radius:0;">
              <div class="panel-title" style="color:#1b2254e3;">
                <i class="glyphicon glyphicon-user"></i>&nbsp;A propos
              </div>
            </div>
            <div class="panel-body">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
          </div>
        </div>
    </div>



@stop