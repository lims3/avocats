@extends('layouts.admin')



@section('content')

		
		
	 <div class="row">
    @include('inc.message')
    
  </div>
	<div class="row">
		<div class="title_left">
	        <h3>&nbsp;&nbsp;Conseiller et assister nos utilisateurs dans leurs problèmes quotidien </h3>
	    </div>
	</div>

	<div class="row">
              
              <div class="col-md-6 col-xs-12" >
                

                

                <div class="x_panel" style="border-top:3px solid #1b2254e3; ">
                  <div class="x_title">
                    <h2>Votre réponse <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p>
                      Donnez des pistes de solution aux gens en repondant aux questions et augmentez votre notoriété.
                    </p>
                    <!-- start form for validation -->
                    <form id="demo-form" action="{{route('active')}}" method="post" data-parsley-validate="" novalidate="">
                    	{{csrf_field()}}

                    	<div class="row">
                    			<div class="form-group">
		                    		<div class="col-md-6">
		                    			<label class="label-control">Numero d'ordre</label>
		                    				<input type="text" class="form-control" name="ordre" placeholder="ex:xxxx">
		                    			<!-- </label> -->
		                    		</div>
		                    		<div class="col-md-6">
		                    			<label class="label-control">Nom du barreau</label>
		                    			<!-- <label class="label-control"> -->
		                    				<input type="text" placeholder="ex:matete" class="form-control" name="barreau">
		                    			<!-- </label> -->
		                    		</div>
                    			</div>
                    	</div>
                    	<br>
                    	<div class="row">
                    			<div class="form-group">
		                    		<div class="col-md-6">
		                    			<label class="label-control">Date de prestation de serment</label>
		                    				<input type="text" class="form-control" name="date" placeholder="ex:25/06/1998">
		                    			<!-- </label> -->
		                    		</div>
		                    		<div class="col-md-6">
		                    			<label class="label-control">Nom du cabinet</label>
		                    			<!-- <label class="label-control"> -->
		                    				<input type="text" placeholder="ex:matete" class="form-control" name="name">
		                    			<!-- </label> -->
		                    		</div>
                    			</div>
                    	</div>
                    	<br>
                    		<div class="row">
                    			<div class="form-group">
		                    		<div class="col-md-6">
		                    			<label class="label-control">Adresse du cabinet</label>
		                    				<input type="text" class="form-control" name="cabinet" placeholder="">
		                    			<!-- </label> -->
		                    		</div>
		                    		<!-- <div class="col-md-6">
		                    			<label class="label-control">Nom du cabinet</label> -->
		                    			<!-- <label class="label-control"> -->
		                    				<!-- <input type="text" placeholder="ex:matete" class="form-control" name=""> -->
		                    			<!-- </label> -->
		                    		<!-- </div> -->
                    			</div>
                    	</div>
                    	<br>
                          <textarea style="text-align:left;" rows="5" id="message" required="required" class="form-control" name="dashboard" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" name="answer">
                          	Etes-vous un avocat stagiaire ou vous êtes inscrit au tableau?
                          </textarea>

                          <br>
                          <button type="submit" class="btn btn-primary pull-right" style="background:#1b2254e3;border-radius:0;">Répondre</button>

                    <p></p></form>
                    <!-- end form for validations -->

                  </div>
                </div>


              </div>


            </div>





@stop