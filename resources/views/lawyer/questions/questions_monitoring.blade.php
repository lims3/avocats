@extends('layouts.admin')


@section('content')
  <div class="row">
    @include('inc.message')
    
  </div>
	<div class="row">
		<div class="title_left">
	        <h3>&nbsp;&nbsp;Repondez aux questions facilement </h3>
	    </div>
	</div>
   
  <div class="row">
    <div class="col-md-12 col-xs-12  ">
                <div class="x_panel" style="border-top:0;border-top:3px solid #1b2254e3; ">
                   <div class="x_title">
                   
                    <div class="clearfix"></div>
                  </div> 
                  <div class="x_content" style="height:33px; ">
                 <!--    <p style="margin-left:1%;">Donnez du sens à votre article en lui donnant un titre</p> -->
                    <!-- <br /> -->
                   
                         <form class="form-horizontal form-label-left">

                      <div class="form-group">
                        
                        
                        <div class="col-md-2 col-sm-9 col-xs-12" style="border-right:1px solid #ccc;">
                            Questions <span class="badge badge-success">{{$total}}</span>
                        </div>
                        <div class="col-md-2" style="border-right:1px solid #ccc;">
                            Repondues <span class="badge badge-success">  {{$answered}}</span>
                        </div>
                        <div class="col-md-2">
                            En attente <span class="badge badge-success">{{$pedding}}</span>
                        </div>
                      <!-- <div class="col-md-6 "> -->
                           <!-- <button class="btn btn-primary">Trier</button> -->
                        <!--    <div class="pull-right">
                             <div class="col-md-6 ">
                             <input type="text" class="form-control" name="" placeholder="Rechercher un article rapidement">
                           </div>
                            <div class="col-md-6">
                             <button class="btn btn-primary" style="border-radius:0;background:#1b2254e3;">Rechercher</button>
                           </div>
                           </div>
                      </div>

                       -->
                      </div>

                    </form>

                    
                  </div>
                </div>

              </div>
  </div>
	<!-- <div class="row"> -->
	<!-- 	<div class="col-md-12 col-xs-12 hidden-xs ">
                <div class="x_panel" style="border-top:0;border-top:3px solid #1b2254e3; ">
                   <div class="x_title">
                   
                    <div class="clearfix"></div>
                  </div> 
                  <div class="x_content" style="height:33px; "> -->
                 <!--    <p style="margin-left:1%;">Donnez du sens à votre article en lui donnant un titre</p> -->
                    <!-- <br /> -->
                   
                      <!--    <form class="form-horizontal form-label-left">

                      <div class="form-group">
                        
                        
                        <div class="col-md-2 col-sm-9 col-xs-12">
                         <div class="col-md-8">
                           <select class="form-control" style="">
                             <option>Croissant</option>
                             <option>Décroisant</option>
                           </select>
                          -->
                        <!-- </div> -->
                    <!--   </div>
                      <div class="col-md-4">
                           <button class="btn btn-primary" style="border-radius:0;background:#1b2254e3;">Trier</button>
                      </div>
                      <div class="col-md-6 "> -->
                           <!-- <button class="btn btn-primary">Trier</button> -->
                     <!--       <div class="pull-right">
                             <div class="col-md-6 ">
                             <input type="text" class="form-control" name="" placeholder="Rechercher un article rapidement">
                           </div>
                            <div class="col-md-6">
                             <button class="btn btn-primary" style="border-radius:0;background:#1b2254e3;">Rechercher</button>
                           </div>
                           </div>
                      </div>

                      
                      </div>

                    </form>

                    
                  </div>
                </div>

              </div>
	</div> -->
	<div class="row">
			
			<div class="row" style=" ">
                <div class="col-md-12 col-sm-6 col-xs-12" style="padding-left:20px;padding-right:20px;">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Toutes les questions posées <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table table-bordered table-hover">
                      <thead style="background:#1b2254e3;color:#fff;">
                        <tr>
                          <th>#</th>
                          <!-- <th>Noms</th> -->
                          <th>Questions</th>
                          <!-- <th>Déscription</th> -->
                          <th>Date de création</th>
                          <th>Status actuel</th>
                          <th class="hidden-xs hidden-sm">Action à appliquer</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        
                        @foreach($questions as $question)

                          <tr>
                            <td>{{$index++}}</td>
                            <!-- <td>{{'inconnu'}}</td> -->
                            <td>
                              {{str_limit($question->question,60)}}

                               <br>
                               <br> 
                              <a class="hidden-md hidden-lg btn btn-primary" style="background:#1b2254e3;color:#fff;
                              border-radius:0;" data-toggle="modal" 
                              data-target=".{{$question->id}}"><i class="fa fa-eye"></i></a>

                              <a href="{{route('answers',['id'=>$question->id])}}" class="btn btn-default hidden-md hidden-lg" style="background:#1b2254e3;color:#fff;">&nbsp;<i class="fa fa-check"></i></a>
                     
                              <a  class="btn btn-default hidden-md hidden-lg" style="background:#1b2254e3;color:#fff;"><i class="fa fa-edit" data-toggle="modal" data-target=".r{{$question->id}}"></i></a>
                            </td>
                            <td>{{$question->created_at}}</td>
                            <!-- <td>25/25/1997</td> -->
                            <td>
                            @if($question->answers())
                              Répondus&nbsp;&nbsp;<span class='badge badge'>{{count($question->answers()->get())}}</span>
                            @else

                              Aucune réponse

                            @endif
                         
                          </td>
                            <td class="">
                              <a class="btn btn-primary hidden-xs hidden-sm" style="background:#1b2254e3;color:#fff;
                              border-radius:0;" data-toggle="modal" 
                              data-target=".{{$question->id}}"><i class="fa fa-eye"></i></a>

                                  <div class="modal fade {{$question->id}}" style="display:none;" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                      <div class="modal-dialog modal-xs" role="document" style="
                                      border-radius:0;">
                                        <div class="modal-content" style="
                                      border-radius:0;" >
                                            <div class="modal-header" style="background:#1b2254e3;color:#fff;height:48px;line-height:48px;">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" 
                                            ><span aria-hidden="true" style="color:#fff;">&times;</span></button>
                                            <h4 class="modal-title" id="gridSystemModalLabel" "><i></i>Question possée
                                          </h4>
                                          </div>

                                          <div class="modal-body">
                                            <h5 style="text-align:center;"><i style="font-size:25px;" class="fa fa-question"></i></h5>
                                            <hr>
                                              <p style="font-size:15px;text-align:justify;">
                                              {{ucfirst($question->question)}}
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                        
                              <a href="{{route('answers',['id'=>$question->id])}}" class="btn btn-default hidden-xs hidden-sm" style="background:#1b2254e3;color:#fff;">&nbsp;<i class="fa fa-check"></i></a>
                     
                              <a  class="btn btn-default hidden-xs hidden-sm" style="background:#1b2254e3;color:#fff;"><i class="fa fa-edit" data-toggle="modal" data-target=".r{{$question->id}}"></i></a>

                              <div class="modal fade r{{$question->id}}" style="display:none;" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                      <div class="modal-dialog modal-xs" role="document" style="
                                      border-radius:0;">
                                        <div class="modal-content" style="
                                      border-radius:0;" >
                                            <div class="modal-header" style="background:#1b2254e3;color:#fff;height:48px;line-height:48px;">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" 
                                            ><span aria-hidden="true" style="color:#fff;">&times;</span></button>
                                            <h4 class="modal-title" id="gridSystemModalLabel" "><i></i>
                                            Reponses à la question
                                          </h4>
                                          </div>

                                          <div class="modal-body" style="max-height:400px;overflow-y:scroll; "> 

                                            @foreach($question->answers()->get() as $answer)
                                            <br>
                                            <div class="row">
                                              <div class="col-md-12" style="background:#eee;color:#000;padding-top:6px;padding-bottom:6px;">
                                                <div class="col-md-1">
                                                  <img style="width:30px;height:30px;" src="{{asset('img/avatar.jpg')}}" class="img-circle img-responsive">
                                                </div>
                                                <div class="col-md-10">
                                          <strong>
                                            {{ 
                                              ucfirst($answer->lawyer()->first()->profileOfLawyer()->first()->firstname).' '.$answer->lawyer()->first()->profileOfLawyer()->first()->name

                                            }}
                                          </strong>
                                                  <br>
                                                  <p>
                                                      {{$answer->answer}}
                                                  </p>
                                                </div>
                                              </div>
                                              <hr>
                                            </div>

                                            @endforeach
                                       
                               
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                            </td>
                          </tr>

                        @endforeach
                        
                      </tbody>
                    </table>
                    <hr>
                    
                  </div>
                </div>
              </div>
              </div>

	</div>


@stop


                                      <!--       <div class="row">
                                              <div class="col-md-12" style="background:#eee;color:#000;padding-top:6px;padding-bottom:6px;">
                                                <div class="col-md-1">
                                                  <img style="width:30px;height:30px;" src="{{asset('img/avatar.jpg')}}" class="img-circle img-responsive">
                                                </div>
                                                <div class="col-md-10">
                                                  <strong>Steve Arvey</strong>
                                                  <br>
                                                  <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                  </p>
                                                </div>
                                              </div>
                                              <hr>
                                            </div> -->