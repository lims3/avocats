@extends('layouts.app')


@section('content')

	<div class="row">
			<div class="col-md-12">
				@if(session('message'))
					<h1>{{session('message')}}</h1>
					{{session()->put('message','')}}
				@endif
				<div class="col-md-8 col-md-offset-2">
					<div class="row">
						<div class="col-md-5">
							<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title">
									Configuration des paramèttres utilisateur
								</div>
							</div>
							<form  method="post" action="{{route('setting.store')}}">
								{{csrf_field()}}
								<div class="panel-body">
									<label>Moyen utilisé pour la réception de notification</label>
									<select name="notification" class="form-control">
										<option value="0">Réception par sms</option>
										<option value="1">Réception par mail</option>
										<option value="2">Réception en inbox(compte utilisateur)</option>
									</select>
								</div>
								<div class="panel-footer text-right">
									<button class="btn btn-primary" style="">Configurer</button>
								</div>
							</form>
						</div>
						</div>
					</div>
			</div>
		</div>
	</div>


@stop