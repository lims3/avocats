@extends('layouts.app')


@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6 col-md-offset-4">
						


					</div>
				</div>

				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<div class="row">
							<div class="col-md-6">
								{{$messages->links()}}
							</div>
						</div>
						<form action="{{route('apply')}}" method="post" enctype="multipart/form-data">
										{{csrf_field()}}
						@foreach($messages as $message)

							@if($message->message_type == 'MESSAGE_IMAGE')

								<div class="row">
									<div class="col-md-4">
										<img style="width:60px;height:60px;" src="{{asset('img/upload/'.$message->message)}}">
									</div>
								</div>

							@endif
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
							<div class="row">
								<div class="col-md-4">
									{{$message->message}}
								</div>
							</div>
							<br>
							<input type="hidden" name="id" value="{{$id}}">


						@endforeach
						
						<div class="row">
								<div class="col-md-6">
									
										<textarea class="form-control" name="message"></textarea>
										
										<label>Ajouter une image</label>
										<input type="file" name="file">
										<br>
										<button class="btn btn-primary">Envoyer</button>
									</form>
								</div>
							</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
@stop