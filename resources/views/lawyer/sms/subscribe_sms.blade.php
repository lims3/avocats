@extends('layouts.app')



@section('content')
	
		@if(session('message'))
			<h1>{{session('message')}}</h1>
			{{session()->put('message','')}}
		@endif
		@if($errors->any())

		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $errors)

					<li>{{$errors}}</li>

				@endforeach
			</ul>
		</div>

		@endif
	
	<div class="col-md-12">
		<div class="col-md-6 col-md-offset-4">
			<h1>Soucrire au service sms</h1>

			<div class="col-md-7">
			<form action="{{route('sms.store')}}" method="post" >
				{{csrf_field()}}

				<div class="form-group">
					<label class="control-label">Offre sms</label>
					<select name="offre" class="form-control">
						<option value="10">10 sms à 2$</option>	
						<option value="20">20 sms à 4$</option>	
						<option value="30">30 sms à 6$</option>	
					</select>
				</div>

				<button type="submit" class="btn btn-primary">Souscrire</button>
			</form>
		</div>

		</div>

		

	</div>
@stop