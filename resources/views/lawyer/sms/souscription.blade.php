@extends('layouts.admin')



@section('content')
		
    <div class="row">
      @include('inc.message')
    </div>

    <div class="row">
        @if($errors->any())

          <div class="alert alert-danger">
            <ul>
              @foreach($errors->all() as $errors)

                <li>{{$errors}}</li>

              @endforeach
            </ul>
          </div>

        @endif
    </div>

		<div class="row">
			<div class="title_left">
                <h3>&nbsp;&nbsp;Souscrivez au service sms </h3>
              </div>
		</div>

		<div class="row" style="padding-left:9px;">
              <div class="col-md-12 " style="background:#fff;height:50px;margin-bottom:15px;border-left:4px solid #1b2254e3;box-shadow:0px 0px 2px rgba(0,0,0,0.40);  ">
                <div>
                  <!-- <p>Note d'itulisation</p> -->
                  <p>
                    En souscrivant à ce service, vous recevrez  des notifications  des consultations  demandées par les clients  directement sur votre téléphone via sms.
                  </p>
                </div>
              </div>
        </div>

        <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12  text-center">
                      <div class="panel panel-default" style="box-shadow:0px 0px 3px rgba(0,0,0,0.60);border:0;border-radius:0;border-top:3px solid #1b2254e3;">
                        <div class="panel-body">
                          <h3 style="line-height:40px;">Offre vimba</h3>
                          <strong style="line-height:40px;font-size:15px;">sms 30</strong>
                          <br>
                          <span class="badge badge" style="background:#1b2254e3;padding:10px;font-size:17px;">$30</span>
                        </div>
                        <div class="panel-footer" style="background:#fff;">
                          <form action="{{route('sms.store')}}" method="post" >
                              {{csrf_field()}}
                              <input type="hidden" name="offre" value="10">
                            <button class="btn btn-primary btn-block" style="border-radius:0;background:#1b2254e3;">Souscrire</button>
                         </form>
       
                      </div>
                      </div>
                      
                    </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                      <div class="panel panel-default" style="box-shadow:0px 0px 3px rgba(0,0,0,0.60);border:0;border-radius:0;border-top:3px solid #1b2254e3;">
                        <div class="panel-body">
                          <h3 style="line-height:40px;">Offre vimba</h3>
                          <strong style="line-height:40px;font-size:15px;">sms 30</strong>
                          <br>
                          <span class="badge badge" style="background:#1b2254e3;padding:10px;font-size:17px;">$30</span>
                        </div>
                        <div class="panel-footer" style="background:#fff;">
                          <form action="{{route('sms.store')}}" method="post" >
                              {{csrf_field()}}
                              <input type="hidden" name="offre" value="20">
                        <button class="btn btn-primary btn-block" style="border-radius:0;background:#1b2254e3;">Souscrire</button>
                      </form>
                      </div>
                      </div>
                      
                    </div>
                    
                  </div>
                  <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12  text-center">
                      <div class="panel panel-default" style="box-shadow:0px 0px 3px rgba(0,0,0,0.60);border:0;border-radius:0;border-top:3px solid #1b2254e3;">
                        <div class="panel-body">
                          <h3 style="line-height:40px;">Offre vimba</h3>
                          <strong style="line-height:40px;font-size:15px;">sms 30</strong>
                          <br>
                          <span class="badge badge" style="background:#1b2254e3;padding:10px;font-size:17px;">$30</span>
                        </div>
                        <div class="panel-footer" style="background:#fff;">
                          <form action="{{route('sms.store')}}" method="post" >
                              {{csrf_field()}}
                              <input type="hidden" name="offre" value="30">
                        <button class="btn btn-primary btn-block" style="border-radius:0;background:#1b2254e3;">Souscrire</button>
                      </form>
                      </div>
                      </div>
                      
                    </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                      <div class="panel panel-default" style="box-shadow:0px 0px 3px rgba(0,0,0,0.60);border:0;border-radius:0;border-top:3px solid #1b2254e3;">
                        <div class="panel-body">
                          <h3 style="line-height:40px;">Offre vimba</h3>
                          <strong style="line-height:40px;font-size:15px;">sms 30</strong>
                          <br>
                          <span class="badge badge" style="background:#1b2254e3;padding:10px;font-size:17px;">$30</span>
                        </div>
                        <div class="panel-footer" style="background:#fff;">
                          <form action="{{route('sms.store')}}" method="post" >
                              {{csrf_field()}}
                              <input type="hidden" name="offre" value="30">
                        <button class="btn btn-primary btn-block" style="border-radius:0;background:#1b2254e3;">Souscrire</button>
                      </form>
                      </div>
                      </div>
                      
                    </div>
                    
                  </div>



@stop