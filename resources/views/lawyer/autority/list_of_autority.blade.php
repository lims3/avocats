@extends('layouts.app')


@section('content')
		@if(session('message'))
			<h1>{{session('message')}}</h1>
			{{session()->put('message','')}}
		@endif
		
		<div class="col-md-6 col-md-offset-3 ">
				<h1>Toutes les compétences <strong>{{count($autorities)}}</strong></h1>

				@foreach($autorities as $autority)
					<h3>{{$autority->Name}}</h3>
					<div style="width:400px;height:30px;background:#155479c9; border-radius:5px; ">
							<div style="background:#681c1c;width:{{$autority->Level.'%'}};height:30px;text-align:center; color:#fff;line-height:30px; border-radius:5px  5px ">
									{{$autority->Level}}%
							</div>
					</div>
					<br>
					<a href="" class="btn btn-primary">Supprimer</a>

				@endforeach
		</div>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

@stop

	