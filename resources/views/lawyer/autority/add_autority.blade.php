@extends('layouts.app')


@section('content')
		@if(session('message'))
			<h1>{{session('message')}}</h1>
			{{session()->put('message','')}}
		@endif
		@if($errors->any())

		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $errors)

					<li>{{$errors}}</li>

				@endforeach
			</ul>
		</div>

		@endif
		<div class="col-md-6 col-md-offset-3 ">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="panel-title">
						<span>Ajouter un une compétence</span>
					</div>
				</div>
				<form method="post" action="{{route('autority.store')}}">
					<div class="panel-body">
						
							{{csrf_field()}}
							<div class="form-group>">
								<label class="label-control pull-left">Nom de la compétence</label>	
									<input type="text" class="form-control" name="name">
							</div>

							<div class="form-group">
								<label class="control-label pull-left">Niveau de la compétences</label>
									<input type="range" min="10" max="100" value="20" step="1"  name="level">
							</div>
					
					</div>
					<div class="panel-footer">
						<button type="submit" class="btn btn-success ">Ajouter la compétence</button>
					</div>
					</form>	
				</div>

			</div>
		</div>
	</div>

@stop

