@extends('layouts.app')


@section('content')

	<div class="container">
		<div class="row">
			@include('inc.message')
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6 col-md-offset-4">
						


					</div>
				</div>
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title" >
									<h3>Dossier à traiter <span class="badge badge-primary"> 
							{{count($onStandby)}}</span></h3>
								</div>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
								<table class="table table-hover table-bordered table-striped ">
									<thead>
										<tr>
											<th>Objet ou raison</th>
											<th>Explication</th>
											<th>Nom du client</th>
											<th>Postnom</th>
											<th>Prénom</th>
											<th>Téléphone</th>
											<th>Procédure</th>
										</tr>
									</thead>

									<tbody>
										@foreach($onStandby as $onStandbyto)
										<tr>
											<td>{{$onStandbyto->Reason}}</td>
											<td>{{$onStandbyto->Explanation}}</td>
											<td>{{$onStandbyto->client->Name}}</td>
											<td>{{$onStandbyto->client->Lastname}}</td>
											<td>{{$onStandbyto->client->Firstname}}</td>
											<td>{{$onStandbyto->client->Number}}</td>
											<td>
												<a href="{{route('treating',['id'=>$onStandbyto->id])}}" class="btn btn-danger">Traiter</a>
												<!-- <a href="" class="btn btn-success">Converser</a> -->
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
@stop