@extends('layouts.stability')



@section('content')
		<div class="container">
		<div class="row" style="margin-bottom:35px;margin-top:120px;">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
           <div class="custom-text text-center">
              <h2 class="text-center text-style">Résultat de la recherche</h2>
              <i class="glyphicon glyphicon-search" style="color:#1b2254e3;font-size:35px;"></i>
              <hr style="width:60%;">
            </div>

	            			<div class="row" style="">
	            				
            	@foreach($usr as $user)
            			
            			@foreach($user as $users)
            			
            	
            		
            					
            				
	            					<div class="col-md-6 col-sm-12 col-xs-12  " style="">
	                    
	                    <div class="box-style text-center overlay" style="	">
	                    
	                      
	                	@if($users->profile->picture()->orderBy('id','desc')->first())
	                			<img src="{{asset('img/upload/'.$users->profile->picture()->orderBy('id','desc')->first()->Name??'avatar.jpg')}}"style="height:80px;width:80px;" class="img-circle img-response">

	                	@else
	                				<img src="{{asset('img/upload/avatar.jpg')}}"style="height:80px;width:80px;" class="img-circle img-response">
	                	@endif
	                    
	                    <br>
	                    <!-- <br> -->
	                    <h3 style="color:#1b2254e3;">
	                    {{$users->profile()->first()->firstname.' '.$users->profile()->first()->name}}
	                	</h3>
	                    <i style="color:#1b2254e3;font-size:23px;" class="glyphicon glyphicon-map-marker"></i>
	                    <p><strong style="font-size:18px;color:#676464;">Kinshasa</strong></p>
	                    <hr>
	                    <div class="row">
	                      <div class="col-md-4">
	                        Articles
	                        <br>
	                        <span class="badge badge-primary">
	                        	{{$users->articles->count()}}
	                        </span>
	                      </div>
	                      <div class="col-md-4">

	                        Questions 
	                        <br>
	                     	<span style="display:none;">
	                     		<!-- {{$iter=0}} -->
	                     	</span>
	                         <span class="badge badge-primary">
	                         	{{--
	                         @foreach($users->speciality()->first()->questions()->get() as $q)
	                         		@foreach($q->answers()->get() as $ans)

	                         			@if($ans->lawyer_id == $users->id)

	                         				<span style="display:none;">	
	                         					{{$iter=$iter + 1}} 
	                         				</span>
	                         				
	                         			@endif

	                         		@endforeach

	                         @endforeach
	                         --}}
	                       	{{$iter}}
	                        </span>
	                      </div>
	                      <div class="col-md-4">
	                        Dossiers
	                        <br>
	                         <span class="badge badge-primary">
	                         	{{count($users->fileOnStandbyOfTreatment()->get())}}
	                         </span>
	                      </div>

	                    </div>
	                    <hr>
	                    <p style="color:#1b2254e3; ">
	                      {{$users->profile()->first()->about}}
	                     </p>
	                     <!-- <hr> -->
	                    <div class="left text-right">
	                       <a href="{{route('send',['lawyer'=>$users->id])}}" class="btn btn-default " style="background:#1b2254e3;border-radius:0;color:#fff;">Consulter</a>
	                    </div>
	                    </div>

	                    
	                </div>

	                	

	                	@endforeach

			      	
            	@endforeach


	            			</div>    

	            			<div class="row">
					          <div class="col-md-12">
					            <nav aria-label="...">
					              <ul class="pager">
					                <li><a href="#"><i class="glyphicon glyphicon-chevron-left"></i></a></li>
					                <li><a href="#"><i class="glyphicon glyphicon-chevron-right"></i></a></li>
					              </ul>
					            </nav>
					          </div>  
					        </div>
            



               <br>
               <br>
             
          </div>
        </div>
      </div>
    </div>
</div>

@stop

	