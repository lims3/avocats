<!Doctype html>
<html class=''>
<head>

<!-- <link rel='stylesheet prefetch' href=''> -->
<title>Avocat.cd | création d'un compte</title>
<link href="{{asset('css/news/css/bootstrap.css')}}" rel="stylesheet" id="bootstrap-css">
<!-- <link href="bootstrap.css" rel="stylesheet" id="bootstrap-css"> -->
<!-- <script src="jquery.js"></script> -->
<!-- <script src="bootstrap.min.js"></script> -->



<!-- <script src="bootstrap.min.js"></script> -->


<style class="cp-pen-styles">
  .field-box{
    /*border-bottom:3px solid #1b2254e3;*/
  }
  .field-text{
    /*background:#1b2254e3;*/
    color:#9e9c9c;
    padding-top:6px;
    padding-left:3px;
    padding-right:3px;
  }
  .custom-btn
  {
    /*background:#1b2254e3 !important;*/
    border-radius:0 !important;
    border-right:0 !important;
    background:#fff !important;
    height:35px !important;


    /*border:0;*/
  }

  .custom-btn-border
  {
  	  border-radius:0 !important;
      /*border-right:0 !important;*/
      background:#fff !important;
      height:35px !important;
  }
  .custom-addon
  {
    border-radius:0 !important;
    background:#fff !important;
    /*border-right:0 !important;*/
    color:#ccc !important;
  }
  .separator
  {
    height:5px;
    border-top:2px solid red;
    width:400px;
    position:absolute;
    /*width:100px;*/
    /*transform:translateX(50%); */
    /*transform:rotate(20%,20%);*/
   -webkit-transform: rotateX(90);
  }
</style>

</head>

<body style="{{$errors->any()?'':'overflow:scroll;'}};background:#FAFAFA;">
    <div class="container" style="padding-top:2px;">

	<div class="container" style="padding-top:5px;">
		<div class="row">
			<div class="col-md-12 text-center" style="padding-right:6px;padding-left:6px; ">
				<h1 style="color:#1b2254e3;font-family:Arial;text-transform:;font-family:Helvetica;font-size:30px;">Envoyer votre dossier facilement</h1>
				<!-- <p style="color:#607d8bf2;font-size:20px;">Resultat produit suite à votre recherche</p> -->
			
				<!-- <br> -->
				<!-- <a href="" class="btn btn-primary">En savoir plus sur la recherche éfféctuée</a> -->
				<hr style="width:25%;">
			</div>
		</div>
		 <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center ">
                    <svg style="margin-top:-10px;color:#1b2254e3;font-size:4px;" viewBox="0 0 54 50" id="hammer" xmlns="http://www.w3.org/2000/svg" width="10%" height="10%"><g fill="currentColor" fill-rule="evenodd"><path d="M51.976 33.23a.999.999 0 0 0-1.398.209l-.595.803-24.108-17.856 3.572-4.822.804.596a.997.997 0 0 0 1.399-.21l2.38-3.213a1 1 0 0 0-.208-1.4L24.179.197a1.002 1.002 0 0 0-1.4.208L20.4 3.618a1 1 0 0 0 .208 1.399l.803.595-10.713 14.465-.804-.595a.999.999 0 0 0-1.399.208l-2.38 3.214a1 1 0 0 0 .208 1.399l9.643 7.143a1 1 0 0 0 1.398-.21l2.38-3.215a1 1 0 0 0-.208-1.398l-.803-.595 3.57-4.821L46.41 39.063l-.595.804a1 1 0 0 0 .208 1.398 4.954 4.954 0 0 0 2.972.98 5.024 5.024 0 0 0 4.02-2.023 5.002 5.002 0 0 0-1.04-6.992zM23.792 2.399l8.036 5.952-1.19 1.607-8.036-5.952 1.19-1.607zm-7.439 26.845L8.317 23.29l1.19-1.607 8.036 5.952-1.19 1.608zm-4.048-7.976L23.018 6.804l4.822 3.57L17.126 24.84l-4.821-3.571zm12.38-3.274l24.108 17.855-1.19 1.608L23.495 19.6l1.19-1.607zm26.726 21.04c-.75 1.013-2.11 1.444-3.29 1.084l2.768-3.74.106-.143.492-.665c.69 1.025.7 2.416-.076 3.464zM17 42h-1.586l-1.707-1.707A.997.997 0 0 0 13 40H5a.997.997 0 0 0-.707.293L2.586 42H1a1 1 0 0 0-1 1v6a1 1 0 0 0 1 1h16a1 1 0 0 0 1-1v-6a1 1 0 0 0-1-1zm-1 6H2v-4h1c.265 0 .52-.105.707-.293L5.414 42h7.172l1.707 1.707A.997.997 0 0 0 15 44h1v4z"></path></g></svg>
        </div>
      </div>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-8 col-md-offset-2">
					<div class="row">
						@if($errors->any())

						<div class="alert alert-danger">
							<ul>
								@foreach($errors->all() as $errors)

									<li>{{$errors}}</li>

								@endforeach
							</ul>
						</div>

						@endif
					</div>	
					@include('inc.message')
					<form method="post" action="{{route('store')}}">
						{{csrf_field()}}
					<div class="panel panel-default" style="border-radius:0;">
				  <!-- <div class="panel-heading">
				    <h3 class="panel-title">Panel title</h3>
				  </div> -->
				  <div class="panel-body">
				    <div class="col-md-6" style="border-right:1px solid #ccc;">
				    	<!-- <h4></h4> -->
				    	<h4 class="field-box" style="font-family:Helvetica;" ><span class="field-text"><i class="glyphicon glyphicon-folder-close"></i>&nbsp;&nbsp;Expliquez-nous votre dossier</span></h4>
				    	<hr>
				    	<div class="form-group">
				    		<div class="input-group ">
 
				    		<input type="text" class="form-control custom-btn-border" style="" placeholder="objet" name="reason" value="{{old('reason')}}">
				    		<span style="border-radius:0;background:transparent;" class="input-group-addon custom-addon" id="sizing-addon1"><i class="glyphicon glyphicon-pencil"></i></span>
							</div>
				    	</div>

				    	<div class="form-group">
				    		<label class="label-control" style="color:#1b2254e3;font-family:Helvetica;">Explication du dossier</label>
				    		<textarea style="border-radius:0;" cols="20" name="explanation" rows="10" class="form-control" value="expliquez votre dossier" value="">
				    			{{old('explanation')}}
				    		</textarea>
				    	</div>
				    </div>
				    <div class="col-md-6">
				    	<!-- <h4></h4> -->
				    	<h4 class="field-box" ><span class="field-text"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;Résumé des faits	</span></h4>
				    	<hr>
				    	<div class="form-group">
				    		<div class="input-group ">
							  <input style="border-right:0; " value="{{old('name')}}" type="text" class="form-control custom-btn" placeholder="nom" name="name" aria-describedby="sizing-addon1">
							  <span style="border-radius:0;background:transparent;" class="input-group-addon custom-addon" id="sizing-addon1"><i class="glyphicon glyphicon-user"></i></span>
							</div>
				    	</div>
				    	<div class="form-group">
				    		<div class="input-group ">
							  <input style="border-right:0; " value="{{old('lastname')}}" name="lastname" type="text" class="form-control custom-btn" placeholder="postnom" aria-describedby="sizing-addon1">
							  <span style="border-radius:0;background:transparent;" class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-user custom-addon"></i></span>
							</div>
				    	</div>
				    	<div class="form-group">
				    		<div class="input-group ">
							  <input style="border-right:0; " value="{{old('firstname')}}" name="firstname" type="text" class="form-control custom-btn" placeholder="prénom" aria-describedby="sizing-addon1">
							  <span style="border-radius:0;background:transparent;" class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-user custom-addon"></i></span>
							</div>
				    	</div>
				    	<div class="form-group">
				    		<div class="input-group ">
							  <input style="border-right:0; " value="{{old('number')}}" name="number" type="text" class="form-control custom-btn" placeholder="téléphone ex:243846871678" aria-describedby="sizing-addon1">
							  <span style="border-radius:0;background:transparent;" class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-phone custom-addon"></i></span>
							</div>
				    	</div>
				    	<div class="form-group">
				    		<div class="input-group ">
							  <input style="border-right:0; " value="{{old('email')}}" name="email" type="text" class="form-control custom-btn" placeholder="Email" aria-describedby="sizing-addon1">
							  <span style="border-radius:0;background:transparent;" class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-envelope custom-addon"></i></span>
							</div>
				    	</div>
				    </div>
				  </div>
				  <div class="panel-footer text-right" style="border-top: 2px solid #1b2254e3;">
				  	<button type="submit" class="btn btn-primary" style="background:#060f26cf;border-radius:0;">Soumettre</button>
				  </div>
				</div>
				</form>
				</div>
			</div>	
		</div>

	</div>

</div>
<script type="text/javascript"></script>
</body>
</html>


