@extends('layouts.admin')



@section('content')
		
	 <div class="row">
    	@include('inc.message')
    
  	</div>
  	<div class="row">
  		@if($errors->any())
  			<ul>
  			@foreach($errors->all() as $error)

  				<li>{{$error}}</li>

  			@endforeach

  			</ul>
  		@endif
  	</div>	
	<div class="row">
		<div class="title_left ">
	        <h3>&nbsp;&nbsp;Apporter une aide juridique</h3>
	    </div>
	</div>

	<div class="row">
              
              <div class="col-md-6 col-xs-12 " >
                

                

                <div class="x_panel" style="border-top:3px solid #1b2254e3; ">
                  <div class="x_title">
                    <h2>Selectionner le thématique <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   
                    <form id="demo-form" action="{{route('save')}}" method="post"  data-parsley-validate="" novalidate="">
                    	{{csrf_field()}}
                    
                    	<div class="row">
                          @foreach($speciality as $specialities)

                              <div class="row">
                            <div class="col-md-12">
                              <input type="hidden" name="selectedItem" id="selectedItem" value="">
                                <div class="pretty p-default" >
                                        <input type="checkbox" name="help" class="addHelp"  multiple="true" value="{{$specialities->id}}" />
                                        <div class="state p-primary"> 
                                            <label>
                                              <span style="color:#;">
                                                {{$specialities->name}}
                                        </span>
                                  </label>
                                        </div>
                                    </div>
                            </div>
                          </div>


                          @endforeach

                      </div>
                  
                          <button type="submit" class="btn btn-primary pull-right" style="background:#1b2254e3;border-radius:0;">Ajouter</button>

                    <p></p></form>
                    <!-- end form for validations -->

                  </div>
                </div>


              </div>


            </div>



@stop