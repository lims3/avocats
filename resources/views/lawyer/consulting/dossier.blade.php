@extends('layouts.admin',['dossier'=>count($onStandby)])


@section('content')
	<div class="row">
		
		<div class="col-md-12 col-xs-12 hidden-xs ">
                <div class="x_panel" style="border-top:0;border-top:3px solid #1b2254e3; ">
                   <div class="x_title">
                   
                    <div class="clearfix"></div>
                  </div> 
                  <div class="x_content" style="height:33px; ">
                 <!--    <p style="margin-left:1%;">Donnez du sens à votre article en lui donnant un titre</p> -->
                    <!-- <br /> -->
                   
                         <form class="form-horizontal form-label-left">

                      <div class="form-group">
                        
                        <div class="col-md-2 col-sm-9 col-xs-12" style="border-right:1px solid #ccc;">
                            Dossiers <span class="badge badge-success">{{count($onStandby)}}</span>
                        </div> 
                      <!--   <div class="col-md-2 col-sm-9 col-xs-12" style="border-right:1px solid #ccc;">
                            Traités <span class="badge badge-success">1</span>
                        </div>
                        <div class="col-md-2 col-sm-9 col-xs-12" style="border-right:1px solid #ccc;">
                            En attente <span class="badge badge-success">1</span>
                        </div>
 -->
                      <!-- <div class="col-md-6 "> -->
                           <!-- <button class="btn btn-primary">Trier</button> -->
                    <!--        <div class="pull-right">
                             <div class="col-md-6 ">
                             <input type="text" class="form-control" name="" placeholder="Rechercher un article rapidement">
                           </div>
                            <div class="col-md-6">
                             <button class="btn btn-primary" style="border-radius:0;background:#1b2254e3;">Rechercher</button>
                           </div>
                           </div>
                      </div> -->

                      
                      </div>

                    </form>

                    
                  </div>
                </div>

              </div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-6 col-xs-12" style="padding-right:20px;">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tous les dossiers à traiter<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table table-bordered table-hover">
                      <thead style="background:#1b2254e3;color:#fff;">
                        <tr>
                          <th>N°</th>
                          <!-- <th>Client</th> -->
                          <th>Raison de la sollicitation</th>
                          <th>Explicatino du dossier</th>
                          <!-- <th>Date de création</th> -->
                          <th>Etat</th>
                          <th class="hidden-xs">Action à appliquer</th>
                          <!-- <th>Date de création</th>
                          <th>Status actuel</th>
                          <th>Action à appliquer</th> -->
                        </tr>
                      </thead>
                      <tbody>

                    @foreach($onStandby as $dossier)
                 
                        <tr>
                           <!-- <th scope="row"><input type="checkbox" name=""></th> -->
                           <th scope="row">{{$loop->iteration}}</th>
                          <td>{{$dossier->Reason}}</td>
                          <td>
                             <button type="button" class="btn btn-primary" style="background:#1b2254e3;color:#fff;border-radius:0;" data-toggle="modal" data-target=".{{$dossier->id}}">lire le dossier</button>
                                                <div class="modal fade {{$dossier->id}}" style="display:none;" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                      <div class="modal-dialog modal-lg" role="document" style="
                                      border-radius:0;">
                                        <div class="modal-content" style="
                                      border-radius:0;" >
                                            <div class="modal-header" style="background:#1b2254e3;color:#fff;height:48px;line-height:48px;">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" 
                                            ><span aria-hidden="true" style="color:#fff;">&times;</span></button>
                                            <h4 class="modal-title" id="gridSystemModalLabel" ">Résumé des faits</h4>
                                          </div>

                                          <div class="modal-body">
                                            <p>Raison de la solitation:</p>
                                            <h2>{{ucfirst($dossier->Reason)}}</h2>
                                            <hr>
                                            <p>Client:</p>
                                            <p style="font-size:18px;">Nom:&nbsp;&nbsp;{{$dossier->client()->first()->Name}}&nbsp;/&nbsp;Prénom:&nbsp;&nbsp;{{$dossier->client()->first()->Firstname}}&nbsp;/&nbsp;
                                              <br class="hidden-xs">
                                              Email:&nbsp;&nbsp;{{$dossier->client()->first()->Email}}&nbsp;
                                              <br class="hidden-xs">/&nbsp;Téléphone:&nbsp;&nbsp;{{$dossier->client()->first()->Number}}</p>
                                            <p>Objet du dossier:</p>
                                            <p>
                                              {{$dossier->Explanation}}
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                          </td>
                       
                          <td>traiter</td>
                          <!-- <td>Traiter}</td> -->
                          <td class="hidden-xs"><a disabled="disabled" href="{{route('treating',['id'=>$dossier->id])}}" class="btn btn-default" style="background:#1b2254e3;color:#fff;">Traiter le dossier&nbsp;<i class="fa fa-check"></i></a>
                          
                        </td>
                        </tr>



                    @endforeach
                    </table>

                  </div>
                </div>
              </div>
            
  </div>


                       

      
@stop