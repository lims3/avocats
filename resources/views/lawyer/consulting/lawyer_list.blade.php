@extends('layouts.search_default')


@section('other')
	
	<link rel="stylesheet" type="text/css" href="{{asset('css/custom/custom.min.css')}}">

@stop


@section('content')

	<div class="container" style="padding-top:40px;">
		<div class="row">
			<div class="col-md-12 text-center" style="padding-right:6px;padding-left:6px; ">
				<h1 style="color:#312929d9;font-family:Arial;text-transform:;">"Trouvez un avocat  facilement et rapidement"</h1>
				<p style="color:#607d8bf2;font-size:20px;">Resultat produit suite à votre recherche</p>
				<strong style="font-size:18px;">Consulter un avocat est dévenit une affaire de quelleques minutes...</strong>
				<!-- <br> -->
				<!-- <a href="" class="btn btn-primary">En savoir plus sur la recherche éfféctuée</a> -->
				<hr style="width:900px;">
			</div>
		</div>

		<div class="row">
			<div class="col-md-12" style="padding-right:30px;padding-left:30px;">
				@foreach($users as $user)

				<div class="col-md-3 col-xs-12 widget widget_tally_box" style="">
                        <div class="x_panel fixed_height_390" style="box-shadow:0px 0px 2px rgba(0,0,0,0.60);border:0;height:430px;border-top:3px solid #060f26cf;">
                          <div class="x_content">

                            <div class="flex" style="margin-left:0px;">
                              <ul class="list-inline widget_profile_box">
                                <li>
                                  <a>
                                    <i class="fa fa-facebook"></i>
                                  </a>
                                </li>
                                <li style="margin-left:15px;">
                                  <img src="{{asset('img/person_5.jpg')}}" alt="..." class="img-circle profile_img">
                                </li>
                                <li>
                                  <a>
                                    <i class="fa fa-twitter"></i>
                                  </a>
                                </li>
                              </ul>
                            </div>

                            <h3 class="name">{{$user->profile->name.'/'.$user->profile->lastname}}</h3>

                            <div class="flex" style="margin:0;width:100%;color:#fff;">
                              <ul class="list-inline count2">
                                <li>
                                  <h3 class="badge badge-primary" style="padding:6px; ">{{!empty($user->articles)?count($user->articles):0}}</h3>
                                  <span style="color:#060f26cf">Articles</span>
                                </li>
                                <li>
                                  <h3 class="badge badge-primary" style="padding:6px; ">0</h3>
                                  <span style="color:#060f26cf">Réponses</span>
                                </li>
                                <li>
                                  <h3  class="badge badge-primary" style="padding:6px; ">0</h3>
                                  <span style="color:#060f26cf">Dossiers</span>
                                </li>
                              </ul>
                            </div>
                            <!-- <p>
                              If you've decided to go in development mode and tweak all of this a bit, there are few things you should do.
                            </p> -->
                            @foreach($user->autorities as $autority)
                            <span class="text">{{$autority->Name}}</span>
                            <div class="progress">
							  <div class="progress-bar progress-bar-{{$level[rand(0,1)]}}" role="progressbar" aria-valuenow="{{$autority->Level}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$autority->Level}}%">
							    <span class="sr-only" style="color:#000;">20% Complete</span>
							  </div>
							</div>
							
							@endforeach
							<!-- <button class="btn btn-primary pull-left">A propos</button> -->
							<a href="{{route('send',['lawyer'=>$user->id])}}" class="btn btn-primary pull-right">Consulter</a>
                          </div>
                        </div>
                      </div>

                    @endforeach

			</div>
		</div>
	</div>

@stop

<!-- <span class="text">Matière juridique</span>
							<div class="progress">
							  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
							    <span class="sr-only">40% Complete (success)</span>
							  </div>
							</div> -->

              