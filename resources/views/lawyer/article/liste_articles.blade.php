@extends('layouts.admin')


@section('content')
  <div class="row">
    @include('inc.message')
    
  </div>
	<div class="row" style="padding-left:10px; ">
		<div class="title_left">
	        <h3>Gérer vos articles simplement et facilement </h3>
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12 col-xs-12  ">
                <div class="x_panel" style="border-top:0;border-top:3px solid #1b2254e3; ">
                   <div class="x_title">
                   
                    <div class="clearfix"></div>
                  </div> 
                  <div class="x_content" style="height:33px; ">
                 <!--    <p style="margin-left:1%;">Donnez du sens à votre article en lui donnant un titre</p> -->
                    <!-- <br /> -->
                   
                         <form class="form-horizontal form-label-left">

                      <div class="form-group">
                        
                        
                        <div class="col-md-2 col-sm-9 col-xs-12" style="border-right:1px solid #ccc;">
                            Articles <span class="badge badge-success">{{count($articles)}}</span>
                        </div>
                        <div class="col-md-2" style="border-right:1px solid #ccc;">
                            En ligne <span class="badge badge-success">{{$online}}</span>
                        </div>
                        <div class="col-md-2">
                            Brouillon <span class="badge badge-success">{{$draft}}</span>
                        </div>
                      <!-- <div class="col-md-6 "> -->
                           <!-- <button class="btn btn-primary">Trier</button> -->
                           <!-- <div class="pull-right"> -->
              <!--                <div class="col-md-6 ">
                             <input type="text" class="form-control" name="" placeholder="Rechercher un article rapidement">
                           </div>
                            <div class="col-md-6">
                             <button class="btn btn-primary" style="border-radius:0;background:#1b2254e3;">Rechercher</button>
                           </div>
                           </div>
                      </div> -->

                      
                      </div>

                    </form>

                    
                  </div>
                </div>

              </div>
	</div>
	<div class="row">
			
			<div class="row" style=" ">
                <div class="col-md-12 col-sm-6 col-xs-12" style="padding-left:20px;padding-right:20px;">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tous les articles <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table table-bordered table-hover">
                      <thead style="background:#1b2254e3;color:#fff;">
                        <tr>
                          <th  class="hidden-xs">#</th>
                          <th>Titre</th>
                          <th class="hidden-xs hidden-sm">Alias</th>
                          <!-- <th>Déscription</th> -->
                          <th>Date de création</th>
                          <th>Status actuel</th>
                          <th class="hidden-xs hidden-sm">Action à appliquer</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <!-- <tr>
                          <th scope="row"><input type="checkbox" name=""></th>
                          <td>Mark</td>
                          <td>Otto</td>
                          <td>@mdo</td>
                          <td>Mark</td>
                          <td>
                            <select class="form-control">
                                <option>Brouillon</option>
                                <option>Publier</option>
                            </select>
                          </td>
                          <td>
                            <select class="form-control">
                                <option>Supprimer</option>
                                <option>Modifier</option>
                            </select>
                          </td>
                        </tr> -->
                        @foreach($articles as $article)
                          <form method="post" action="{{route('byaction')}}"> 
                            {{csrf_field()}}
                            <input type="hidden" name="item" value="{{$article->id}}">
                            <tr>
                              <td  class="hidden-xs"><input type="checkbox" name="" class="checked" value="checked"></td>
                              <td>{{$article->title}}</td>
                              <td class="hidden-xs hidden-sm">{{$article->slug}}</td>
                              <!-- <td>{{$article->content}}</td> -->
                              <td>{{$article->created_at}}</td>
                              <!-- <td>{{$article->title}}</td> -->
                              <!-- <td> -->
                              <td>
                                @switch($article->state)
                                  @case('DRAFT')
                                      {{'Brouillon'}}
                                    @break
                                  @case('PUT_ON_LINE')
                                      {{'Publié'}}
                                    @break
                                @endswitch
                              </td>
                              <td>
                              <select  name="action" class="form-control">
                                  <option value="1" >Supprimer</option>
                                  <option value="2" >Modifier</option>
                              </select>
                              <br>
                               <button class="btn btn-primary pull-right hidden-sm hidden-md hidden-lg" style="background:#1b2254e3;border-radius:0; ">Appliquer</button>
                            </td>
                            </td>
                            <td class="hidden-xs">
                              <button class="btn btn-primary pull-right" style="background:#1b2254e3;border-radius:0; ">Appliquer</button>
                            </td>
                              <!-- </td> -->
                            </tr>
                          </form>
                        @endforeach
                        <!-- <tr>
                          <th scope="row"><input type="checkbox" name=""></th>
                          <td>Jacob</td>
                          <td>Thornton</td>
                          <td>@fat</td>
                          <td>Jacob</td>
                          <td>
                            <select class="form-control">
                                <option>Brouillon</option>
                                <option>Publier</option>
                            </select>
                          </td>
                          <td>
                             <select class="form-control">
                                <option>Supprimer</option>
                                <option>Modifier</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row"><input type="checkbox" name=""></th>
                          <td>Larry</td>
                          <td>the Bird</td>
                          <td>@twitter</td>
                          <td>Larry</td>
                          <td>
                            <select class="form-control">
                                <option>Brouillon</option>
                                <option>Mettre en ligne</option>
                            </select>
                          </td>
                          <td>
                             <select class="form-control">
                                <option>Supprimer</option>
                                <option>Modifier</option>
                            </select>
                          </td>
                        </tr> -->
                      </tbody>
                    </table>
                    <hr>
                    
                  </div>
                </div>
              </div>
              </div>

	</div>


@stop
