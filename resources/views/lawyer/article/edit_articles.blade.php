@extends('layouts.admin')



@section('content')
  <div class="row">
    @if($errors->any())
      @foreach($errors->all() as $error)
        <p>{{$error}}</p>
      @endforeach
    @endif
  </div>
    <div class="row">
      @include('inc.message')
    </div>
    <form method="post" action="{{route('update')}}" enctype="multipart/form-data">  
      <input type="hidden" name="item" value="{{$article->id}}">
      {{csrf_field()}}
    <div class="row">
      <div class="title_left">
                <h3>&nbsp;&nbsp;Rediger vos articles facilement </h3>
              </div>
    </div>
    <div class="clear-fix"></div>
    <div class="row" >
              <div class="col-md-6 col-xs-12">
                <div class="x_panel" style="border-top:3px solid #1b2254e3;">
                   <div class="x_title">
                    <h2>Ajouter une image en live  </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-info-circle"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
s                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div> 
                  <div class="x_content text-center">
                      <p>Cette une image servira de présentation à votre article.</p>
                    <br>
                   
                         <!-- <form class="form-horizontal form-label-left"> -->
                      <div class="col-md-9" id="image-panel">
                        
                      </div>
                      <div class="form-group">
                        
                        <div class="col-md-9 col-sm-12 col-xs-12">

                          <label class="img-default btn btn-default" for="image-live" style="background:#1b2254e3;color:#fff;">Ajouter une image <i class="fa fa-picture-o"></i>
                            <input type="file" name="file" class="form-control" id="image-live" placeholder="Default Input">
                          </label>
                        </div><br><br><br>
                       
                      </div>
                      
                      <!-- </form> -->
                    </div>

                    

                    
                  </div>
                </div>

                <div class="col-md-6 col-xs-12">
                <div class="x_panel" style="border-top: 3px solid #1b2254e3;">
                   <div class="x_title">
                    <h2>Enregistrer l'article comme </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-info-circle"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div> 
                  <div class="x_content ">
                    <p style="margin-left:3%;">Cette option indique où sera mis l'article après soumision.</p>
                    <br>
                   
                         <!-- <form class="form-horizontal form-label-left"> -->

                      <div class="form-group">
                        
                        
                        <div class="col-md-12 col-sm-9 col-xs-12 text-center">
                          <select name="tosendlike" class="form-control" required="required" style="">
                            <!-- <option >Publier comme</option> -->
                            <option value="1" selected="selected">Brouillon</option>
                            <option value="2">Mettre en ligne</option>
                          </select>
                        </div>
                      </div>
                      
                      <!-- </form> -->
                    </div>

                    

                    
                  </div>
                </div>

              </div>

              <div class="row" >
              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                   <div class="x_title">
                   
                    <div class="clearfix"></div>
                  </div> 
                  <div class="x_content">
                    <p style="margin-left:1%;">Donnez du sens à votre article en lui donnant un titre</p>
                    <br>
                   
                         <!-- <form class="form-horizontal form-label-left"> -->

                      <div class="form-group">
                        
                        
                        <div class="col-md-12 col-sm-9 col-xs-12">
                         <input type="text" value="{{old('title')?old('title'):$article->title}}" required="required"  name="title" class="form-control" placeholder="donner un titre à l'article ici">
                        </div>
                      </div>
                      
                      <!-- </form> -->
                    </div>

                    

                    
                  </div>
                </div>

              </div>

              <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Décrivez votre article à ce niveau<small>(description)</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-info-circle"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p>Laissez libre cours à votre imagination, et faites découvrir aux autres ce qu'il y a de mieux en vous à travers votre imagination.</p>
                  <div id="alerts"></div>
                  <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor-one">
                    <div class="btn-group">
                      <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                      <ul class="dropdown-menu">
                      </ul>
                    </div>

                    <div class="btn-group">
                      <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li>
                          <a data-edit="fontSize 5">
                            <p style="font-size:17px">Huge</p>
                          </a>
                        </li>
                        <li>
                          <a data-edit="fontSize 3">
                            <p style="font-size:14px">Normal</p>
                          </a>
                        </li>
                        <li>
                          <a data-edit="fontSize 1">
                            <p style="font-size:11px">Small</p>
                          </a>
                        </li>
                      </ul>
                    </div>

                    <div class="btn-group">
                      <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                      <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                      <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                      <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                    </div>

                    <div class="btn-group">
                      <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                      <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                      <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                      <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                    </div>

                    <div class="btn-group">
                      <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                      <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                      <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                      <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                    </div>

                    <div class="btn-group">
                      <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                      <div class="dropdown-menu input-append">
                        <input class="span2" placeholder="URL" type="text" data-edit="createLink">
                        <button class="btn" type="button">Add</button>
                      </div>
                      <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
                    </div>

                    <div class="btn-group">
                      <a class="btn" title="Insert picture (or just drag &amp; drop)" id="pictureBtn"><i class="fa fa-picture-o"></i></a>
                      <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" value="{{old('file')}}">
                    </div>

                    <div class="btn-group">
                      <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                      <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                    </div>
                  </div>

                  <div id="editor-one" class="editor-wrapper placeholderText" style="overflow:hidden;" contenteditable="true"></div>

                  <textarea name="descr" id="descr" style="display:none;"></textarea>
                  
                  <br>
                  <div id="retype" style="display:none;">{{old('descr')?old('descr'):$article->content}}</div>
                  <div class="ln_solid"></div>

                  <div class="form-group">
                    
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <input type="submit" style="background:#1b2254e3;" value="Publier" class="btn btn-primary">
                    </div>
                  </div>
                </div>
              </div>
            </div>


              </div>  

        </form>



@stop