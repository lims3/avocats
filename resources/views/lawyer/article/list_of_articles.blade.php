@extends('layouts.app')


@section('content')
		@if(session('message'))
			<h1>{{session('message')}}</h1>
			{{session()->put('message','')}}
		@endif
		
		<div class="col-md-6 col-md-offset-3 ">
				<h1>Tous les articles <strong>{{count($articles)}}</strong></h1>

				@foreach($articles as $article)
					<h3>{{$article->title}}</h3>
					<p>{{$article->Content}}</p>
					<a href="{{route('article.show',['id'=>$article->id])}}" class="btn btn-default">lire la suite</a>
					<a href="{{route('delete',['id'=>$article->id])}}" class="btn btn-primary">Supprimer</a>

				@endforeach
		</div>
		

@stop

	