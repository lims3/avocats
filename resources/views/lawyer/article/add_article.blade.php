@extends('layouts.app')


@section('content')
		@include('inc.message')
		@if($errors->any())

		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $errors)

					<li>{{$errors}}</li>

				@endforeach
			</ul>
		</div>
		<div class="col-md-6 col-md-offset-3 ">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="panel-title">
						<span>Ajouter un article</span>
					</div>
				</div>
				<form method="post" action="{{route('article.store')}}">
					<div class="panel-body">
						
							{{csrf_field()}}
							<div class="form-group>">
								<label class="label-control pull-left">Titre</label>	
									<input type="text" class="form-control" name="title">
							</div>

							<div class="form-group">
								<label class="control-label pull-left">Contenu de l'article</label>
									<textarea name="content" class="form-control">
									</textarea>
							</div>
					
					</div>
					<div class="panel-footer">
						<button type="submit" class="btn btn-success ">Ajouter l'article</button>
					</div>
					</form>	
				</div>

			</div>
		</div>
	</div>

@stop

