@extends('layouts.stability',['toblack'=>'true'])

@inject('autority','App\Models\Questions')



@section('content')
		
		<div class="container" style="padding-top:110px;">
		<!-- <div class="row" style="margin-bottom:35px;margin-top:90px;"> -->
      <!-- <div class="col-md-12"> -->
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
           <div class="custom-text text-center">
              <h2 class="text-center text-style">Les dernières questions posées</h2>
              <i class="glyphicon glyphicon-question-sign" style="color:#1b2254e3;font-size:35px;"></i>
              <!-- <hr style="width:60%;"> -->
            </div>
            </div>
            </div>
            <br>
            <br>
            <div class="row text-center">
            	<div class="col-md-12 ">
            		@foreach($questions as $question)

	            		 <h3 style="font-size:1.6em;"><a href="{{route('faq',['id'=>$question->id])}}">{{$question->question}}</a></h3>
                   <h1 style="font-size:18px;" class="label label-default">{{$question->shema()['speciality']}}</h1>
                   <h1 style="font-size:18px;" class="label label-primary">
                    {{ $question->shema()['autority']->name}}</h1>
	                 <hr style="width:60%;">
            		@endforeach
            	</div>	
            </div>

            <div class="row text-center">
              {{$questions->links()}}
            </div>


			</div>

        <br><br><br><br>
          <div class="row" style="background:#343A40;color:#fff;">
      <div class="col-md-8 col-md-offset-2 " >
        <br class="hidden-xs hidden-sm">
          <div class="row">
             <div class="col-md-3 hidden-xs hidden-sm">
            <h4 
"><span >A propos</span></h4>
             <!-- <hr style="color:#fff;"> -->
            <p style="color:#c3c0c0;">
            Charte des avocats
              <br>
              CGU
     

 </p>
          </div>
            <div class="col-md-5 hidden-xs hidden-sm  ">
            <h4 ><span >Mentions légales</span></h4>
            <!-- <hr style="color:#fff;"> -->
            <p style="color:#c3c0c0;">
            Le présent site internet est conçu et édité par la société Avocats.cd. SAS, 
capital: 2.000 USSD, 
immatriculation:CD/KNG/RCCM/18-B-01316 AT
Directeur de la publication: Junior Luyindula. 
Le site internet est hébergé par la société Amazon Web Services Inc (Ireland)
              
             </p>
          </div>
         
          <div class="col-md-4 hidden-xs hidden-sm">
            <h4 style=" "><span>Contactez nous</span></h4>
             <!-- <hr style="color:#fff;"> -->
            <input type="text" class="form-control custom-field-add" style="background:#1b2254e3;border-radius:20px;" name="" placeholder="email">
            <br>
            <textarea class="form-control custom-field-add" placeholder="message..." style="border-radius:10px;padding-left:0px;"  >
             
            </textarea>
            <br>
            <button class="btn btn-primary pull-right" style="background:#1b2254e3;border-radius:3px;border:0;padding:10px;margin-top:4px;">Envoyer</button>
          </div>
      </div>
      </div>
      </div>
      <div class="row" style="background:#2a2e31;padding-top:15px;color:#fff;">
        <!-- <br class="hidden-lg"> -->
        <!-- <br class="hidden-lg"> -->
        <!-- <br class="hidden-lg"> -->
        <div class="col-md-6 col-md-offset-3 text-center">
          <!-- <hr> -->
            <p>Copyright © 2018 Avocats.cd - Tous droits réservés</p>
            <p>Powered by ohelene</p>

        </div>
        <!-- <br class="hidden-lg"> -->
        <br class="hidden-lg hidden-sm">
        <br class="hidden-lg hidden-sm">
      </div>
		


@stop

