@extends('layouts.app')

@section('content')

	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box ">
					<div class="left">
						<div class="content">
							<div class="header">
								 {{--  <div class="logo text-center"><img src="{{asset('/img/logo-dark.png')}}" alt="Klorofil Logo"></div>   --}}
								<p class="lead">Bienvenue sur schoolap</p>
							</div>
							<form class="form-auth-small" method="POST" action="{{ route('login') }}">
                             {{csrf_field()}}
                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            {{--  <label for="email" class="col-md-4 control-label">Identifiant</label>  --}}
								<div class="input-group">
								<input id="username" type="text" class="form-control" name="username" placeholder="Indentifiant" value="{{ old('username') }}" required autofocus autocomplete="off" >
								<span class="input-group-addon"><i class="lnr lnr-user"></i> </span>
								</div>
                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
								@endif
								</div>

								<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
									{{--  <label for="password" class="col-md-4 control-label">Password</label>  --}}
                            {{--  <div class="col-md-6">  --}}
								<div class="input-group">
                                <input id="password" type="password" placeholder="Mot de passe" class="form-control" name="password" required>
								<span class="input-group-addon"><i class="lnr lnr-lock"></i> </span>	
								</div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
									@endif
									{{--  </div>  --}}
								</div>

								<div class="form-group clearfix">
									<label class="fancy-checkbox element-left">
										<input type="checkbox">
										<span>Se souvenir de moi</span>
									</label>
								</div>
								<button type="submit" class="btn btn-primary btn-lg btn-block">Se connecter <i class="glyphicon glyphicon-sign-in"></i></button>
								<div class="bottom">
									{{--  <span class="helper-text"><i class="fa fa-lock"></i> <a href="#">Mot de passe oublié?</a></span>  --}}
								</div>
							</form>
						</div>
					</div>
					<div class="right" style="background-color:#00aeef;" >

						 <div class="overlay" style="background:url('{{asset('img/Schoolap.jpg')}}');background-size:100% 80%;background-repeat:no-repeat;"></div> 
						{{--  <img src="{{asset('img/Schoolap.jpg')}}" class="img-responsive"/>  --}}
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
@endsection
