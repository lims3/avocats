@extends('layouts.app')




@section('content')

		
	<div class="col-md-12">
		<div class="col-md-6 col-md-offset-3 text-center">
			<h1>Veuillez procéder au paiement pour rendre votre compte actif</h1>
			<h4>Une fois votre compte activé, vous pourrez jouir de toutes les fonctionnalités disponibles sur cette plateforme </h4>
<!-- 				
			<a href="{{url('/').'/home'}}" class="btn btn-primary">Faire ça plus tard</a>
			&nbsp; -->
			<a href="{{url('/').'/payementCheckout'}}" class="btn btn-success">Procéder au paiement</a>
		</div>
	</div>
@stop

