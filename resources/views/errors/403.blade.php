@extends('layouts.admin')

@section('title') Accès interdit (forbiden 403) @stop

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">
                <h3>Compte inactif</h3>
            </div>
        </div>
        <div class="panel-body">
            <p>
               Veuillez confirmer votre compte afin de poursuivre votre navigation sur ce site
            </p>
        </div>
        <div class="panel-footer">
            <a href="{{route('lwck')}}" class="btn btn-primary">Activez mon compte</a>
        </div>
    </div>
@stop