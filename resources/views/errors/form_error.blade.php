@if($errors->count())
    <div class="alert alert-warning">
        <h4>Les erreurs ont été trouvées pendant la validation de votre réquête</h4>
        <ul class="list">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif