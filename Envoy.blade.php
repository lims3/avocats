@servers(['web'=> 'labesadmin@138.68.159.109', 'local'=>'127.0.0.1'])


@story('deploy')
    update
    migrate
    composer
@endstory

@task('update', ['on'=>'local'])
    scp -r app/ config/ routes/ labesadmin@138.68.159.109:~/applications/xterqvudtx/public_html/
    scp -r database/migrations labesadmin@138.68.159.109:~/applications/xterqvudtx/public_html/database
    scp -r resources/views labesadmin@138.68.159.109:~/applications/xterqvudtx/public_html/resources
@endtask

@task('migrate', ['on'=>'web'])
    php public_html/artisan migrate:fresh --env=local
@endtask
@task('composer', ['on'=>'web'])
    php ~/applications/xterqvudtx/public_html/artisan migrate:fresh
@endtask