<?php

return [
	'mode'                  => 'utf-8',
	'format'                => 'A4-L',
	'author'                => 'Labes key',
	'subject'               => 'Rapport',
	'keywords'              => '',
	'creator'               => 'Schoolap',
	'display_mode'          => 'fullpage',
    'displayDefaultOrientation' => true,
];
