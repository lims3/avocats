
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autorities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();

       
        });

         DB::table('autorities')->insert([
            'name' => 'Droit penal'
    
        ]);

         DB::table('autorities')->insert([
                    'name' => 'Droit civil des personnes et de la famille'
            
        ]);

         DB::table('autorities')->insert([
                    'name' => 'Droit foncier,immobilier et logement'
            
        ]);

         DB::table('autorities')->insert([
                    'name' => 'Droit du travail et de la sécurité sociale'
            
        ]);

        DB::table('autorities')->insert([
                    'name' => 'Droit des affaires et sociétés commerciales'
            
        ]);

        DB::table('autorities')->insert([
                    'name' => 'Proprièté intellectuelle'
            
        ]);


        Schema::table('users', function (Blueprint $table) {
            $table->foreign('autority_id')->references('id')->on('autorities')
                                                               ->onDelete('restrict')
                                                               ->onUpdate('cascade');
        });

        Schema::table('specialities', function (Blueprint $table) {
            $table->foreign('autority_id')->references('id')->on('autorities')
                                                               ->onDelete('restrict')
                                                               ->onUpdate('cascade');
        });
      
        
        DB::table('specialities')->insert([
            'name' => 'divorce','autority_id'=>1]
        );
         DB::table('specialities')->insert([
            'name' => 'adoption','autority_id'=>1]
        ); 
         DB::table('specialities')->insert([
            'name' => 'création entreprise','autority_id'=>1]
        );

        DB::table('specialities')->insert([
            'name' => 'Abandon de famille','autority_id'=>1]
        );

        DB::table('specialities')->insert([
            'name' => 'Convocation à la police','autority_id'=>1]
        );

        DB::table('specialities')->insert([
            'name' => 'Arrestation','autority_id'=>1]
        );

        DB::table('specialities')->insert([
            'name' => 'Abus  de confiance','autority_id'=>1]
        );


        DB::table('specialities')->insert([
            'name' => 'Coup et blessures','autority_id'=>1]
        );

        DB::table('specialities')->insert([
                    'name' => 'Escroquerie','autority_id'=>1]
        );


        DB::table('specialities')->insert([
            'name' => 'Injures','autority_id'=>1]
        );


        DB::table('specialities')->insert([
            'name' => 'Menace de mort','autority_id'=>1]
        );

        DB::table('specialities')->insert([
            'name' => 'Viol','autority_id'=>1]
        );


       
        DB::table('specialities')->insert([
            'name' => 'Violences sexuelles','autority_id'=>1]
        );
        
        DB::table('specialities')->insert([
                    'name' => 'Adoption','autority_id'=>2]
        );

        DB::table('specialities')->insert([
                    'name' => 'Divorce','autority_id'=>2]
        ); 

        DB::table('specialities')->insert([
                    'name' => 'Succession','autority_id'=>2]
        );

        DB::table('specialities')->insert([
                    'name' => 'Pension alimentaire','autority_id'=>2]
        );

        DB::table('specialities')->insert([
                    'name' => 'Vente de parcelle','autority_id'=>3]
        );

        DB::table('specialities')->insert([
                    'name' => 'Conflit parcellaire','autority_id'=>3]
        );

        DB::table('specialities')->insert([
                    'name' => 'Relation propriétaire locataire','autority_id'=>3]
        );

        DB::table('specialities')->insert([
                    'name' => 'Expulsion','autority_id'=>3]
        );

        DB::table('specialities')->insert([
                    'name' => 'Expropriation','autority_id'=>3]
        );

        DB::table('specialities')->insert([
                    'name' => 'Contrat de travail','autority_id'=>4]
        );

        DB::table('specialities')->insert([
                    'name' => 'Protection de l\'employé ','autority_id'=>4]
        );

        DB::table('specialities')->insert([
                    'name' => 'Sécurité sociale ','autority_id'=>4]
        );

        DB::table('specialities')->insert([
                    'name' => 'Harcèlement au travail ','autority_id'=>4]
        );
        
        DB::table('specialities')->insert([
                            'name' => 'Licenciement','autority_id'=>4]
        );

        DB::table('specialities')->insert([
                            'name' => 'Décompte final','autority_id'=>4]
        );
        
        DB::table('specialities')->insert([
                            'name' => 'Création d\'entreprise','autority_id'=>5]
        );

        DB::table('specialities')->insert([
                            'name' => 'Litige commercial','autority_id'=>5]
        );

        DB::table('specialities')->insert([
                            'name' => 'Concurrence déloyal','autority_id'=>5]
        );

        DB::table('specialities')->insert([
                            'name' => 'Recouvrement des créances','autority_id'=>5]
        );

        DB::table('specialities')->insert([
                            'name' => 'Contrat de partenariat','autority_id'=>5]
        );

        DB::table('specialities')->insert([
                            'name' => 'Bail commercial','autority_id'=>5]
        );

        DB::table('specialities')->insert([
                            'name' => 'Dépot brevet','autority_id'=>6]
        );
        
        DB::table('specialities')->insert([
                            'name' => 'Protéger une marque','autority_id'=>6]
        );

        DB::table('specialities')->insert([
                            'name' => 'Droits d\'auteur','autority_id'=>6]
        );

        DB::table('specialities')->insert([
                            'name' => 'Contrfaçon de votre marque','autority_id'=>6]
        );

        DB::table('specialities')->insert([
                            'name' => 'Violation de votre brevet','autority_id'=>6]
        );
        



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autorities');
    }
}
