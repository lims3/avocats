<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use App\Models\Picture;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',60)->nullable();
            $table->string('lastname',60)->nullable();
            $table->string('firstname',60)->nullable();
            $table->string('sexe',1)->nullable();
            $table->string('province',60)->nullable();
            $table->string('phone',12)->nullable();
            $table->text('about')->nullable();
            $table->string('numero_ordre')->nullable();
            $table->string('barreau')->nullable();
            $table->string('date_prestation')->nullable();
            $table->string('type_avocat')->nullable();
            $table->string('adresse_cabinet')->nullable();
            $table->string('nom_cabinet')->nullable();

            $table->string('adresse',160);
            $table->integer('profile_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('profile_id')->references('id')->on('users')
                                                            ->onDelete('restrict')
                                                            ->onUpdate('cascade');
        });



        $p=DB::table('profiles')->insert([
            'name' => 'musimbi',
            'lastname'=>'musimbi',
            'firstName' => 'arvey',
            'sexe' => '1',
            'phone'=>'0815854126',
            'province' => 1,
            'adresse' => 'lokele I 2 bus matete',
            'profile_id'=>User::first()->id
        ]);


        $p=DB::table('profiles')->insert([
            'name' => 'musimbi',
            'lastname'=>'musimbi',
            'firstName' => 'arvey',
            'sexe' => '1',
            'phone'=>'0815854126',
            'province' => 1,
            'adresse' => 'lokele I 2 bus matete',
            'profile_id'=>User::find(2)->id
        ]); 

    
        $p=DB::table('profiles')->insert([
            'name' => 'musimbi',
            'lastname'=>'musimbi',
            'firstName' => 'arvey',
            'sexe' => '1',
            'phone'=>'0815854126',
            'province' => 2,
            'adresse' => 'lokele I 2 bus matete',
            'profile_id'=>User::get()->last()->id
        ]);

       
       

        

         

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
