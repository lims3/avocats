<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pictures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Name');
            $table->longText('thumbnail')->nullable();
            $table->integer('profile_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('profile_id')->references('id')->on('profiles')
                                                            ->onDelete('restrict')
                                                            ->onUpdate('cascade');
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pictures');
    }
}
