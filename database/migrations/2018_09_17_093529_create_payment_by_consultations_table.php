<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentByConsultationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_by_consultations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Amount');
            $table->integer('solde_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('solde_id')->references('id')->on('soldes')
                                                            ->onDelete('restrict')
                                                            ->onUpdate('cascade');
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_by_consultations');
    }
}
