<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_id')->unsigned()->index()->nullable();
            $table->integer('lawyer_id')->unsigned()->index()->nullable();
            $table->text('answer')->nullable();
            $table->timestamps();


            $table->foreign('question_id')->references('id')->on('questions')
                                                           ->onDelete('restrict')
                                                           ->onUpdate('cascade');

            $table->foreign('lawyer_id')->references('id')->on('users')
                                                           ->onDelete('restrict')
                                                           ->onUpdate('cascade');                                                           

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers');
    }
}
