<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('state')->nullable();
            $table->integer('barreaux_id')->unsigned()->nullable();
            $table->integer('autority_id')->unsigned()->nullable();
            // $table->integer('specialities_id')->unsigned()->nullable();
            $table->integer('legal_fields_id')->unsigned()->nullable();
            $table->rememberToken();
            $table->timestamps();

            

            
        });


        DB::table('users')->insert([
            'email' => 'avocat@cabinet.cd',
            'state'=>'VALID',
            'autority_id'=>3,
            'password' => bcrypt('12345678')
    
        ]);
        DB::table('users')->insert([
            'email' => 'test@test.cd',
            'state'=>'VALID',
            'autority_id'=>1,
            'password' => bcrypt('12345678')
    
        ]);
        DB::table('users')->insert([
            'email' => 'test@contact.cd',
            'state'=>'VALID',
            'autority_id'=>1,
            'password' => bcrypt('12345678')
    
        ]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
