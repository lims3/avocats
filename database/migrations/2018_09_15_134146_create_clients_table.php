<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Name',60);
            $table->string('Lastname',60);
            $table->string('Firstname',60);
            $table->string('Number',60);
            $table->string('Email',60);
            $table->integer('consulting_id')->index()->unsigned()->nullable();
            $table->timestamps();


            $table->foreign('consulting_id')->references('id')->on('consultings')
                                                              ->onDelete('restrict')
                                                              ->onUpdate('cascade');
        });

        // Schema::table('consultings', function (Blueprint $table) {
        //     $table->foreign('client_id')->references('id')->on('clients')
        //                                                     ->onDelete('restrict')
        //                                                     ->onUpdate('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
