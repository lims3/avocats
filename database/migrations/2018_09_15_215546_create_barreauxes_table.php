<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarreauxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barreauxes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',60);
            $table->integer('province_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('province_id')->references('id')->on('provinces')
                                                            ->onDelete('restrict')
                                                            ->onUpdate('cascade');

        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('barreaux_id')->references('id')->on('barreauxes')
                                                            ->onDelete('restrict')
                                                            ->onUpdate('cascade');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barreauxes');
    }
}
