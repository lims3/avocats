<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index()->nullable()->unsigned();
            $table->integer('speciality_id')->index()->nullable()->unsigned();
            $table->timestamps();
        });



         Schema::table('helps', function (Blueprint $table) {
            $table->foreign('speciality_id')->references('id')->on('specialities')
                                                               ->onDelete('restrict')
                                                               ->onUpdate('cascade');
        });

        
       Schema::table('helps', function (Blueprint $table) {
        $table->foreign('user_id')->references('id')->on('users')
                                                           ->onDelete('restrict')
                                                           ->onUpdate('cascade');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('helps');
    }
}
