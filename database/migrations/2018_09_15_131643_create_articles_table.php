<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',255);
            $table->string('slug',255);
            $table->longText('content');
            $table->string('state')->nullable();
            $table->string('cover')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')
                                                        ->onDelete('restrict')
                                                        ->onUpdate('cascade');
                                                        
        });

        // Schema::table('users', function (Blueprint $table) {
        //     $table->foreign('user_id')->references('id')->on('users')
        //                                                     ->onDelete('restrict')
        //                                                     ->onUpdate('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
