<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('message');
            $table->text('message_type');
            $table->integer('consulting_id')->index()->unsigned()->nullable();
            $table->integer('user_id')->index()->unsigned()->nullable();
            $table->string('sender_type')->nullable();
            $table->timestamps();

            $table->foreign('consulting_id')->references('id')->on('consultings')
                                                              ->onUpdate('cascade')
                                                              ->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                                                              ->onUpdate('cascade')
                                                              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversations');
    }
}
