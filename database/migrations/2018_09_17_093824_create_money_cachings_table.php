<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoneyCachingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('money_cachings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('solde_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('solde_id')->references('id')->on('soldes')
                                                            ->onDelete('restrict')
                                                            ->onUpdate('cascade');
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('money_cachings');
    }
}
