<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specialities', function (Blueprint $table) {
            $table->increments('id');
            // $table->timestamps();
            $table->string('name')->nullable();
            $table->integer('autority_id')->nullable()->index()->unsigned();
        
            $table->timestamps();

           
        });

       








       
       // Schema::table('users', function (Blueprint $table) {
       //  $table->foreign('specialities_id')->references('id')->on('specialities')
       //                                                     ->onDelete('restrict')
       //                                                     ->onUpdate('cascade');
       //  });
      


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specialities');
    }
}
