<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLegalFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legal_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Name');
            $table->timestamps();
         

        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('legal_fields_id')->references('id')->on('legal_fields')
                                                           ->onDelete('restrict')
                                                           ->onUpdate('cascade');
        });

        // Schema::table('users', function (Blueprint $table) {
        //     $table->foreign('profile_id')->references('id')->on('profiles')
        //                                                     ->onDelete('restrict')
        //                                                     ->onUpdate('cascade');
        // }); 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legal_fields');
    }
}
