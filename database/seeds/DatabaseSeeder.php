<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\School::class, 5)->create()->each(function($school){
            $u = factory(\App\User::class)->create();
            factory(\App\Models\Capability::class)->create([
                'user_id' => $u->id,
                'access' => array_merge(\App\Models\Capability::DEFAULT_PRIVILEGES, \App\Models\Capability::DEFAULT_ADMIN_PRIVILEGES)
            ]);
            factory(\App\Models\DefaultInstitutionAdmin::class)->create([
                'user_id' => $u->id,
                'institution_type' => get_class($school),
                'institution_id' => $school->id
            ]);
        });
    }
}
